//
//  LibraryClubService.swift
//  TheLibraryClub
//
//  Created by Phaninder on 27/11/17.
//  Copyright © 2017 Phaninder. All rights reserved.
//

import Foundation
import RxSwift

protocol LibraryClubService {

    func fetchOutlets(pageNumber: String, searchString: String, lat: String, long: String, favorite: Bool, promoId: String) -> Observable<OutletPagination>
    
    func fetchPromotions(pageNumber: String, searchString: String, lat: String, long: String, favorite: Bool, outletId: String, date: String?) -> Observable<PromotionPagination>

    func getOutletLocations() -> Observable<[OutletLocation]>
    
    func getOutletDetailsWithID(_ id: String) -> Observable<OutletDescription>
    
    func getPromotionDetailsWithID(_ id: String) -> Observable<PromotionDescription>

    func loginUserWithEmail(_ email: String, andPassword password: String) -> Observable<User>
    
    func registerUserWithEmail(_ email: String, firstName: String, lastName: String, password: String, gender: String, dateOfBirth: String, phoneNumber: String) -> Observable<User>
    
    func editUserWithEmail(_ email: String, firstName: String, lastName: String, userId: String, dateOfBirth: String, gender: String, phoneNumber: String) -> Observable<User>
    
    func favoriteOutlet(withId id: String, userId: String) -> Observable<String>
    
    func unFavoriteOutlet(withId id: String) -> Observable<String>
    
    func favoritePromotion(withId id: String, userId: String) -> Observable<String>
    
    func unFavoritePromotion(withId id: String) -> Observable<String>
    
    func forgotPasswordForEmail(_ email: String) -> Observable<String>

    func changePassword(_ password: String) -> Observable<String>
    
    func registerDeviceToken(_ deviceToken: String) -> Observable<String>
    
    func logoutUser() -> Observable<Bool>
    
    func fetchNotifications(pageNumber: String) -> Observable<NotificationPagination>
    
    func fetchMyBookings(pageNumber: String) -> Observable<MyBookingsPagination>
    
    func fetchCustomerDetails() -> Observable<User>
    
    func fetchDashboardDetails() -> Observable<CustomerInfoPagination>
    
//    func fetchOrders(pageNumber: String) -> Observable<CustomerInfoPagination>
    
    func editProfileWithImage(params: [String: String], image: UIImage) -> Observable<User>
    
    func fetchBadges(_ userId: String) -> Observable<[Badge]>
    
    func reserveAnOutlet(_ params: [String: String]) -> Observable<String>
    
    func fetchDashBoardPromotions(pageNumber: String) -> Observable<DashBoardPromotion>
    
    func fetchDashBoardOutlets(pageNumber: String) -> Observable<DashBoardOutlet>
    
    func fetchOrders(pageNumber: String) -> Observable<OrderPagination>
    func fetchCheckEmailResponse(email: String) -> Observable<CheckEmailResponse>


}
