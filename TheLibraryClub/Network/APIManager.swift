//
//  APIManager.swift
//  TheLibraryClub
//
//  Created by Phaninder on 27/11/17.
//  Copyright © 2017 Phaninder. All rights reserved.
//

import Foundation
import Unbox
import RxSwift
import MobileCoreServices

class ApiManager {
    
    static let shared = ApiManager()
    var apiService: LibraryClubService = DataService()
    
    fileprivate init() {
        //super.init()
    }
    
    class func getResponseErrorWithStatusCode(_ statusCode:Int) -> ResponseError {
        switch statusCode {
        case ResponseStatusCode.notFound.rawValue:
            return ResponseError.notFoundError
        case ResponseStatusCode.badRequest.rawValue:
            return ResponseError.badRequestError
        case ResponseStatusCode.timeout.rawValue:
            return ResponseError.timeoutError
        case ResponseStatusCode.internalServer.rawValue:
            return ResponseError.internalServerError
        default:
            return ResponseError.unknownError
        }
    }
    
    class func dataTask(request: NSMutableURLRequest) -> Observable<ResponseObject> {
        
        return Observable.create { observer in
            
            let urlconfig = URLSessionConfiguration.default
            urlconfig.timeoutIntervalForRequest = 90
            urlconfig.timeoutIntervalForResource = 90
            
            let session = URLSession(configuration: urlconfig)
            let dataTask = session.dataTask(with: request as URLRequest) { (data, response, error) -> Void in
                if let error_ = error {
                    observer.on(.error(error_))
                } else if let response = response as? HTTPURLResponse {
                    let responseObject = (response, data)
                    observer.on(.next(responseObject))
                }
            }
            dataTask.resume()
            return Disposables.create {
                dataTask.cancel()
            }
        }
    }
    
    class func post(path: URI, isUserAPI: Bool, params: Dictionary<String, AnyObject>? = nil) -> Observable<ResponseObject> {
        
        return ApiManager.dataTask(request: clientURLRequest(path: path.rawValue, isUserAPI: isUserAPI, params: params, method: "POST"))
    }
    
    class func delete(path: URI, isUserAPI: Bool, params: Dictionary<String, AnyObject>? = nil) -> Observable<ResponseObject> {
        var parameters = params

        var pathString = path.rawValue
        if path == .favUnFavOutlet || path == .favUnFavPromotions {
            pathString += "/\(params!["appendId"]!)"
            parameters!["appendId"] = nil
        }
        return ApiManager.dataTask(request: clientURLRequest(path: pathString, isUserAPI: isUserAPI, params: params, method: "DELETE"))
    }
    /*
    class func put(path: URI, isUserAPI: Bool, params: Dictionary<String, AnyObject>? = nil) -> Observable<ResponseObject> {
        return ApiManager.dataTask(request: clientURLRequest(path: path.rawValue, isUserAPI: isUserAPI, params: params, method: "PUT"))
    }
    */
    
    class func get(path: URI, isUserAPI: Bool, params: Dictionary<String, AnyObject>? = nil)  -> Observable<ResponseObject> {
        var parameters = params
        var pathString = path.rawValue
        if (path == .outletDetails || path == .promotionDetails || path == .logout) {
            pathString += parameters!["id"] as! String
            parameters!["id"] = nil
        }
        return ApiManager.dataTask(request: clientURLRequest(path: pathString, isUserAPI: isUserAPI, params: parameters, method: "GET"))
    }
    
    class func clientURLRequest(path: String, isUserAPI: Bool, params: Dictionary<String, AnyObject>? = nil, method: String) -> NSMutableURLRequest {
        var baseURL = ""
        if isUserAPI == true {
            baseURL = userBaseURL
        } else {
            baseURL = appBaseURL
        }
        
        var parameters = params
        if parameters == nil {
            parameters = ["locale": UserStore.shared.userLanguage as AnyObject]
        } else {
            parameters!["locale"] = UserStore.shared.userLanguage as AnyObject
        }
        if isUserAPI == false {
            parameters!["user_id"] = UserStore.shared.userId as AnyObject
        }


        let baseURLString = "\(baseURL)\(path)"
        let request = NSMutableURLRequest(url: NSURL(string: baseURLString)! as URL)
        request.httpMethod = method
        if let _params = parameters {
            switch method {
            case "POST":
                    request.httpMethod = "POST"
                    var queryString = ""

                    for (key, value) in _params {
                        queryString += "\(key)=\(value)&"
                    }

                    let postData = NSMutableData(data: queryString.data(using: String.Encoding.utf8)!)
                    request.httpBody = postData as Data;
                break
            case "GET":
                request.url = formURL(base: baseURLString, paramDict: _params)!
                break
            default:
                request.httpMethod = "DELETE"
                request.url = formURL(base: baseURLString, paramDict: _params)!
                break
            }
            
        }
    
        //add headers
        for (key, value) in headers {
            request.addValue(value, forHTTPHeaderField: key)
        }
        if let version = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String {
           request.addValue(version, forHTTPHeaderField: "App-Version")
        }
        return request
    }
    
    fileprivate class func formURL(base: String, paramDict: Dictionary<String, AnyObject>) -> URL? {
        
        var urlComponents = URLComponents(string: base)!
        var queryItems = [URLQueryItem]()
        for (key, value) in paramDict {
            queryItems.append(URLQueryItem(name: key, value: String(describing: value)))
        }
        urlComponents.queryItems = queryItems as [URLQueryItem]?
        return urlComponents.url
    }
    
    fileprivate class func jsonToString(json: AnyObject) ->  String {
        do {
            let data1 =  try JSONSerialization.data(withJSONObject: json, options: JSONSerialization.WritingOptions.prettyPrinted) // first of all convert json to the data
            let convertedString = String(data: data1, encoding: String.Encoding.utf8) // the data will be converted to the string
            return convertedString ?? ""
            
        } catch let myJSONError {
            print(myJSONError)
            return ""
        }
    }
    
    class func validateResponse(forResponse responseObject: ResponseObject, path: URI, successCompletionHandler:((_ success: Bool, _ jsonObject: Any?, _ error: NSError?) -> Void)) {
        UserStore.shared.isServerError = false
        let response = responseObject.response
        let data = responseObject.data
        if let data = data {
            #if DEBUG
                let string1 = String(data: data, encoding: String.Encoding.utf8) ?? "Data could not be printed"
                print("REQUEST: \(String(describing: responseObject.response.url!))")
                print("RESPONSE:\(string1)")
            #endif
            do  {
                let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers)
                
                //TODO: Consider the status as a boolean, once all APIs return boolean value.
                guard response.statusCode == successCode else {
                    if let jsonDict = json as? [String: AnyObject], let message = jsonDict["message"] as? String {
                        let error = NSError(domain: "", code: -1, userInfo: [NSLocalizedDescriptionKey: message])
                        return successCompletionHandler(false, json, error)
                    } else {
                        let error = NSError(domain: "", code: -1, userInfo: [NSLocalizedDescriptionKey: "Failed to get data"])
                        return successCompletionHandler(false, json, error)
                    }
                }
                return successCompletionHandler(true, json, nil)

            } catch {
                return successCompletionHandler(false, nil, ResponseError.parseError as NSError)
            }
        }
        return successCompletionHandler(false, nil, ResponseError.parseError as NSError)
    }
    
    
    /// Create request
    /// - returns:            The `URLRequest` that was created
    
    class func createRequest(params: [String: String], chosenImage: UIImage, path: String) -> Observable<ResponseObject>  {
        let baseURLString = "\(userBaseURL)\(path)"
        let r  = NSMutableURLRequest(url: URL(string: baseURLString)!)
        r.httpMethod = "POST"
        let boundary = generateBoundaryString()
        r.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
        
        r.httpBody = createBody(parameters: params,
                                boundary: boundary,
                                data: UIImageJPEGRepresentation(chosenImage, 0.7)!,
                                mimeType: "image/jpg",
                                filename: "profilepic")
        return ApiManager.dataTask(request: r)
    }
    
    fileprivate class func createBody(parameters: [String: String],
                    boundary: String,
                    data: Data,
                    mimeType: String,
                    filename: String) -> Data {
        let body = NSMutableData()
        
        let boundaryPrefix = "--\(boundary)\r\n"
        
        for (key, value) in parameters {
            body.appendString(boundaryPrefix)
            body.appendString("Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n")
            body.appendString("\(value)\r\n")
        }
        
        body.appendString(boundaryPrefix)
        body.appendString("Content-Disposition: form-data; name=\"file\"; filename=\"\(filename)\"\r\n")
        body.appendString("Content-Type: \(mimeType)\r\n\r\n")
        body.append(data)
        body.appendString("--\(boundary)--\r\n")
        
        return body as Data
    }
    
    /// Create boundary string for multipart/form-data request
    ///
    /// - returns:            The boundary string that consists of "Boundary-" followed by a UUID string.
    
    fileprivate class func generateBoundaryString() -> String {
        return "Boundary-\(UUID().uuidString)"
    }
    
    /// Determine mime type on the basis of extension of a file.
    ///
    /// This requires `import MobileCoreServices`.
    ///
    /// - parameter path:         The path of the file for which we are going to determine the mime type.
    ///
    /// - returns:                Returns the mime type if successful. Returns `application/octet-stream` if unable to determine mime type.
    
    private func mimeType(for path: String) -> String {
        let url = URL(fileURLWithPath: path)
        let pathExtension = url.pathExtension
        
        if let uti = UTTypeCreatePreferredIdentifierForTag(kUTTagClassFilenameExtension, pathExtension as NSString, nil)?.takeRetainedValue() {
            if let mimetype = UTTypeCopyPreferredTagWithClass(uti, kUTTagClassMIMEType)?.takeRetainedValue() {
                return mimetype as String
            }
        }
        return "application/octet-stream"
    }
}

extension NSMutableData {
    func appendString(_ string: String) {
        let data = string.data(using: String.Encoding.utf8, allowLossyConversion: false)
        append(data!)
    }
}
