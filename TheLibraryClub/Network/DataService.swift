    //
//  DataService.swift
//  TheLibraryClub
//
//  Created by Phaninder on 27/11/17.
//  Copyright © 2017 Phaninder. All rights reserved.
//

import Foundation
import UIKit
import RxSwift
import Unbox
import MapKit

// MARK: - APIClient (Convenient Resource Methods)

class DataService:LibraryClubService {

    let disposableBag = DisposeBag()
    
    func fetchOutlets(pageNumber: String, searchString: String, lat: String, long: String, favorite: Bool, promoId: String) -> Observable<OutletPagination> {
        var params = ["page": pageNumber,
                      "favourite": favorite ? "1" : "0"]
        if !searchString.isEmptyString() {
            params["query"] = searchString
        }
        if !lat.isEmptyString() {
            params["latitude"] = lat
        }
        if !long.isEmptyString() {
            params["longitude"] = long
        }
        if (!promoId.isEmptyString()) {
            params["promotion_id"] = promoId
        }
        
        return Observable.create{ observer in
            let outletObserver = ApiManager.get(path: .outletList, isUserAPI: false, params: params as Dictionary<String, AnyObject>)
            _ = outletObserver.retry(2)
                .subscribe(
                    onNext: {(responseObject) in
                        ApiManager.validateResponse(forResponse: responseObject, path: .outletList, successCompletionHandler: { (success, json, error) in
                            
                            if success == true, let outletArray = json {
                                let outletObject = ["data": outletArray]
                                do {
                                    let outletListPagination: OutletPagination = try unbox(dictionary: outletObject)
                                    observer.on(.next(outletListPagination))
                                } catch {
                                    observer.onError(ResponseError.unboxParseError)
                                }
                            } else if success == false, let error_ = error {
                                observer.onError(error_)
                            } else {
                                observer.onError(ResponseError.unknownError as NSError)
                            }
                        })
                }, onError: {(error) in
                    observer.onError(error)
                })
            return Disposables.create {}
        }
    }
    
    func fetchPromotions(pageNumber: String, searchString: String, lat: String, long: String, favorite: Bool, outletId: String, date: String?) -> Observable<PromotionPagination> {
        var params = ["page": pageNumber,
                      "favourite": favorite ? "1" : "0"]
        if !searchString.isEmptyString() {
            params["query"] = searchString
        }
        if !lat.isEmptyString() {
            params["latitude"] = lat
        }
        if !long.isEmptyString() {
            params["longitude"] = long
        }
        if !outletId.isEmptyString() {
            params["outlet_id"] = outletId
        }
        if let date = date {
            params["date"] = date
        }
        return Observable.create{ observer in
            let outletObserver = ApiManager.get(path: .promotionList, isUserAPI: false, params: params as Dictionary<String, AnyObject>)
            _ = outletObserver.retry(2)
                .subscribe(
                    onNext: {(responseObject) in
                        ApiManager.validateResponse(forResponse: responseObject, path: .promotionList, successCompletionHandler: { (success, json, error) in
                            
                            if success == true, let promotionArray = json {
                                let promotionObject = ["data": promotionArray]
                                do {
                                    let promotionListPagination: PromotionPagination = try unbox(dictionary: promotionObject)
                                    observer.on(.next(promotionListPagination))
                                } catch {
                                    observer.onError(ResponseError.unboxParseError)
                                }
                            } else if success == false, let error_ = error {
                                observer.onError(error_)
                            } else {
                                observer.onError(ResponseError.unknownError as NSError)
                            }
                        })
                }, onError: {(error) in
                    observer.onError(error)
                })
            return Disposables.create {}
        }
    }
    
    func getOutletLocations() -> Observable<[OutletLocation]> {
        return Observable.create{ observer in
            let outletLocationObserver = ApiManager.get(path: .outletLocations, isUserAPI: false, params: nil)
            _ = outletLocationObserver.retry(2)
                .subscribe(
                    onNext: {(responseObject) in
                        ApiManager.validateResponse(forResponse: responseObject, path: .outletLocations, successCompletionHandler: { (success, json, error) in
                            if success == true, let outletObject = json as? [[String: AnyObject]] {
                                var outlets = [OutletLocation]()
                                for outletDict in outletObject {
                                    let title  = outletDict["name"] as? String ?? ""
                                    let id: Int = outletDict["id"] as? Int ?? 0
                                    let address: String = outletDict["address"] as? String ?? ""
                                    let isBookingAllowed: Bool = outletDict["allow_booking"] as? Bool ?? false
                                    let lat = outletDict["latitude"] as? String ?? "0"
                                    let long = outletDict["longitude"] as? String ?? "0"
                                    let operatingHours = outletDict["operating_hours"] as? [String: String]
                                    let outletLocation = OutletLocation(title: title, outletID: id, locationName: address, allowBooking: isBookingAllowed, coordinate: CLLocationCoordinate2D(latitude: Double(lat)!,
                                                                                                                                                                                              longitude: Double(long)!), operatingHours: operatingHours)
                                    outlets.append(outletLocation)
                                }
                                observer.on(.next(outlets))
                            } else if success == false, let error_ = error {
                                observer.onError(error_)
                            } else {
                                observer.onError(ResponseError.unknownError as NSError)
                            }
                        })
                }, onError: {(error) in
                    observer.onError(error)
                })
            return Disposables.create {}
        }
    }
    
    func getOutletDetailsWithID(_ id: String) -> Observable<OutletDescription> {
        let params = ["id": id]
        return Observable.create{ observer in
            let outletObserver = ApiManager.get(path: .outletDetails, isUserAPI: false, params: params as Dictionary<String, AnyObject>)
            _ = outletObserver.retry(2)
                .subscribe(
                    onNext: {(responseObject) in
                        ApiManager.validateResponse(forResponse: responseObject, path: .outletDetails, successCompletionHandler: { (success, json, error) in
                            if success == true, let outletObject = json as? [String: AnyObject] {
                                do {
                                    let outletDescription: OutletDescription = try unbox(dictionary: outletObject)
                                    observer.on(.next(outletDescription))
                                } catch {
                                    observer.onError(ResponseError.unboxParseError)
                                }
                            } else if success == false, let error_ = error {
                                observer.onError(error_)
                            } else {
                                observer.onError(ResponseError.unknownError as NSError)
                            }
                        })
                }, onError: {(error) in
                    observer.onError(error)
                })
            return Disposables.create {}
        }
    }

    func getPromotionDetailsWithID(_ id: String) -> Observable<PromotionDescription> {
        let params = ["id": id]
        return Observable.create{ observer in
            let promotionObserver = ApiManager.get(path: .promotionDetails, isUserAPI: false, params: params as Dictionary<String, AnyObject>)
            _ = promotionObserver.retry(2)
                .subscribe(
                    onNext: {(responseObject) in
                        ApiManager.validateResponse(forResponse: responseObject, path: .promotionDetails, successCompletionHandler: { (success, json, error) in
                            if success == true, let promotionObject = json as? [String: AnyObject] {
                                do {
                                    let promoDescription: PromotionDescription = try unbox(dictionary: promotionObject)
                                    observer.on(.next(promoDescription))
                                } catch {
                                    observer.onError(ResponseError.unboxParseError)
                                }
                            } else if success == false, let error_ = error {
                                observer.onError(error_)
                            } else {
                                observer.onError(ResponseError.unknownError as NSError)
                            }
                        })
                }, onError: {(error) in
                    observer.onError(error)
                })
            return Disposables.create {}
        }
    }
    
    
    func fetchCheckEmailResponse(email: String)-> Observable<CheckEmailResponse>{
        let params = ["email": email]
        return Observable.create{ observer in
            let loginObserver = ApiManager.post(path: .checkEmail, isUserAPI: true, params: params as Dictionary<String, AnyObject>)
            _ = loginObserver.retry(2)
                .subscribe(
                    onNext: {(responseObject) in
                        ApiManager.validateResponse(forResponse: responseObject, path: .checkEmail, successCompletionHandler: { (success, json, error) in
                            if success == true, let userObject = json as? [String: AnyObject], userObject.count > 0 {
                                do {
                
                                    let userDict: NSMutableDictionary = userObject["data"] as! NSMutableDictionary
                                    userDict.setObject(userObject["balance"] as? String ?? "", forKey: "balance" as NSCopying)

                                    let user: CheckEmailResponse = try unbox(dictionary: userDict as! UnboxableDictionary)
                                    observer.on(.next(user))
                                } catch {
                                    observer.onError(ResponseError.unboxParseError)
                                }
                            } else if success == false, let error_ = error {
                                observer.onError(error_)
                            } else {
                                observer.onError(ResponseError.unknownError as NSError)
                            }
                        })
                }, onError: {(error) in
                    observer.onError(error)
                })
            return Disposables.create {}
        }
    }
    
    

    func loginUserWithEmail(_ email: String, andPassword password: String) -> Observable<User> {
        let params = ["email": email, "password": password]
        return Observable.create{ observer in
            let loginObserver = ApiManager.post(path: .login, isUserAPI: true, params: params as Dictionary<String, AnyObject>)
            _ = loginObserver.retry(2)
                .subscribe(
                    onNext: {(responseObject) in
                        ApiManager.validateResponse(forResponse: responseObject, path: .login, successCompletionHandler: { (success, json, error) in
                            if success == true, let userObject = json as? [[String: AnyObject]], userObject.count > 0 {
                                do {
                                    let userDict = userObject[0]
                                    let user: User = try unbox(dictionary: userDict)
                                    observer.on(.next(user))
                                } catch {
                                    observer.onError(ResponseError.unboxParseError)
                                }
                            } else if success == false, let error_ = error {
                                observer.onError(error_)
                            } else {
                                observer.onError(ResponseError.unknownError as NSError)
                            }
                        })
                }, onError: {(error) in
                    observer.onError(error)
                })
            return Disposables.create {}
        }
    }

    func registerUserWithEmail(_ email: String, firstName: String, lastName: String, password: String, gender: String, dateOfBirth: String, phoneNumber: String) -> Observable<User> {
        let params = ["email": email,
                      "password": password,
                      "firstname": firstName,
                      "lastname": lastName,
                      "dob": dateOfBirth,
                      "gender": gender,
                      "phonenum": phoneNumber]
        print(params)
        return Observable.create{ observer in
            let registerObserver = ApiManager.post(path: .register, isUserAPI: true, params: params as Dictionary<String, AnyObject>)
            _ = registerObserver.retry(2)
                .subscribe(
                    onNext: {(responseObject) in
                        ApiManager.validateResponse(forResponse: responseObject, path: .register, successCompletionHandler: { (success, json, error) in
                            if success == true, let userObject = json as? [[String: AnyObject]], userObject.count > 0 {
                                do {
                                    let userDict = userObject[0]
                                    let user: User = try unbox(dictionary: userDict)
                                    observer.on(.next(user))
                                } catch {
                                    observer.onError(ResponseError.unboxParseError)
                                }
                            } else if success == false, let error_ = error {
                                observer.onError(error_)
                            } else {
                                observer.onError(ResponseError.unknownError as NSError)
                            }
                        })
                }, onError: {(error) in
                    observer.onError(error)
                })
            return Disposables.create {}
        }
    }

    func editUserWithEmail(_ email: String, firstName: String, lastName: String, userId: String, dateOfBirth: String, gender: String, phoneNumber: String) -> Observable<User> {
        let params = ["email": email,
                      "id": userId,
                      "firstname": firstName,
                      "lastname": lastName,
                      "dob": dateOfBirth,
                      "gender": gender,
                      "phonenum": phoneNumber]
        return Observable.create{ observer in
            let editProfileObserver = ApiManager.post(path: .editProfile, isUserAPI: true, params: params as Dictionary<String, AnyObject>)
            _ = editProfileObserver.retry(2)
                .subscribe(
                    onNext: {(responseObject) in
                        ApiManager.validateResponse(forResponse: responseObject, path: .editProfile, successCompletionHandler: { (success, json, error) in
                            if success == true, let userObject = json as? [[String: AnyObject]], userObject.count > 0 {
                                do {
                                    let userDict = userObject[0]
                                    let user: User = try unbox(dictionary: userDict)
                                    observer.on(.next(user))
                                } catch {
                                    observer.onError(ResponseError.unboxParseError)
                                }
                            } else if success == false, let error_ = error {
                                observer.onError(error_)
                            } else {
                                observer.onError(ResponseError.unknownError as NSError)
                            }
                        })
                }, onError: {(error) in
                    observer.onError(error)
                })
            return Disposables.create {}
        }
    }

    func favoriteOutlet(withId id: String, userId: String) -> Observable<String> {
        let params = ["outlet_id": id,
                      "user_id": userId]
        return Observable.create{ observer in
            let favOutletObserver = ApiManager.post(path: .favUnFavOutlet, isUserAPI: false, params: params as Dictionary<String, AnyObject>)
            _ = favOutletObserver.retry(2)
                .subscribe(
                    onNext: {(responseObject) in
                        ApiManager.validateResponse(forResponse: responseObject, path: .favUnFavOutlet, successCompletionHandler: { (success, json, error) in
                            if success == true, let jsonObject = json as? [String: String], let message = jsonObject["message"]  {
                                observer.onNext(message)
                            } else if success == false, let error_ = error {
                                observer.onError(error_)
                            } else {
                                observer.onError(ResponseError.unknownError as NSError)
                            }
                        })
                }, onError: {(error) in
                    observer.onError(error)
                })
            return Disposables.create {}
        }
    }

    func unFavoriteOutlet(withId id: String) -> Observable<String> {
        let params = ["appendId": id]
        return Observable.create{ observer in
            let unFavOutletObserver = ApiManager.delete(path: .favUnFavOutlet, isUserAPI: false, params: params as Dictionary<String, AnyObject>)
            _ = unFavOutletObserver.retry(2)
                .subscribe(
                    onNext: {(responseObject) in
                        ApiManager.validateResponse(forResponse: responseObject, path: .favUnFavOutlet, successCompletionHandler: { (success, json, error) in
                            if success == true, let jsonObject = json as? [String: String], let message = jsonObject["message"]  {
                                observer.onNext(message)
                            } else if success == false, let error_ = error {
                                observer.onError(error_)
                            } else {
                                observer.onError(ResponseError.unknownError as NSError)
                            }
                        })
                }, onError: {(error) in
                    observer.onError(error)
                })
            return Disposables.create {}
        }
    }

    func favoritePromotion(withId id: String, userId: String) -> Observable<String> {
        let params = ["promotion_id": id,
                      "user_id": userId]
        return Observable.create{ observer in
            let unFavOutletObserver = ApiManager.post(path: .favUnFavPromotions, isUserAPI: false, params: params as Dictionary<String, AnyObject>)
            _ = unFavOutletObserver.retry(2)
                .subscribe(
                    onNext: {(responseObject) in
                        ApiManager.validateResponse(forResponse: responseObject, path: .favUnFavPromotions, successCompletionHandler: { (success, json, error) in
                            if success == true, let jsonObject = json as? [String: String], let message = jsonObject["message"]  {
                                observer.onNext(message)
                            } else if success == false, let error_ = error {
                                observer.onError(error_)
                            } else {
                                observer.onError(ResponseError.unknownError as NSError)
                            }
                        })
                }, onError: {(error) in
                    observer.onError(error)
                })
            return Disposables.create {}
        }
    }
    
    func unFavoritePromotion(withId id: String) -> Observable<String> {
        let params = ["appendId": id]
        return Observable.create{ observer in
            let unFavOutletObserver = ApiManager.delete(path: .favUnFavPromotions, isUserAPI: false, params: params as Dictionary<String, AnyObject>)
            _ = unFavOutletObserver.retry(2)
                .subscribe(
                    onNext: {(responseObject) in
                        ApiManager.validateResponse(forResponse: responseObject, path: .favUnFavPromotions, successCompletionHandler: { (success, json, error) in
                            if success == true, let jsonObject = json as? [String: String], let message = jsonObject["message"]  {
                                observer.onNext(message)
                            } else if success == false, let error_ = error {
                                observer.onError(error_)
                            } else {
                                observer.onError(ResponseError.unknownError as NSError)
                            }
                        })
                }, onError: {(error) in
                    observer.onError(error)
                })
            return Disposables.create {}
        }
    }
    
    func forgotPasswordForEmail(_ email: String) -> Observable<String> {
        let params = ["email": email]
        return Observable.create{ observer in
            let forgotPassObserver = ApiManager.post(path: .forgotPassword, isUserAPI: true, params: params as Dictionary<String, AnyObject>)
            _ = forgotPassObserver.retry(2)
                .subscribe(
                    onNext: {(responseObject) in
                        ApiManager.validateResponse(forResponse: responseObject, path: .forgotPassword, successCompletionHandler: { (success, json, error) in
                            if success == true, let responseObject = json as? [[String: AnyObject]], responseObject.count > 0 {
                                let responseDict = responseObject[0]
                                observer.on(.next(responseDict["message"] as! String))
                            } else if success == false, let error_ = error {
                                observer.onError(error_)
                            } else {
                                observer.onError(ResponseError.unknownError as NSError)
                            }
                        })
                }, onError: {(error) in
                    observer.onError(error)
                })
            return Disposables.create {}
        }
    }
    
    func changePassword(_ password: String) -> Observable<String> {
        let params = ["password": password,
                      "confirmation": password,
                      "id": UserStore.shared.userId]
        return Observable.create{ observer in
            let changePasswordObserver = ApiManager.post(path: .changePassword, isUserAPI: true, params: params as Dictionary<String, AnyObject>)
            _ = changePasswordObserver.retry(2)
                .subscribe(
                    onNext: {(responseObject) in
                        ApiManager.validateResponse(forResponse: responseObject, path: .changePassword, successCompletionHandler: { (success, json, error) in
                            if success == true, let responseDict = json as? [String: String] {
                                observer.on(.next(responseDict["message"]!))
                            } else if success == false, let error_ = error {
                                observer.onError(error_)
                            } else {
                                observer.onError(ResponseError.unknownError as NSError)
                            }
                        })
                }, onError: {(error) in
                    observer.onError(error)
                })
            return Disposables.create {}
        }
    }

    func registerDeviceToken(_ deviceToken: String) -> Observable<String> {
        var params = ["token": deviceToken,
                      "platform": "ios"]
        if UserStore.shared.isLoggedIn == true {
            params["user_id"] = UserStore.shared.userId
        }
        return Observable.create{ observer in
            let deviceObserver = ApiManager.post(path: .registerDeviceToken, isUserAPI: false, params: params as Dictionary<String, AnyObject>)
            _ = deviceObserver.retry(2)
                .subscribe(
                    onNext: {(responseObject) in
                        ApiManager.validateResponse(forResponse: responseObject, path: .registerDeviceToken, successCompletionHandler: { (success, json, error) in
                            if success == true, let responseDict = json as? [String: String] {
                                observer.on(.next(responseDict["message"]!))
                            } else if success == false, let error_ = error {
                                observer.onError(error_)
                            } else {
                                observer.onError(ResponseError.unknownError as NSError)
                            }
                        })
                }, onError: {(error) in
                    observer.onError(error)
                })
            return Disposables.create {}
        }
    }
    
    func logoutUser() -> Observable<Bool> {
        let params = ["id": UserStore.shared.userId]
        return Observable.create{ observer in
            let logoutObserver = ApiManager.get(path: .logout, isUserAPI: false, params: params as Dictionary<String, AnyObject>)
            _ = logoutObserver.retry(2)
                .subscribe(
                    onNext: {(responseObject) in
                        ApiManager.validateResponse(forResponse: responseObject, path: .logout, successCompletionHandler: { (success, json, error) in
                            if success == true, let _ = json as? [String: String] {
                                observer.on(.next(true))
                            } else if success == false, let error_ = error {
                                observer.onError(error_)
                            } else {
                                observer.onError(ResponseError.unknownError as NSError)
                            }
                        })
                }, onError: {(error) in
                    observer.onError(error)
                })
            return Disposables.create {}
        }
    }

    func fetchNotifications(pageNumber: String) -> Observable<NotificationPagination> {
        return Observable.create{ observer in
            let notificationObserver = ApiManager.get(path: .notifications, isUserAPI: false, params: nil)
            _ = notificationObserver.retry(2)
                .subscribe(
                    onNext: {(responseObject) in
                        ApiManager.validateResponse(forResponse: responseObject, path: .notifications, successCompletionHandler: { (success, json, error) in
                            if success == true, let responseArray = json {
                                do {
                                    let dict = ["data": responseArray]
                                    let notificationPagination: NotificationPagination = try unbox(dictionary: dict)
                                    observer.on(.next(notificationPagination))
                                } catch {
                                    observer.onError(ResponseError.unboxParseError)
                                }
                            } else if success == false, let error_ = error {
                                observer.onError(error_)
                            } else {
                                observer.onError(ResponseError.unknownError as NSError)
                            }
                        })
                }, onError: {(error) in
                    observer.onError(error)
                })
            return Disposables.create {}
        }
    }

    func fetchMyBookings(pageNumber: String) -> Observable<MyBookingsPagination> {
        
        let params = ["user_id": UserStore.shared.userId, "page": pageNumber]
        return Observable.create{ observer in
            let myBookingsObserver = ApiManager.get(path: .bookings, isUserAPI: false, params: params as Dictionary<String, AnyObject>)
            _ = myBookingsObserver.retry(2)
                .subscribe(
                    onNext: {(responseObject) in
                        ApiManager.validateResponse(forResponse: responseObject, path: .bookings, successCompletionHandler: { (success, json, error) in
                            if success == true, let responseArray = json {
                                do {
                                    let dict = ["data": responseArray]
                                    let myBookingsPagination: MyBookingsPagination = try unbox(dictionary: dict)
                                    observer.on(.next(myBookingsPagination))
                                } catch {
                                    observer.onError(ResponseError.unboxParseError)
                                }
                            } else if success == false, let error_ = error {
                                observer.onError(error_)
                            } else {
                                observer.onError(ResponseError.unknownError as NSError)
                            }
                        })
                }, onError: {(error) in
                    observer.onError(error)
                })
            return Disposables.create {}
        }
    }
    
    func fetchCustomerDetails() -> Observable<User> {
        let params = ["email": UserStore.shared.emailId,
                      "customer_id": UserStore.shared.userId]
        
        return Observable.create{ observer in
            let etchCustomerObserver = ApiManager.post(path: .userProfile, isUserAPI: true, params: params as Dictionary<String, AnyObject>)
            _ = etchCustomerObserver.retry(2)
                .subscribe(
                    onNext: {(responseObject) in
                        ApiManager.validateResponse(forResponse: responseObject, path: .userProfile, successCompletionHandler: { (success, json, error) in
                            if success == true, let userObject = json as? [[String: AnyObject]], userObject.count > 0 {
                                do {
                                    let userDict = userObject[0]
                                    let user: User = try unbox(dictionary: userDict)
                                    observer.on(.next(user))
                                } catch {
                                    observer.onError(ResponseError.unboxParseError)
                                }
                            } else if success == false, let error_ = error {
                                observer.onError(error_)
                            } else {
                                observer.onError(ResponseError.unknownError as NSError)
                            }
                        })
                }, onError: {(error) in
                    observer.onError(error)
                })
            return Disposables.create {}
        }
    }
    
    func fetchDashboardDetails() -> Observable<CustomerInfoPagination> {
        let params = ["id": UserStore.shared.userId,
                      "page": "1"]
//        if UserStore.shared.emailId == "phaninderkumar.m@gmail.com" {
//            params["id"] = "9976"
//        }

        return Observable.create{ observer in
            let fetchDashboardObserver = ApiManager.post(path: .customerReward, isUserAPI: true, params: params as Dictionary<String, AnyObject>)
            _ = fetchDashboardObserver.retry(2)
                .subscribe(
                    onNext: {(responseObject) in
                        ApiManager.validateResponse(forResponse: responseObject, path: .customerReward, successCompletionHandler: { (success, json, error) in
                            if success == true, let dashboardObject = json as? [[String: AnyObject]], dashboardObject.count > 0 {
                                do {
                                    let dashboardDict = dashboardObject[0]
                                    let pagination: CustomerInfoPagination = try unbox(dictionary: dashboardDict)
                                    observer.on(.next(pagination))
                                } catch {
                                    observer.onError(ResponseError.unboxParseError)
                                }
                            } else if success == false, let error_ = error {
                                observer.onError(error_)
                            } else {
                                observer.onError(ResponseError.unknownError as NSError)
                            }
                        })
                }, onError: {(error) in
                    observer.onError(error)
                })
            return Disposables.create {}
        }
    }

//    func fetchOrders(pageNumber: String) -> Observable<CustomerInfoPagination> {
//        let params = ["id": UserStore.shared.userId,
//                      "page": pageNumber]
////        if UserStore.shared.emailId == "phaninderkumar.m@gmail.com" {
////            params["id"] = "9976"
////        }
//
//        return Observable.create{ observer in
//            let orderObserver = ApiManager.post(path: .customerReward, isUserAPI: true, params: params as Dictionary<String, AnyObject>)
//            _ = orderObserver.retry(2)
//                .subscribe(
//                    onNext: {(responseObject) in
//                        ApiManager.validateResponse(forResponse: responseObject, path: .customerReward, successCompletionHandler: { (success, json, error) in
//                            if success == true, let dashboardObject = json as? [[String: AnyObject]], dashboardObject.count > 0 {
//                                do {
//                                    let dashboardDict = dashboardObject[0]
//                                    let pagination: CustomerInfoPagination = try unbox(dictionary: dashboardDict)
//                                    observer.on(.next(pagination))
//                                } catch {
//                                    observer.onError(ResponseError.unboxParseError)
//                                }
//                            } else if success == false, let error_ = error {
//                                observer.onError(error_)
//                            } else {
//                                observer.onError(ResponseError.unknownError as NSError)
//                            }
//                        })
//                }, onError: {(error) in
//                    observer.onError(error)
//                })
//            return Disposables.create {}
//        }
//    }
    
    func editProfileWithImage(params: [String: String], image: UIImage) -> Observable<User> {
        return Observable.create{ observer in
            let editObserver = ApiManager.createRequest(params: params, chosenImage: image, path: URI.editProfile.rawValue)
            _ = editObserver.retry(2)
                .subscribe(
                    onNext: {(responseObject) in
                        ApiManager.validateResponse(forResponse: responseObject, path: .editProfile, successCompletionHandler: { (success, json, error) in
                            if success == true, let userObject = json as? [[String: AnyObject]], userObject.count > 0 {
                                do {
                                    let userDict = userObject[0]
                                    let user: User = try unbox(dictionary: userDict)
                                    observer.on(.next(user))
                                } catch {
                                    observer.onError(ResponseError.unboxParseError)
                                }
                            } else if success == false, let error_ = error {
                                observer.onError(error_)
                            } else {
                                observer.onError(ResponseError.unknownError as NSError)
                            }
                        })
                }, onError: {(error) in
                    observer.onError(error)
                })
            return Disposables.create {}
        }
    }
    
    func fetchBadges(_ userId: String) -> Observable<[Badge]> {
        let params = ["entity_id": userId]
        return Observable.create{ observer in
            let badgeObserver = ApiManager.get(path: .badges, isUserAPI: true, params: params as Dictionary<String, AnyObject>)
            _ = badgeObserver.retry(2)
                .subscribe(
                    onNext: {(responseObject) in
                        ApiManager.validateResponse(forResponse: responseObject, path: .badges, successCompletionHandler: { (success, json, error) in
                            if success == true, let badgeObject = json as? [[String: AnyObject]] {
                                var badges = [Badge]()
                                for badgeDict in badgeObject {
                                    do {
                                        let badge: Badge = try unbox(dictionary: badgeDict)
                                        badges.append(badge)
                                    } catch {
                                        //Do nothing
                                    }
                                }
                                observer.onNext(badges)
                            } else if success == false, let error_ = error {
                                observer.onError(error_)
                            } else {
                                observer.onError(ResponseError.unknownError as NSError)
                            }
                        })
                }, onError: {(error) in
                    observer.onError(error)
                })
            return Disposables.create {}
        }
    }
    
    func reserveAnOutlet(_ params: [String: String]) -> Observable<String> {

        print(params)
        return Observable.create{ observer in
            let badgeObserver = ApiManager.post(path: .bookings, isUserAPI: false, params: params as Dictionary<String, AnyObject>)
            _ = badgeObserver.retry(2)
                .subscribe(
                    onNext: {(responseObject) in
                        ApiManager.validateResponse(forResponse: responseObject, path: .bookings, successCompletionHandler: { (success, json, error) in
                            if success == true, let jsonObject = json as? [String: String], let message = jsonObject["message"]  {
                                observer.onNext(message)
                            } else if success == false, let error_ = error {
                                observer.onError(error_)
                            } else {
                                observer.onError(ResponseError.unknownError as NSError)
                            }
                        })
                }, onError: {(error) in
                    observer.onError(error)
                })
            return Disposables.create {}
        }
    }

    func fetchDashBoardPromotions(pageNumber: String) -> Observable<DashBoardPromotion> {
        let params = ["page": pageNumber]
        return Observable.create{ observer in
            let outletObserver = ApiManager.get(path: .favUnFavPromotions, isUserAPI: false, params: params as Dictionary<String, AnyObject>)
            _ = outletObserver.retry(2)
                .subscribe(
                    onNext: {(responseObject) in
                        ApiManager.validateResponse(forResponse: responseObject, path: .favUnFavPromotions, successCompletionHandler: { (success, json, error) in
                            
                            if success == true, let promotionsDict = json as? [String: AnyObject], let promotionsArray = promotionsDict["promotions"] as? [[String: AnyObject]] {
                                let promotionObject = ["data": promotionsArray]
                                do {
                                    let promotionListPagination: PromotionPagination = try unbox(dictionary: promotionObject)
                                    let dashboardPromotion = (heading: promotionsDict["heading"] as? String ?? "Saved Promotions", promotions: promotionListPagination)
                                    observer.on(.next(dashboardPromotion))
                                } catch {
                                    observer.onError(ResponseError.unboxParseError)
                                }
                            } else if success == false, let error_ = error {
                                observer.onError(error_)
                            } else {
                                observer.onError(ResponseError.unknownError as NSError)
                            }
                        })
                }, onError: {(error) in
                    observer.onError(error)
                })
            return Disposables.create {}
        }
    }

    func fetchDashBoardOutlets(pageNumber: String) -> Observable<DashBoardOutlet> {
        let params = ["page": pageNumber]
        return Observable.create{ observer in
            let outletObserver = ApiManager.get(path: .favUnFavOutlet, isUserAPI: false, params: params as Dictionary<String, AnyObject>)
            _ = outletObserver.retry(2)
                .subscribe(
                    onNext: {(responseObject) in
                        ApiManager.validateResponse(forResponse: responseObject, path: .favUnFavOutlet, successCompletionHandler: { (success, json, error) in
                            
                            if success == true, let outletsDict = json as? [String: AnyObject], let outletsArray = outletsDict["outlets"] as? [[String: AnyObject]] {
                                let outletObject = ["data": outletsArray]
                                do {
                                    let outletListPagination: OutletPagination = try unbox(dictionary: outletObject)
                                    let dashboardOutlet = (heading: outletsDict["heading"] as? String ?? "Saved Outlets", outlets: outletListPagination)
                                    observer.on(.next(dashboardOutlet))
                                } catch {
                                    observer.onError(ResponseError.unboxParseError)
                                }
                            } else if success == false, let error_ = error {
                                observer.onError(error_)
                            } else {
                                observer.onError(ResponseError.unknownError as NSError)
                            }
                        })
                }, onError: {(error) in
                    observer.onError(error)
                })
            return Disposables.create {}
        }
    }

    func fetchOrders(pageNumber: String) -> Observable<OrderPagination> {
        let params = ["entity_id": UserStore.shared.userId,
                      "page": pageNumber,
                      "page_size": "10",
                       "t": String(Date().timeIntervalSince1970)]

        return Observable.create{ observer in
            let orderObserver = ApiManager.get(path: .transhistory, isUserAPI: true, params: params as Dictionary<String, AnyObject>)
            _ = orderObserver.retry(2)
                .subscribe(
                    onNext: {(responseObject) in
                        ApiManager.validateResponse(forResponse: responseObject, path: .transhistory, successCompletionHandler: { (success, json, error) in
                            if success == true, let responseArray = json {
                                do {
                                    let dict = ["data": responseArray]
                                    let orderPagination: OrderPagination = try unbox(dictionary: dict)
                                    observer.on(.next(orderPagination))
                                } catch {
                                    observer.onError(ResponseError.unboxParseError)
                                }
                            } else if success == false, let error_ = error {
                                observer.onError(error_)
                            } else {
                                observer.onError(ResponseError.unknownError as NSError)
                            }
                        })
                }, onError: {(error) in
                    observer.onError(error)
                })
            return Disposables.create {}
        }
    }

    
    
}
