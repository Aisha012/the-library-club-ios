//
//  Constants.swift
//  TheLibraryClub
//
//  Created by Phaninder on 26/11/17.
//  Copyright © 2017 Phaninder. All rights reserved.
//

import Foundation
import UIKit

let headers = ["Accept" : "application/json",
               "Content-Type" : "application/x-www-form-urlencoded",
               "Os-Type": "ios"]
//13.250.2.145
let appBaseURL = "http://dashboard.librarycard.club/api" //"http://13.250.2.145/api" //"http://13.229.208.204/api"//
let userBaseURL = "http://librarycard.club"
//let userBaseURL = "http://acecard.club"



//MARK: - UserDefault Keys -

let CupsCount = "CupsCount"
let IsLoggedIn = "IsLoggedIn"
let SkipLogin = "SkipLogin"
let ServerError = "ServerError"
let LoginType = "LoginType"
let FacebookId = "FacebookId"
let UserId = "UserId"
let EmailId = "EmailId"
let FirstName = "FirstName"
let LastName = "LastName"
let ProfilePicture = "ProfilePicture"
let DateOfBirth = "DateOfBirth"
let Gender = "Gender"
let PhoneNumber = "PhoneNumber"
let CardNumber = "CardNumber"
let PreferredLanguage = "PreferredLanguage"
let LocationPreviousTime = "LocationPreviousTime"
let UpdatePreviousTime = "UpdatePreviousTime"
let BadgeNumber = "BadgeNumber"
let DeviceToken = "DeviceToken"
let LanguageSet = "LanguageSet"

let InvalidAuthTokenErrorCode = 403
let ForceAppUpdateErrorCode = 412

let successCode = 200
let successCode2 = 201

let AppName = "ACE Card".localized

typealias ResponseObject = (response: HTTPURLResponse, data: Data?)
typealias DashBoardPromotion = (heading: String, promotions: PromotionPagination)
typealias DashBoardOutlet = (heading: String, outlets: OutletPagination)
typealias UpdateAppObject = (isUpdatedAvailable: Bool, link: String)


//5* 24 * 60 * 60
let fiveDaysInSeconds = 432000

//MARK: - Colors -

//let darkBlueColor =  UIColor(colorLiteralRed: 0, green: 134/255, blue: 179/255, alpha: 1)
//let themeColor =  UIColor(colorLiteralRed: 0, green: 167/255, blue: 225/255, alpha: 1)
//let borderViewDefaultColor = UIColor(colorLiteralRed: 235/255, green: 235/255, blue: 235/255, alpha: 1)

//MARK: - Dimension Constants -

let ScreenWidth = UIScreen.main.bounds.width
let ScreenHeight = UIScreen.main.bounds.height

//MARK: - Validation Constants -

let PasswordMinLimit = 6
let PasswordMaxLimit = 32
let EmailMaxLimit = 254
let NameMinLimit = 1
let NameMaxLimit = 85
let PhoneNumberMinLimit = 7

extension Notification.Name {
    static let profileUpdated = Notification.Name("profileUpdated")
    static let promoUnfavorited = Notification.Name("promoUnfavorited")
    static let outletUnfavorited = Notification.Name("outletUnfavorited")
    static let promoFavorited = Notification.Name("promoFavorited")
    static let outletFavorited = Notification.Name("outletFavorited")
    static let notificationCountChanged = Notification.Name("notificationCountChanged")

}

//MARK: - URL Constants -
let termsAndConditionsURL = "http://acecard.club/index.php/terms"//"http://librarycard.club/index.php/terms"
let privacyPolicyURL = "http://acecard.club/index.php/privacy"//"http://librarycard.club/index.php/privacy"
let faqURL = "http://acecard.club/index.php/faq"
let contactUsURL = "http://acecard.club/index.php/contacts"
let facebookURL = "https://www.facebook.com/thelibrarycard/"
let instagramURL = "https://www.instagram.com/get_sip/?hl=en"
