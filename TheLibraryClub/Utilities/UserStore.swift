//
//  UserStore.swift
//  TheLibraryClub
//
//  Created by Phaninder on 26/11/17.
//  Copyright © 2017 Phaninder. All rights reserved.
//

import Foundation

class UserStore: NSObject {
    
    static let shared = UserStore()
    let userDefaults = UserDefaults.standard
    
    fileprivate override init() {
        super.init()
    }
    
    var isLoggedIn: Bool {
        set {
            userDefaults.setValue(newValue, forKey: IsLoggedIn)
            userDefaults.synchronize()
        }
        get {
            return userDefaults.value(forKey: IsLoggedIn) as? Bool ?? false
        }
    }


    var userId: String {
        set {
            userDefaults.setValue(newValue, forKey: UserId)
            userDefaults.synchronize()
        }
        get {
            return userDefaults.value(forKey: UserId) as? String ?? ""
        }
    }
    
    var emailId: String {
        set {
            userDefaults.setValue(newValue, forKey: EmailId)
            userDefaults.synchronize()
        }
        get {
            return userDefaults.value(forKey: EmailId) as? String ?? ""
        }
    }

    var firstName: String {
        set {
            userDefaults.setValue(newValue, forKey: FirstName)
            userDefaults.synchronize()
        }
        get {
            return userDefaults.value(forKey: FirstName) as? String ?? ""
        }
    }

    var lastName: String {
        set {
            userDefaults.setValue(newValue, forKey: LastName)
            userDefaults.synchronize()
        }
        get {
            return userDefaults.value(forKey: LastName) as? String ?? ""
        }
    }
    
    var profilePic: String {
        set {
            userDefaults.setValue(newValue, forKey: ProfilePicture)
            userDefaults.synchronize()
        }
        get {
            return userDefaults.value(forKey: ProfilePicture) as? String ?? ""
        }
    }

    var DOB: String {
        set {
            userDefaults.setValue(newValue, forKey: DateOfBirth)
            userDefaults.synchronize()
        }
        get {
            return userDefaults.value(forKey: DateOfBirth) as? String ?? "Not Set"
        }
    }

    var gender: String {
        set {
            userDefaults.setValue(newValue, forKey: Gender)
            userDefaults.synchronize()
        }
        get {
            return userDefaults.value(forKey: Gender) as? String ?? "Not Set"
        }
    }

    var phoneNumber: String {
        set {
            userDefaults.setValue(newValue, forKey: PhoneNumber)
            userDefaults.synchronize()
        }
        get {
            return userDefaults.value(forKey: PhoneNumber) as? String ?? ""
        }
    }

    var cardNumber: String {
        set {
            userDefaults.setValue(newValue, forKey: CardNumber)
            userDefaults.synchronize()
        }
        get {
            return userDefaults.value(forKey: CardNumber) as? String ?? ""
        }
    }

    var userLanguage: String {
        set {
            userDefaults.setValue(newValue, forKey: PreferredLanguage)
            userDefaults.synchronize()
        }
        get {
            return userDefaults.value(forKey: PreferredLanguage) as? String ?? "en"
        }
    }
    
    var isLanguageSetManually: Bool {
        set {
            userDefaults.setValue(newValue, forKey: LanguageSet)
            userDefaults.synchronize()
        }
        get {
            return userDefaults.value(forKey: LanguageSet) as? Bool ?? false
        }
    }

    var deviceToken: String {
        set {
            userDefaults.setValue(newValue, forKey: DeviceToken)
            userDefaults.synchronize()
        }
        get {
            return userDefaults.value(forKey: DeviceToken) as? String ?? ""
        }
    }
    
    
    
    var isServerError: Bool {
        set {
            userDefaults.setValue(newValue, forKey: ServerError)
            userDefaults.synchronize()
        }
        get {
            return userDefaults.value(forKey: ServerError) as? Bool ?? false
        }
    }

    var locationPreviouslyShown: Int {
        set {
            userDefaults.setValue(newValue, forKey: LocationPreviousTime)
            userDefaults.synchronize()
        }
        get {
            return userDefaults.value(forKey: LocationPreviousTime) as? Int ?? Int(Date().timeIntervalSince1970)
        }
    }
    
    var updatePreviousChecked: Int {
        set {
            userDefaults.setValue(newValue, forKey: UpdatePreviousTime)
            userDefaults.synchronize()
        }
        get {
            return userDefaults.value(forKey: UpdatePreviousTime) as? Int ?? Int(Date().timeIntervalSince1970)
        }
    }
    
    var badgeNumber: Int {
        set {
            userDefaults.setValue(newValue, forKey: BadgeNumber)
            userDefaults.synchronize()
        }
        get {
            return userDefaults.value(forKey: BadgeNumber) as? Int ?? 0
        }
    }

}

