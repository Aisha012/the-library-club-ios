//
//  Utilities.swift
//  TheLibraryClub
//
//  Created by Phaninder on 27/11/17.
//  Copyright © 2017 Phaninder. All rights reserved.
//

import Foundation
import Reachability
import Toaster
import RxSwift

class Utilities: NSObject {
    
    static let shared = Utilities()
    var reachability: Reachability?
    var disposableBag = DisposeBag()

    fileprivate override init() {
        super.init()
    }
    
    class func getStoryBoard(withName name: Storyboard!) -> UIStoryboard? {
        return UIStoryboard(name: name.rawValue, bundle: nil)
    }
    
    class func showHUD(forView view: UIView!, excludeViews: [UIView?]) {
        let hudView = MBProgressHUD.showAdded(to: view, animated: true)
        view.bringSubview(toFront: hudView)
        if !excludeViews.isEmpty {
            for subView in excludeViews {
                view.bringSubview(toFront: subView!)
            }
        }
    }
    
    class func showToast(withString string: String) {
        Toast(text: string, duration: Delay.short).show()
    }
    
    class func hideHUD(forView view: UIView!) {
        MBProgressHUD.hide(for: view, animated: true);
    }
    
    class func getGroupsArrayFromResponse(_ dict: [String: AnyObject]) -> [[String: Any]] {
        
        if let resultsDict = dict["results"] as? [String: AnyObject],
            let resultDict = resultsDict["results"] as? [String: AnyObject],
            let groupedDict = resultDict["grouped"] as? [String: AnyObject],
            let physicalCardDict = groupedDict["physical_card_id"] as? [String: AnyObject],
            let cardArray = physicalCardDict["groups"] as? [[String: Any]] {
            return cardArray
        } else {
            print("Error while getting card dicts")
            return [[:]]
        }
    }
    
    class func logoutUser() {
        DispatchQueue.main.async(execute: {
            UserStore.shared.isLoggedIn = false
            UserStore.shared.userId = ""
            UserStore.shared.firstName = ""
            UserStore.shared.lastName = ""
            UserStore.shared.emailId = ""
            UserStore.shared.profilePic = ""
            UserStore.shared.DOB = ""
            UserStore.shared.gender = ""
            UserStore.shared.phoneNumber = ""
            UserStore.shared.cardNumber = ""
            UserStore.shared.deviceToken = ""
        })
    }
    
    class func hasTwoDaysPassed() -> Bool {
        let twoDaysFromNow = Int(Date().timeIntervalSince1970) - 2 * 24 * 60 * 60
        return UserStore.shared.locationPreviouslyShown <= twoDaysFromNow
    }
    
    @objc func navigateToServerErrorScreen() {
    }
    
    class func randomString(length: Int) -> String {
        
        let letters : NSString = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        let len = UInt32(letters.length)
        
        var randomString = ""
        
        for _ in 0 ..< length {
            let rand = arc4random_uniform(len)
            var nextChar = letters.character(at: Int(rand))
            randomString += NSString(characters: &nextChar, length: 1) as String
        }
        
        return randomString
    }
    
    class func getUnderLineTextWithFontFamily(_ familyName: String, ofFontSize size: CGFloat, andText text: String) -> NSMutableAttributedString {
        let attrs = [
            NSAttributedStringKey.font : UIFont.init(name: familyName, size: size)!,
            NSAttributedStringKey.underlineStyle : 1,
            NSAttributedStringKey.foregroundColor : UIColor.black,] as [NSAttributedStringKey : Any]
        
        return NSMutableAttributedString(string:text, attributes:attrs)

    }
    
    fileprivate func initializeReachability() {
        let reachability = Reachability()
        self.reachability = reachability
        
        do {
            try self.reachability!.startNotifier()
        } catch {
            print("Unable to start notifier")
        }
    }
    
    func setupReachability() {
        self.initializeReachability()
    }
    
    func isNetworkReachable() -> Bool {
        return reachability?.connection != .none
    }
    
    class func isValidString(_ string: String?) -> Bool {
        if let string_ = string, !string_.isEmpty {
            return true
        }
        return false
    }
    
    class func colorFromHex(_ rgbValue:UInt32, alpha:Double=1.0) -> UIColor {
        let red = CGFloat((rgbValue & 0xFF0000) >> 16)/256.0
        let green = CGFloat((rgbValue & 0xFF00) >> 8)/256.0
        let blue = CGFloat(rgbValue & 0xFF)/256.0
        
        return UIColor(red:red, green:green, blue:blue, alpha:CGFloat(alpha))
    }

    class func getHoursAndMinutesFromTimeString(_ time: String) -> (hours: Int, minutes: Int, isAM: Bool, hasError: Bool){
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "h:mm a"
        if let date = dateFormatter.date(from: time) {
            let calendar = Calendar.current
            
            let hour = calendar.component(.hour, from: date)
            let minutes = calendar.component(.minute, from: date)
            let isAM = time.suffix(2) == "AM"
            return (hours: hour, minutes: minutes, isAM: isAM, hasError: false)
        } else {
            return (hours: 0, minutes: 0, isAM: true, hasError: true)
        }
    }
    
    class func getTimeInformationFromInterval(_ interval: String) -> (hour1: Int, minutes1: Int, isAM1: Bool, hour2: Int, minutes2: Int, isAM2: Bool, hasError: Bool) {
        let timeArray = interval.components(separatedBy: " - ")
        if timeArray.count == 2 {
            let timeInfo1 = Utilities.getHoursAndMinutesFromTimeString(timeArray[0])
            if timeInfo1.hasError == false {
                
                let timeInfo2 = Utilities.getHoursAndMinutesFromTimeString(timeArray[1])
                if timeInfo2.hasError == false {
                    return (hour1: timeInfo1.hours, minutes1: timeInfo1.minutes, isAM1: timeInfo1.isAM, hour2: timeInfo2.hours, minutes2: timeInfo2.minutes, isAM2: timeInfo2.isAM, hasError: false)
                }
                return (hour1: 0, minutes1: 0, isAM1: true, hour2: 0, minutes2: 0, isAM2: true, hasError: true)
            }
            return (hour1: 0, minutes1: 0, isAM1: true, hour2: 0, minutes2: 0, isAM2: true, hasError: true)
        }
        return (hour1: 0, minutes1: 0, isAM1: true, hour2: 0, minutes2: 0, isAM2: true, hasError: true)
    }
    
    class func getTimeDetailsFromDate(_ date: Date) -> (hours: Int, minutes: Int, isAM: Bool, hasError: Bool) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "h:mm a"
        let timeString = dateFormatter.string(from: date)
        return Utilities.getHoursAndMinutesFromTimeString(timeString)
        
    }
    
    func setLanguageOfApp() {
        if UserStore.shared.isLanguageSetManually == false {
            var userLanguage = "en"
            if let langStr = Locale.preferredLanguages.first {
                if langStr.prefix(2) == "th" {
                    userLanguage = "th"
                }
            }
            print("Preferred language \(userLanguage)")
            UserStore.shared.userLanguage = userLanguage
        }
    }
    
    func registerDeviceToken() {
        guard UserStore.shared.isLoggedIn == true && !UserStore.shared.deviceToken.isEmpty else {
            return
        }
        UIApplication.shared.isNetworkActivityIndicatorVisible = true

        let deviceObserver = ApiManager.shared.apiService.registerDeviceToken(UserStore.shared.deviceToken)
        let deviceDisposable = deviceObserver.subscribe(onNext: {(message) in
            DispatchQueue.main.async(execute: {
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
            })
        }, onError: {(error) in
            DispatchQueue.main.async(execute: {
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
            })
        })
        deviceDisposable.disposed(by: disposableBag)
    }

}
