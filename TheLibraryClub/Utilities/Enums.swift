//
//  Enums.swift
//  TheLibraryClub
//
//  Created by Phaninder on 25/11/17.
//  Copyright © 2017 Phaninder. All rights reserved.
//

import Foundation
import UIKit

enum URI: String {
    case outletList = "/outlets"
    case promotionList = "/promotions"
    case outletLocations = "/outlets/index_map"
    case outletDetails = "/outlets/"
    case promotionDetails = "/promotions/"
    case login = "/mlogin.php"
    case register = "/mcustomerregistration.php"
    case editProfile = "/mcustomerupdate.php"
    case favUnFavOutlet = "/favourite_outlets"
    case favUnFavPromotions = "/favourite_promotions"
    case forgotPassword = "/forgot/index.php"
    case changePassword = "/mcustomerpasswordupdate.php"
    case registerDeviceToken = "/devices"
    case logout = "/logout/"
    case notifications = "/notifications"
    case userProfile = "/mcustomerdetails.php"
    case customerReward = "/mgetcustomertreward.php"
    case badges = "/badge.php"
    case bookings = "/bookings"
    case transhistory = "/transhistory.php"
    case checkEmail = "/mcheckemail.php"
}

enum APIServiceType {
    case apiService
}

enum ResponseError: Error {
    case notFoundError
    case badRequestError
    case timeoutError
    case internalServerError
    case parseError
    case unboxParseError
    case apiFailureError(message:String)
    case unknownError
    
    func description() -> String {
        switch self {
        case .notFoundError:
            return "URL not found!!!".localized
        case .badRequestError:
            return "ACE.API.BadRequest".localized
        case .timeoutError:
            return "ACE.API.RequestTimeout".localized
        case .internalServerError:
            return "ACE.API.InternalServerError".localized
        case .parseError:
            return "ACE.API.ParseError".localized
        case .unboxParseError:
            return "ACE.API.UnboxParseError".localized
        default:
            return "ACE.API.UnknownError".localized
        }
    }
}

enum ErrorMessage {
    case apiFailureError(message:String)
}

enum ResponseStatusCode:Int {
    case notFound = 404
    case badRequest = 400
    case timeout = 408
    case internalServer = 500
}

enum AccountInfoTableViewCellType: Int {
    case tappable = 0
    case nonTappable = 1
}

enum ProfileInfoDataType: Int {
    case firstName = 0,
    lastName,
    cardNumber,
    email,
    dob,
    gender,
    badges,
    phoneNumber,
    myQR,
    myBookings,
    help
}

enum HelpTableDataType: Int {
    case edit = 0, changePass, faq, contact, terms, privacy, facebook, language, version, logout
}


enum PaginationType: Int {
    case new, old, reload
}

enum FetchType: Int {
    case New = 0
    case More = 1
    case Reload = 2
}

enum LoginScreenActions: Int {
    case RememberUser = 1
    case Login = 2
    case ForgotPassword = 3
    case Register = 4
}

enum Storyboard: String {
    case Main = "Main"
}

enum ScreenID: String {
    case TabBarController = "tabBarController"
    case LoginViewController = "LoginViewController"
    case RegistrationViewController = "RegistrationViewController"
    case OutletDetailsViewController = "OutletDetailsViewController"
    case PromotionDetailViewController = "PromotionDetailViewController"
    case HelpAndSettingViewController = "HelpAndSettingViewController"
    case ChangePasswordViewController = "ChangePasswordViewController"
    case EditProfileViewController = "EditProfileViewController"
    case PromotionListingViewController = "PromotionListingViewController"
    case DashboardViewMoreViewController = "DashboardViewMoreViewController"
    case OutletListingViewController = "OutletListingViewController"
    case MapViewController = "MapViewController"
    case NotificationViewController = "NotificationViewController"
    case FilterViewController = "FilterViewController"
    case LanguageSettingsViewController = "LanguageSettingsViewController"
    case OrderHistoryViewController = "OrderHistoryViewController"
    case BadgeViewController = "BadgeViewController"
    case BookingViewController = "BookingViewController"
}

enum RegistrationTextFieldType: Int {
    case firstName = 0, lastName, email, password, rePassword, dob, gender, phoneNumber
    
}

enum UpdateProfileTextFieldType: Int {
    case firstName = 0, lastName, gender, phoneNumber
}

enum BookingScreenRowType: Int {
    case outlet = 0, date, time, promotion, persons, email, phone, remarks
}

enum BookingScreenTextFieldType: Int {
    case outlet = 0, date, time, promotion, persons, email, phone, allergies, others
}

