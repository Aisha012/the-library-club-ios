//
//  AppDelegate.swift
//  TheLibraryClub
//
//  Created by Phaninder on 17/01/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import UserNotifications
import FBSDKCoreKit
import FBSDKLoginKit
import GoogleMaps
import Fabric
import Crashlytics

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, UNUserNotificationCenterDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        IQKeyboardManager.sharedManager().enable = true
        GMSServices.provideAPIKey("AIzaSyB-L6x2Jo7tssHdF4j_6J9jRPAjL0fWQHk")
       // GMSPlacesClient.provideAPIKey("AIzaSyCyDO3ZbMIQe9qsweE81e0DbPdI98xyPkI")
        Utilities.shared.setupReachability()
        registerForPushNotifications()
        Fabric.with([Crashlytics.self])
        if #available(iOS 10.0, *) {
            UNUserNotificationCenter.current().delegate = self
        }

        FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)


        if let launchOptions = launchOptions,
            let remoteNotificationDict = launchOptions[UIApplicationLaunchOptionsKey.remoteNotification] as? [AnyHashable : Any], UserStore.shared.isLoggedIn == true {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let tabBarController = storyboard.instantiateViewController(withIdentifier: ScreenID.TabBarController.rawValue) as! UITabBarController
            
            tabBarController.selectedIndex = 0
            
            if let viewControllers = tabBarController.viewControllers, let nav = viewControllers[0] as? BaseNavigationController, let vc = nav.viewControllers[0] as? DashboardViewController {
                vc.userInfo = remoteNotificationDict
            }
            self.window = UIWindow(frame: UIScreen.main.bounds)
            self.window?.rootViewController = tabBarController
            self.window?.makeKeyAndVisible()
        } else if UserStore.shared.isLoggedIn == false {
            let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let initialViewController = mainStoryboard.instantiateViewController(withIdentifier: ScreenID.LoginViewController.rawValue) as! LoginViewController
            let navController = BaseNavigationController(rootViewController: initialViewController)
            self.window = UIWindow(frame: UIScreen.main.bounds)
            self.window?.rootViewController = navController
            self.window?.makeKeyAndVisible()
        } else {
            let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let tabBarController = mainStoryboard.instantiateViewController(withIdentifier: ScreenID.TabBarController.rawValue) as! UITabBarController
            
            tabBarController.selectedIndex = 0
            self.window = UIWindow(frame: UIScreen.main.bounds)
            self.window?.rootViewController = tabBarController
            self.window?.makeKeyAndVisible()
        }

        
        let statusBarView = UIView.init(frame: CGRect(x: 0, y: 0, width: ScreenWidth, height: 20))
        statusBarView.backgroundColor = UIColor.black
        window?.rootViewController?.view.addSubview(statusBarView)

        return true
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        Utilities.shared.registerDeviceToken()
        Utilities.shared.setLanguageOfApp()
    }

    func registerForPushNotifications() {
        if #available(iOS 10.0, *) {
            UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound, .badge]) { (granted, error) in
                guard granted else { return }
                self.getNotificationSettings()
            }
        } else {
            let setting = UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            UIApplication.shared.registerUserNotificationSettings(setting)
            UIApplication.shared.registerForRemoteNotifications()
            getNotificationSettings()
        }
    }
    
    func getNotificationSettings() {
        if #available(iOS 10.0, *) {
            UNUserNotificationCenter.current().getNotificationSettings { (settings) in
                guard settings.authorizationStatus == .authorized else { return }
                DispatchQueue.main.async {
                    UIApplication.shared.registerForRemoteNotifications()
                }
            }
        } else {
            if UIApplication.shared.isRegisteredForRemoteNotifications == false {
                UIApplication.shared.registerForRemoteNotifications()
            }
        }
    }
    
    
    func application(_ application: UIApplication,
                     didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        
        let tokenParts = deviceToken.map { data -> String in
            return String(format: "%02.2hhx", data)
        }
        
        let token = tokenParts.joined()
        UserDefaults.standard.set(token, forKey: "DeviceToken")
        UserDefaults.standard.synchronize()
        Utilities.shared.registerDeviceToken()
        print("Device Token: \(token)")
    }
    
    func application(_ application: UIApplication,
                     didFailToRegisterForRemoteNotificationsWithError error: Error) {
        UserDefaults.standard.set("", forKey: "DeviceToken")
        print("Failed to register: \(error)")
    }
    
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        let loginManager: FBSDKLoginManager = FBSDKLoginManager()
        loginManager.logOut()
    }
    
    
    
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        UserStore.shared.badgeNumber = UserStore.shared.badgeNumber + 1;
        NotificationCenter.default.post(name: .notificationCountChanged, object: nil, userInfo:nil)
        completionHandler([.alert, .badge, .sound])
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any]) {
        BaseViewController.handleNotificationWhenLive(userInfo: userInfo)
        NotificationCenter.default.post(name: .notificationCountChanged, object: nil, userInfo:userInfo)

        switch application.applicationState {
        case .active:
            //app is currently active, can update badges count here
            print("active")
        case .inactive:
            //app is transitioning from background to foreground (user taps notification), do what you need when user taps here
            print("inactive")
        case .background:
            //app is in background, if content-available key of your notification is set to 1, poll to your backend to retrieve data and update your interface here
            print("background")
        }
    }

    public func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any] = [:]) -> Bool {
        
        
        let facebookDidHandle = FBSDKApplicationDelegate.sharedInstance().application(
            app,
            open: url,
            sourceApplication: options[UIApplicationOpenURLOptionsKey.sourceApplication] as! String,
            annotation: options[UIApplicationOpenURLOptionsKey.annotation])
        
        return  facebookDidHandle

    }
    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool
    {
        let facebookDidHandle = FBSDKApplicationDelegate.sharedInstance().application(
            application,
            open: url,
            sourceApplication: sourceApplication,
            annotation: annotation)
        return facebookDidHandle
    }

    
}

