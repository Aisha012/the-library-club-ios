//
//  BaseViewController.swift
//  TheLibraryClub
//
//  Created by Phaninder on 17/01/18.
//  Copyright © 2018 Phaninder. All rights reserved.
//

import UIKit
import Toaster
import JSSAlertView

class BaseViewController: UIViewController, UIViewControllerTransitioningDelegate {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.enableExclusiveTouchForView(self.view)
        ToastView.appearance().font = UIFont.systemFont(ofSize: 16.0)
        if UIDevice().userInterfaceIdiom == .phone {
            switch UIScreen.main.nativeBounds.height {
            case 1136:
               // constraintTop.constant = 0.0
                break
            case 1334:
               // constraintTop.constant = 0.0
                break
            case 1920, 2208:
                //constraintTop.constant = 0.0
                break
            case 2436:
                //constraintTop.constant = 20.0
                for vw in self.view.subviews{
                    if vw.bounds.size.height == 74 && vw.bounds.origin.y == 0 && vw.bounds.origin.x == 0 && vw.backgroundColor == UIColor.black{
                        for cons in vw.superview!.constraints{
                            if isTopConstraint(constraint: cons, vw: vw){
                                cons.constant = 20
                                break
                            }
                        }
                        break
                    }
                }
                break
            default:
                print("unknown")
            }
            self.view.layoutIfNeeded()
        }
    }
    
    func isTopConstraint (constraint:NSLayoutConstraint, vw:AnyObject?)->Bool{
        return firstItemMatchesTopConstraint(constraint: constraint, vw: vw) || secondItemMatchesTopConstraint(constraint: constraint, vw: vw)
    }
    
    func firstItemMatchesTopConstraint (constraint:NSLayoutConstraint, vw:AnyObject?)->Bool{
        return (constraint.firstItem!.isEqual(vw)) && (constraint.secondAttribute == NSLayoutAttribute.top)

    }
    
    func secondItemMatchesTopConstraint (constraint:NSLayoutConstraint, vw:AnyObject?)-> Bool{
        return (constraint.secondItem!.isEqual(vw)) && (constraint.secondAttribute == NSLayoutAttribute.top)

    }
    /*
 - (BOOL)isTopConstraint:(NSLayoutConstraint *)constraint
 {
 return  [self firstItemMatchesTopConstraint:constraint] ||
 [self secondItemMatchesTopConstraint:constraint];
 }
 
 - (BOOL)firstItemMatchesTopConstraint:(NSLayoutConstraint *)constraint
 {
 return constraint.firstItem == self && constraint.firstAttribute == NSLayoutAttributeTop;
 }
 
 - (BOOL)secondItemMatchesTopConstraint:(NSLayoutConstraint *)constraint
 {
 return constraint.secondItem == self && constraint.secondAttribute == NSLayoutAttributeTop;
 }
 */
    
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    //Subclasses should override this method if they want to allow the user to navigate back
    //while the network indicator is visible.
    func shouldBlockNavigation() -> Bool {
        return true
    }
    
    deinit {

    }
    
    @IBAction func navigateBack(sender: AnyObject) {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    
    func navigateToAppStore(_ url: String) {
        if let url = URL(string: url),
            UIApplication.shared.canOpenURL(url)
        {
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }
    
    func navigateToSettingsPage() {
        guard let settingsUrl = URL(string: UIApplicationOpenSettingsURLString) else {
            return
        }
        
        if UIApplication.shared.canOpenURL(settingsUrl) {
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                    print("Settings opened: \(success)") // Prints true
                })
            } else {
                UIApplication.shared.openURL(settingsUrl)
            }
        }
    }
    
    @objc func navigateToServerErrorScreen() {
        DispatchQueue.main.async(execute: {
            Utilities.shared.navigateToServerErrorScreen()
        })
    }

    override var shouldAutorotate: Bool {
        return false
    }
    
    override var preferredInterfaceOrientationForPresentation: UIInterfaceOrientation {
        return .portrait
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
    
    override var prefersStatusBarHidden: Bool {
        return false
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    
    func enableExclusiveTouchForView(_ v: UIView) {
        
        for subView in v.subviews {
            subView.isExclusiveTouch = false
            if subView.subviews.count > 0 {
                self.enableExclusiveTouchForView(subView)
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func showNoInternetMessage() {
        Utilities.showToast(withString: "Please check your internet connection.".localized)
    }
    
    func showInvalidInputsMessage() {
        Utilities.showToast(withString: "Please enter valid inputs.".localized)
    }
    
    static func handleNotificationWhenLive(userInfo: [AnyHashable : Any]) {
        let storyBoard = UIStoryboard.init(name: "Main", bundle: nil)

        if UserStore.shared.isLoggedIn == true {
            if let aps = userInfo["aps"] as? NSDictionary,
                let alert = aps["alert"] as? NSDictionary,
                let page = alert["page"] as? String,
                page == "Promotion", let promoId = alert["id"] as? Int  {
                let viewController = storyBoard.instantiateViewController(withIdentifier: "PromotionDetailViewController") as! PromotionDetailViewController
                viewController.promotionID  = String(promoId)
                viewController.promotionName  = ""
                UIApplication.topViewController()?.navigationController?.pushViewController(viewController, animated: true)
            } else {
                let viewController = storyBoard.instantiateViewController(withIdentifier: ScreenID.NotificationViewController.rawValue) as! NotificationViewController
                UIApplication.topViewController()?.navigationController?.pushViewController(viewController, animated: true)
            }
        }
    }
    
    func getAppVersion() -> String {
        
        if let text = Bundle.main.infoDictionary?[kCFBundleVersionKey as String] as? String {
            return text
        }
        return ""
    }
    
    func topViewController() -> UIViewController {
        
        let appDelegate: AppDelegate = UIApplication.shared.delegate as! AppDelegate
        let navController: UINavigationController = appDelegate.window?.rootViewController
            as! UINavigationController
        if navController.isKind(of: UINavigationController.self) {
            return navController.visibleViewController!
        }
        return navController
    }
    
    func getFormattedDate(_ date: Date, dateFormat: String = "dd/MM/yyyy") -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = dateFormat
        return dateFormatter.string(from: date)
    }

}

