//
//  NotificationViewController.swift
//  TheLibraryClub
//
//  Created by Phaninder on 30/01/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import UIKit
import RxSwift

class NotificationViewController: BaseViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var emptyPlaceholderLabel: UILabel!
    
    private let refreshControl = UIRefreshControl()

    var notificationPagination = NotificationPagination.init();
    var isFirstTime = true
    var notificationObserver: Observable<NotificationPagination>!
    var disposableBag = DisposeBag()
    var isFetchingData = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        UserStore.shared.badgeNumber = 0
        UIApplication.shared.applicationIconBadgeNumber = 0
        NotificationCenter.default.post(name: .notificationCountChanged, object: nil, userInfo:nil)

        if #available(iOS 10.0, *) {
            tableView.refreshControl = refreshControl
        } else {
            tableView.addSubview(refreshControl)
        }
        refreshControl.addTarget(self, action: #selector(refreshData(_:)), for: .valueChanged)
        tableView.tableFooterView = UIView()
        fetchNotifications()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tabBarController?.tabBar.isHidden = true
    }
    
    @objc func refreshData(_ sender: Any) {
        self.notificationPagination.paginationType = .reload
        fetchNotifications()
        if let refreshControl = sender as? UIRefreshControl {
            refreshControl.endRefreshing()
        }
    }

    func fetchNotifications() {
        guard Utilities.shared.isNetworkReachable() else {
            self.showNoInternetMessage()
            return
        }
        
        if notificationPagination.paginationType != .old {
            self.notificationPagination.currentPageNumber = 1
            if isFirstTime == true {
                Utilities.showHUD(forView: self.view, excludeViews: [])
            } else {
                UIApplication.shared.isNetworkActivityIndicatorVisible = true
            }
        } else {
            UIApplication.shared.isNetworkActivityIndicatorVisible = true
        }
        
        isFetchingData = true
        notificationObserver = ApiManager.shared.apiService.fetchNotifications(pageNumber: String(notificationPagination.currentPageNumber))
        let notificationDisposable = notificationObserver.subscribe(onNext: {(paginationObject) in
            self.notificationPagination.appendDataFromObject(paginationObject)
            self.isFetchingData = false
            DispatchQueue.main.async(execute: {
                Utilities.hideHUD(forView: self.view)
                if self.notificationPagination.paginationType != .old && self.isFirstTime == true {
                    self.isFirstTime = false
                } else {
                    UIApplication.shared.isNetworkActivityIndicatorVisible = false
                }
                if self.notificationPagination.notifications.count == 0 {
                    self.emptyPlaceholderLabel.isHidden = false
                    self.tableView.isHidden = true
                } else {
                    self.emptyPlaceholderLabel.isHidden = true
                    self.tableView.isHidden = false
                }
                self.tableView.reloadData()
            })
        }, onError: {(error) in
            self.isFetchingData = false
            DispatchQueue.main.async(execute: {
                Utilities.hideHUD(forView: self.view)
            })
        })
        notificationDisposable.disposed(by: disposableBag)
    }

}

extension NotificationViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let notifCount = notificationPagination.notifications.count
        return notificationPagination.hasMoreToLoad ? notifCount + 1 : notifCount
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard indexPath.row < notificationPagination.notifications.count else {
            let cell = tableView.dequeueReusableCell(withIdentifier: LoadMoreTableViewCell.cellIdentifier(), for: indexPath) as! LoadMoreTableViewCell
            cell.activityIndicator.startAnimating()
            return cell
        }

        let cell = tableView.dequeueReusableCell(withIdentifier: NotificationTableViewCell.cellIdentifier(), for: indexPath) as! NotificationTableViewCell
        let notificationObj = notificationPagination.notifications[indexPath.row]
        
        cell.timeLabel.text = notificationObj.elapsedTime
        
        let title = notificationObj.message ?? "Sample message"
        cell.messageLabel.text = title
        
        var notificationTitle = "Library Club"
         if let notification = notificationObj.title, !notification.isEmpty {
            notificationTitle = notification
        }
        cell.titleLabel.text = notificationTitle
        
        cell.accessoryType = .none
        if notificationObj.type == "Promotion" {
            cell.accessoryType = .disclosureIndicator
        }

        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let notificationObj = notificationPagination.notifications[indexPath.row]

        if notificationObj.type == "Promotion",
            let promoId = notificationObj.redirectId {
            let storyBoard = UIStoryboard.init(name: "Main", bundle: nil)
            let viewController = storyBoard.instantiateViewController(withIdentifier: "PromotionDetailViewController") as! PromotionDetailViewController
            viewController.promotionID  = String(promoId)
            viewController.promotionName  = notificationObj.title
            UIApplication.topViewController()?.navigationController?.pushViewController(viewController, animated: true)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        guard indexPath.row <  notificationPagination.notifications.count else {
            return 50.0
        }

        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row == self.notificationPagination.notifications.count && self.notificationPagination.hasMoreToLoad && !isFetchingData {
            fetchNotifications()
        }
    }
    
}

