//
//  PromotionCell.swift
//  TheLibraryClub
//
//  Created by Namespace  on 03/08/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import UIKit

class PromotionCell: UITableViewCell, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout{
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var btnViewMore:UIButton!
    @IBOutlet weak var titleLabel:UILabel!
    weak var badgeDelegate:PromotionDualTableViewCellDelegate?
    
    var promotions = [Promotion]()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    var CellWidth  = 166
    var CellSpacing = 10
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {

        let totalCellWidth = CellWidth * promotions.count
        let totalSpacingWidth = CellSpacing * (promotions.count - 1)

        let leftInset = (collectionView.frame.size.width - CGFloat(totalCellWidth + totalSpacingWidth)) / 2 + 5
        let rightInset = leftInset
        print(leftInset)
        if self.promotions.count <= 2 {
            
            return UIEdgeInsetsMake(0, leftInset, 0, rightInset)
        }
        return UIEdgeInsetsMake(0, 0, 0, 0)
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return promotions.count > 10 ? 10 : promotions.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "badgeCell", for: indexPath) as! BadgeViewCell
        let promotion1 = promotions[indexPath.row]
        cell.ivBadge.sd_setShowActivityIndicatorView(true)
        cell.ivBadge.sd_setIndicatorStyle(.gray)
        cell.ivBadge.sd_setImage(with: URL(string: promotion1.imageURL), completed: nil)
        cell.lblBadgeTitle.text = promotion1.name!
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.badgeDelegate?.promotiontOneButtonTapped(index:indexPath.row)
    }
    
    @IBAction func btnViewMoreTapped(_ sender: UIButton) {
        badgeDelegate?.viewMorePromosButtonTapped()
    }
    
    func startTimerWithTaskFlashIndicators(){
        self.collectionView.flashScrollIndicators()
        if #available(iOS 10.0, *) {
            Timer.scheduledTimer(withTimeInterval: 5, repeats: true) {[weak self] (timer) in
                UIView.animate(withDuration: 1, animations: {
                    self?.collectionView.flashScrollIndicators()
                })
            }
        } else {
            // Fallback on earlier versions
            Timer.scheduledTimer(timeInterval: 5, target: self, selector: #selector(setTimer), userInfo: nil, repeats: true)
        }
    }
    
    @objc func setTimer(){
        UIView.animate(withDuration: 1, animations: {
            self.collectionView.flashScrollIndicators()
        })
    }
    
    
}

