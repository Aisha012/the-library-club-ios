//
//  DashboardViewMoreViewController.swift
//  TheLibraryClub
//
//  Created by Phaninder on 04/04/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import UIKit
import RxSwift

class DashboardViewMoreViewController: BaseViewController {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    
    var promotionPagination = PromotionPagination.init();
    var outletPagination = OutletPagination.init();

    var isPromotions = true
    var screenTitle = "Promotions"
    var isFetchingData = false
    var isFirstTime = true
    var promotionObserver: Observable<DashBoardPromotion>!
    var outletObserver: Observable<DashBoardOutlet>!
    var disposableBag = DisposeBag()

    private let refreshControl = UIRefreshControl()

    override func viewDidLoad() {
        super.viewDidLoad()
        titleLabel.text = screenTitle
        if #available(iOS 10.0, *) {
            collectionView.refreshControl = refreshControl
        } else {
            collectionView.addSubview(refreshControl)
        }
        refreshControl.addTarget(self, action: #selector(refreshData(_:)), for: .valueChanged)
        if isPromotions == true {
            fetchDashboardPromotions()
        } else {
            fetchDashboardOutlets()
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
       tabBarController?.tabBar.isHidden = true
    }
    
    @objc func refreshData(_ sender: Any) {
        if isPromotions == true {
            promotionPagination.paginationType = .reload
            promotionPagination.currentPageNumber = 1
            fetchDashboardPromotions()
        } else {
            outletPagination.paginationType = .reload
            outletPagination.currentPageNumber = 1
            fetchDashboardOutlets()
        }

        promotionPagination.paginationType = .reload
        if let refreshControl = sender as? UIRefreshControl {
            refreshControl.endRefreshing()
        }
    }
    
    func fetchDashboardPromotions() {
        guard Utilities.shared.isNetworkReachable() else {
            self.showNoInternetMessage()
            return
        }
        
        if promotionPagination.paginationType != .old {
            self.promotionPagination.currentPageNumber = 1
            if isFirstTime == true {
                Utilities.showHUD(forView: self.view, excludeViews: [])
            } else {
                UIApplication.shared.isNetworkActivityIndicatorVisible = true
            }
        } else {
            UIApplication.shared.isNetworkActivityIndicatorVisible = true
        }
        
        isFetchingData = true
        promotionObserver = ApiManager.shared.apiService.fetchDashBoardPromotions(pageNumber: String(promotionPagination.currentPageNumber))
        let promotionDisposable = promotionObserver.subscribe(onNext: {(paginationObject) in
            self.promotionPagination.appendDataFromObject(paginationObject.promotions)
            self.isFetchingData = false
            DispatchQueue.main.async(execute: {
                Utilities.hideHUD(forView: self.view)
                
                if self.promotionPagination.paginationType != .old && self.isFirstTime == true {
                    self.isFirstTime = false
                } else {
                    UIApplication.shared.isNetworkActivityIndicatorVisible = false
                }
                self.collectionView.reloadData()
            })
        }, onError: {(error) in
            self.isFetchingData = false
            DispatchQueue.main.async(execute: {
                Utilities.hideHUD(forView: self.view)
            })
        })
        promotionDisposable.disposed(by: disposableBag)
    }

    func fetchDashboardOutlets() {
        guard Utilities.shared.isNetworkReachable() else {
            self.showNoInternetMessage()
            return
        }
        
        if outletPagination.paginationType != .old {
            self.outletPagination.currentPageNumber = 1
            if isFirstTime == true {
                Utilities.showHUD(forView: self.view, excludeViews: [])
            } else {
                UIApplication.shared.isNetworkActivityIndicatorVisible = true
            }
        } else {
            UIApplication.shared.isNetworkActivityIndicatorVisible = true
        }
        
        isFetchingData = true
        outletObserver = ApiManager.shared.apiService.fetchDashBoardOutlets(pageNumber: String(outletPagination.currentPageNumber))
        let outletDisposable = outletObserver.subscribe(onNext: {(paginationObject) in
            self.outletPagination.appendDataFromObject(paginationObject.outlets)
            self.isFetchingData = false
            DispatchQueue.main.async(execute: {
                Utilities.hideHUD(forView: self.view)
                
                if self.outletPagination.paginationType != .old && self.isFirstTime == true {
                    self.isFirstTime = false
                } else {
                    UIApplication.shared.isNetworkActivityIndicatorVisible = false
                }
                self.collectionView.reloadData()
            })
        }, onError: {(error) in
            self.isFetchingData = false
            DispatchQueue.main.async(execute: {
                Utilities.hideHUD(forView: self.view)
            })
        })
        outletDisposable.disposed(by: disposableBag)
    }

}

extension DashboardViewMoreViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if isPromotions {
            return promotionPagination.promotions.count
        } else {
            return outletPagination.outlets.count
        }
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if isPromotions {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ViewMorePromotionCollectionViewCell.cellIdentifier(), for: indexPath) as! ViewMorePromotionCollectionViewCell
            cell.configureCell(promotion: promotionPagination.promotions[indexPath.item])
            return cell
        } else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ViewMoreOutletCollectionViewCell.cellIdentifier(), for: indexPath) as! ViewMoreOutletCollectionViewCell
            cell.configureCellWithOutlet(outletPagination.outlets[indexPath.item] )
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if isPromotions == true {
            if indexPath.item == promotionPagination.promotions.count - 1 && promotionPagination.hasMoreToLoad && !isFetchingData {
                fetchDashboardPromotions()
            }
        } else {
            if indexPath.item == outletPagination.outlets.count - 1 && outletPagination.hasMoreToLoad && !isFetchingData {
                fetchDashboardOutlets()
            }

        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize.init(width: ScreenWidth / 2, height: isPromotions ? 216 : 130)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if isPromotions == true {
            let promotion = promotionPagination.promotions[indexPath.row]
            let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let promotionDetailViewController = mainStoryboard.instantiateViewController(withIdentifier: ScreenID.PromotionDetailViewController.rawValue) as! PromotionDetailViewController
            promotionDetailViewController.promotionID = String(promotion.id)
            promotionDetailViewController.promotionName = promotion.name
            self.tabBarController?.tabBar.isHidden = true
            self.navigationController?.pushViewController(promotionDetailViewController, animated: true)
        } else {
            let outlet = outletPagination.outlets[indexPath.row]
            let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let outletDetailViewController = mainStoryboard.instantiateViewController(withIdentifier: ScreenID.OutletDetailsViewController.rawValue) as! OutletDetailsViewController
            outletDetailViewController.outletID = String(outlet.id)
            outletDetailViewController.outletName = outlet.name
            self.tabBarController?.tabBar.isHidden = true
            self.navigationController?.pushViewController(outletDetailViewController, animated: true)

        }
    }
    

}
