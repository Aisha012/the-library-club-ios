//
//  OrderHistoryViewController.swift
//  TheLibraryClub
//
//  Created by Phaninder on 05/02/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import UIKit
import RxSwift

class OrderHistoryViewController: BaseViewController {

    @IBOutlet weak var tableView: UITableView!
    
    var disposableBag = DisposeBag()
    var isFetchingData = false
    var orderPagination = OrderPagination.init()
    var isFirstTime = true

    private let refreshControl = UIRefreshControl()

    override func viewDidLoad() {
        super.viewDidLoad()
        if #available(iOS 10.0, *) {
            tableView.refreshControl = refreshControl
        } else {
            tableView.addSubview(refreshControl)
        }
        refreshControl.addTarget(self, action: #selector(refreshData(_:)), for: .valueChanged)
        self.tabBarController?.tabBar.isHidden = true
        fetchOrders()
    }

    @objc func refreshData(_ sender: Any) {
        self.orderPagination.paginationType = .new
        fetchOrders()
        if let refreshControl = sender as? UIRefreshControl {
            refreshControl.endRefreshing()
        }
    }

    func fetchOrders() {
        guard Utilities.shared.isNetworkReachable() else {
            self.showNoInternetMessage()
            return
        }
        
        if orderPagination.paginationType != .old {
            self.orderPagination.currentPageNumber = 0
            if isFirstTime == true {
                Utilities.showHUD(forView: self.view, excludeViews: [])
            } else {
                UIApplication.shared.isNetworkActivityIndicatorVisible = true
            }

        } else {
            UIApplication.shared.isNetworkActivityIndicatorVisible = true
        }

        let orderObserver = ApiManager.shared.apiService.fetchOrders(pageNumber: String(orderPagination.currentPageNumber))
        let orderDisposable = orderObserver.subscribe(onNext: {(paginationObject) in
            self.isFetchingData = false

            self.orderPagination.appendDataFromObject(paginationObject)
            DispatchQueue.main.async(execute: {
                Utilities.hideHUD(forView: self.view)
                if self.orderPagination.paginationType != .old && self.isFirstTime == true {
                    self.isFirstTime = false
                } else {
                    UIApplication.shared.isNetworkActivityIndicatorVisible = false
                }
                self.tableView.reloadData()
            })
        }, onError: {(error) in
            self.isFetchingData = false
            DispatchQueue.main.async(execute: {
                Utilities.hideHUD(forView: self.view)
            })
        })
        orderDisposable.disposed(by: disposableBag)
    }

}

extension OrderHistoryViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if orderPagination.orders.count == 0 {
            return 0
        } else {
           return orderPagination.orders.count + 2
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard indexPath.row < orderPagination.orders.count + 1 else {
            let cell = tableView.dequeueReusableCell(withIdentifier: LoadMoreTableViewCell.cellIdentifier(), for: indexPath) as! LoadMoreTableViewCell
            cell.activityIndicator.startAnimating()
            return cell
        }

        if indexPath.row == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: LastOrdersTitleTableViewCell.cellIdentifier(), for: indexPath) as! LastOrdersTitleTableViewCell
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: LastOrderContentTableViewCell.cellIdentifier(), for: indexPath) as! LastOrderContentTableViewCell
            cell.configureCell(withOrder: orderPagination.orders[indexPath.row - 1], index: indexPath.row - 1)
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        guard indexPath.row < orderPagination.orders.count + 1 else {
            return orderPagination.hasMoreToLoad ? 50.0 : 0
        }
        return indexPath.row == 0 ? 56 : 50
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row == orderPagination.orders.count + 1 && self.orderPagination.hasMoreToLoad && !isFetchingData {
            fetchOrders()
        }
    }

    
}
