//
//  DashboardViewController.swift
//  TheLibraryClub
//
//  Created by Phaninder on 18/01/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import UIKit
import RxSwift
import Crashlytics

class DashboardViewController: BaseViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var vwHeader: UIView!
    
    @IBOutlet weak var constraintTop: NSLayoutConstraint!
    @IBOutlet weak var notificationCountLabel: UILabel!
    @IBOutlet weak var notificationView: UIView!
    
    var hasGradientSet:Bool = false
    var lastOrders = [Promotion]()
    var savedPromotionsCellHeight: CGFloat = 0.0
    var savedPromotionsBadgeHeight: CGFloat = 0.0

    var lastOrdersCellHeight: CGFloat = 0.0
    var savedOutletCellHeight: CGFloat = 0.0
    var disposableBag = DisposeBag()
    var promotionPagination = PromotionPagination.init();
    var outletPagination = OutletPagination.init();
    var customerPagination = CustomerInfoPagination.init()
    var orderPagination = OrderPagination.init()
    var gradientView:UIView?
    var rowCount = 0
    private let refreshControl = UIRefreshControl()
    var userInfo: [AnyHashable : Any]?
    var skipEdgeInsets = true
    var promotionHeading = "Saved Promotions"
    var outletHeading = "Saved Outlets"
    
    override func viewDidLoad() {
        super.viewDidLoad()

        configureNotifications()        
        
        notificationChanged()
        if #available(iOS 10.0, *) {
            tableView.refreshControl = refreshControl
        } else {
            tableView.addSubview(refreshControl)
        }
        if let notificationInformation = userInfo {
            BaseViewController.handleNotificationWhenLive(userInfo: notificationInformation)
        }

        refreshControl.addTarget(self, action: #selector(refreshData(_:)), for: .valueChanged)

        tableView.register(UINib.init(nibName: UserStatusTableViewCell.cellIdentifier(), bundle: nil), forCellReuseIdentifier: UserStatusTableViewCell.cellIdentifier())
        tableView.register(UINib.init(nibName: DashboardPromoTableViewCell.cellIdentifier(), bundle: nil), forCellReuseIdentifier: DashboardPromoTableViewCell.cellIdentifier())
       

    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        fetchDashboardDetails()
     //  //self.navigationController?.tabBarController?.tabBar.isHidden = false
        if skipEdgeInsets == false {
            tableView.contentInset = UIEdgeInsetsMake(0, 0, 49, 0)
        }
        skipEdgeInsets = false
        refreshControl.setValue(150, forKey: "_snappingHeight")
        tableView.reloadData()
    //    Crashlytics.sharedInstance().crash()

    }
    
    override func viewWillAppear(_ animated: Bool) {
     self.gradientView?.isHidden = false
   //  self.navigationController?.tabBarController?.tabBar.isHidden = false

    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.gradientView?.isHidden = true
    

    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: .promoUnfavorited, object: nil)
        NotificationCenter.default.removeObserver(self, name: .outletUnfavorited, object: nil)
        NotificationCenter.default.removeObserver(self, name: .promoFavorited, object: nil)
        NotificationCenter.default.removeObserver(self, name: .outletFavorited, object: nil)
        NotificationCenter.default.removeObserver(self, name: .notificationCountChanged, object: nil)
        
    }
    
    func configureNotifications() {
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(deletePromo(_:)), name: .promoUnfavorited,
                                               object: nil)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(notificationChanged), name: .notificationCountChanged,
                                               object: nil)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(deleteOutlet(_:)), name: .outletUnfavorited,
                                               object: nil)
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(refreshPromotions), name: .promoFavorited,
                                               object: nil)
        
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(refreshOutlets), name: .outletFavorited,
                                               object: nil)
        

    }

    
    @objc func refreshData(_ sender: Any) {
        self.outletPagination.paginationType = .new
        self.promotionPagination.paginationType = .new
        self.customerPagination.paginationType = .new
        fetchDashboardDetails()
        if let refreshControl = sender as? UIRefreshControl {
            refreshControl.endRefreshing()
        }
    }
    
    @objc func deletePromo(_ notification: NSNotification) {
        if let promoID = notification.userInfo?["promoID"] as? Int {
            let index = self.promotionPagination.promotions.index(where: { (promo) -> Bool in
                return promo.id == promoID
            })
            if let deleteIndex = index {
                self.promotionPagination.promotions.remove(at: deleteIndex)
                DispatchQueue.main.async(execute: {
                    self.tableView.reloadData()


                })
            }
        }
    }
    
    @objc func notificationChanged() {
        DispatchQueue.main.async {
            if UserStore.shared.badgeNumber == 0 {
                self.notificationCountLabel.isHidden = true
                self.notificationCountLabel.text = "0"
                self.notificationView.isHidden = true
            } else {
                self.notificationCountLabel.isHidden = false
                self.notificationCountLabel.text = "\(UserStore.shared.badgeNumber)"
                self.notificationView.isHidden = false
            }

        }
    }

    @objc func deleteOutlet(_ notification: NSNotification) {
        if let outletID = notification.userInfo?["outletID"] as? Int {
            let index = self.outletPagination.outlets.index(where: { (outlet) -> Bool in
                return outlet.id == outletID
            })
            if let deleteIndex = index {
                self.outletPagination.outlets.remove(at: deleteIndex)
                DispatchQueue.main.async(execute: {
                    self.tableView.reloadData()


                })
            }
        }
    }
    
    @IBAction func badgeButtonTapped(_ sender: UIButton) {
        let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let badgeViewController = mainStoryboard.instantiateViewController(withIdentifier: "BadgeViewController") as! BadgeViewController
      //  self.tabBarController?.tabBar.isHidden = true
        self.navigationController?.pushViewController(badgeViewController, animated: true)
    }
    
    @objc func refreshPromotions() {
        self.fetchDashboardPromotions(false)
    }
    
    @objc func refreshOutlets() {
        self.fetchDashboardOutlets(false)
    }
    
    func fetchDashboardDetails() {
        guard Utilities.shared.isNetworkReachable() else {
            self.showNoInternetMessage()
            return
        }
        customerPagination.paginationType = .new
        Utilities.showHUD(forView: self.view, excludeViews: [])
        
        let dashboardObserver = ApiManager.shared.apiService.fetchDashboardDetails()
        let dashboardDisposable = dashboardObserver.subscribe(onNext: {(paginationObject) in
            self.customerPagination.appendDataFromObject(paginationObject)
            self.rowCount = 1

            DispatchQueue.main.async(execute: {
                self.tableView.reloadData()


                self.fetchBadges()
            })
        }, onError: {(error) in
            DispatchQueue.main.async(execute: {
                Utilities.hideHUD(forView: self.view)
            })
        })
        dashboardDisposable.disposed(by: disposableBag)
    }
    
    
    //MARK:- FETCH BADGES:-
    var badges = [Badge]()

    func fetchBadges() {
        guard Utilities.shared.isNetworkReachable() else {
            self.showNoInternetMessage()
            return
        }
        
        Utilities.showHUD(forView: self.view, excludeViews: [])
        let badgeObserver = ApiManager.shared.apiService.fetchBadges(UserStore.shared.userId)
        let badgeDisposable = badgeObserver.subscribe(onNext: {(badgeArray) in
            self.badges = badgeArray
            DispatchQueue.main.async(execute: {
                Utilities.hideHUD(forView: self.view)
                self.rowCount = 2
                self.tableView.reloadData()
                self.fetchDashboardPromotions(true)
               // self.badgeEmptyPlaceholder.isHidden = !self.badges.isEmpty
            })
        }, onError: {(error) in
            DispatchQueue.main.async(execute: {
                Utilities.hideHUD(forView: self.view)
            })
        })
        badgeDisposable.disposed(by: disposableBag)
    }
    

    func fetchDashboardPromotions(_ inSequence: Bool) {
        guard Utilities.shared.isNetworkReachable() else {
            self.showNoInternetMessage()
            return
        }
        promotionPagination.paginationType = .new
        if inSequence == false {
            UIApplication.shared.isNetworkActivityIndicatorVisible = true
        }
        
        let promotionObserver = ApiManager.shared.apiService.fetchDashBoardPromotions(pageNumber: "1")
        let promotionDisposable = promotionObserver.subscribe(onNext: {(dashboardPromotions) in
            self.promotionHeading = dashboardPromotions.heading
            self.promotionPagination.appendDataFromObject(dashboardPromotions.promotions)
            DispatchQueue.main.async(execute: {
                self.tableView.reloadData()


                if (inSequence == true) {
                    self.rowCount = 3
                    //self.fetchDashboardOutlets(true)
                    self.fetchOrders()
                    self.tableView.reloadData()


                } else {
                    self.tableView.reloadRows(at: [IndexPath.init(row: 1, section: 0)], with: .none)
                    self.tableView.scrollToRow(at: IndexPath.init(row: 1, section: 0), at: .middle, animated: false)
                    UIApplication.shared.isNetworkActivityIndicatorVisible = false
                }
            })
        }, onError: {(error) in
            DispatchQueue.main.async(execute: {
                Utilities.hideHUD(forView: self.view)
            })
        })
        promotionDisposable.disposed(by: disposableBag)
    }
    
    func fetchDashboardOutlets(_ inSequence: Bool) {
        guard Utilities.shared.isNetworkReachable() else {
            self.showNoInternetMessage()
            return
        }
        
        if inSequence == false {
            UIApplication.shared.isNetworkActivityIndicatorVisible = true
        }
        outletPagination.paginationType = .new
        
        let outletObserver = ApiManager.shared.apiService.fetchDashBoardOutlets(pageNumber: "1")
        let outletDisposable = outletObserver.subscribe(onNext: {(dashboardOutlets) in
            self.outletHeading = dashboardOutlets.heading
            self.outletPagination.appendDataFromObject(dashboardOutlets.outlets)
            DispatchQueue.main.async(execute: {
                self.tableView.reloadData()


                
                if inSequence == true {
                    self.rowCount = 4
                    self.fetchOrders()
//                    if self.customerPagination.orders.count > 5 {
//                        self.rowCount = 9
//                    } else {
//                        self.rowCount = 4 + self.customerPagination.orders.count
//                    }
                    self.tableView.reloadData()


                } else {
                    self.tableView.reloadRows(at: [IndexPath.init(row: 2, section: 0)], with: .none)
                    self.tableView.scrollToRow(at: IndexPath.init(row: 2, section: 0), at: .middle, animated: false)
                    UIApplication.shared.isNetworkActivityIndicatorVisible = false
                }
            })
        }, onError: {(error) in
            DispatchQueue.main.async(execute: {
                Utilities.hideHUD(forView: self.view)
            })
        })
        outletDisposable.disposed(by: disposableBag)
    }

    func fetchOrders() {
        guard Utilities.shared.isNetworkReachable() else {
            self.showNoInternetMessage()
            return
        }
        
        let orderObserver = ApiManager.shared.apiService.fetchOrders(pageNumber: "0")
        let orderDisposable = orderObserver.subscribe(onNext: {(paginationObject) in

            self.orderPagination = paginationObject//.appendDataFromObject(paginationObject)
            DispatchQueue.main.async(execute: {
                self.tableView.reloadData()


                if self.orderPagination.orders.count >= 5 {
                    self.rowCount = 9
                } else {
                    self.rowCount = 3 + self.orderPagination.orders.count //4 +
                }
                self.tableView.reloadData()


                Utilities.hideHUD(forView: self.view)
            })
        }, onError: {(error) in
            DispatchQueue.main.async(execute: {
                Utilities.hideHUD(forView: self.view)
            })
        })
        orderDisposable.disposed(by: disposableBag)
    }

    func showOutletDetailsForOutlet(_ outlet: Outlet) {
        let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let outletDetailViewController = mainStoryboard.instantiateViewController(withIdentifier: ScreenID.OutletDetailsViewController.rawValue) as! OutletDetailsViewController
        outletDetailViewController.outletID = String(outlet.id)
        outletDetailViewController.outletName = outlet.name
        self.navigationController?.tabBarController?.tabBar.isHidden = true
        outletDetailViewController.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(outletDetailViewController, animated: true)
    }

    func navigateToPromotionDescription(_ promotion: Promotion) {
        let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let promotionDetailViewController = mainStoryboard.instantiateViewController(withIdentifier: ScreenID.PromotionDetailViewController.rawValue) as! PromotionDetailViewController
        promotionDetailViewController.promotionID = String(promotion.id)
        promotionDetailViewController.promotionName = promotion.name
        self.navigationController?.tabBarController?.tabBar.isHidden = true
        promotionDetailViewController.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(promotionDetailViewController, animated: true)
    }
    
    @IBAction func QRBtnTapped(_ sender : UIButton){
        let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let QRVcPage = mainStoryboard.instantiateViewController(withIdentifier: "QRViewController") as! QRViewController
        self.navigationController?.pushViewController(QRVcPage, animated: true)
    }
    
}

extension DashboardViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return rowCount
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.row {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: UserStatusTableViewCell.cellIdentifier(), for: indexPath) as! UserStatusTableViewCell
            cell.controller = self
            cell.configureCell(customerPagination)
            return cell
            
        case 1:
            if badges.count >= 2 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "BadgeCell", for: indexPath) as! BadgeCell
                cell.badges = self.badges
                cell.collectionView.reloadData()
                cell.startTimerWithTaskFlashIndicators()
                cell.titleLabel.text = "Badges"
                cell.badgeDelegate = self as BadgeDualTableViewCellDelegate
                savedPromotionsBadgeHeight = 328//cell.configureCellsWithBadges(badges)
                return cell
           } else if badges.count == 1 {
                let cell = tableView.dequeueReusableCell(withIdentifier: PromotionSingleTableViewCell.cellIdentifier(), for: indexPath) as! PromotionSingleTableViewCell
                cell.titleLabel.text = "Badges"
                cell.badgedelegate = self as badgeSingleTableViewCellDelegate
                savedPromotionsBadgeHeight = cell.configureCellsWithBadge(badges)
                return cell
            }else {
                let cell = tableView.dequeueReusableCell(withIdentifier: PromotionEmptyTableViewCell.cellIdentifier(), for: indexPath) as! PromotionEmptyTableViewCell
                cell.titleLabel.text = "Badges"
                savedPromotionsBadgeHeight = 0//100
                cell.messageLabel.text = "No " + "Badges"
                return cell
            }
        case 2:
            if promotionPagination.promotions.count >= 2 {
                //let cell = tableView.dequeueReusableCell(withIdentifier: PromotionDualTableViewCell.cellIdentifier(), for: indexPath) as! PromotionDualTableViewCell
                //cell.titleLabel.text = promotionHeading
                //cell.delegate = self
                //savedPromotionsCellHeight = cell.configureCellsWithPromos(promotionPagination.promotions)
                let cell = tableView.dequeueReusableCell(withIdentifier: "PromotionCell", for: indexPath) as! PromotionCell
                cell.promotions = promotionPagination.promotions
                cell.collectionView.reloadData()
                cell.startTimerWithTaskFlashIndicators()
                cell.titleLabel.text = promotionHeading
                cell.badgeDelegate = self as PromotionDualTableViewCellDelegate
                savedPromotionsCellHeight = 328
                return cell
            } else if promotionPagination.promotions.count == 1 {
                let cell = tableView.dequeueReusableCell(withIdentifier: PromotionSingleTableViewCell.cellIdentifier(), for: indexPath) as! PromotionSingleTableViewCell
                cell.titleLabel.text = promotionHeading
                cell.delegate = self
                savedPromotionsCellHeight = cell.configureCellsWithPromos(promotionPagination.promotions)
                return cell
            } else {
                let cell = tableView.dequeueReusableCell(withIdentifier: PromotionEmptyTableViewCell.cellIdentifier(), for: indexPath) as! PromotionEmptyTableViewCell
                cell.titleLabel.text = promotionHeading
                savedPromotionsCellHeight = 100
                cell.messageLabel.text = "No " + promotionHeading
                return cell
            }
//        case 4:
//            if outletPagination.outlets.count >= 2 {
//                let cell = tableView.dequeueReusableCell(withIdentifier: OutletDualTableViewCell.cellIdentifier(), for: indexPath) as! OutletDualTableViewCell
//                cell.titleLabel.text = outletHeading
//                cell.delegate = self
//                savedOutletCellHeight = cell.configureCellsWithOutlets(outletPagination.outlets)
//                return cell
//            } else if outletPagination.outlets.count == 1 {
//                let cell = tableView.dequeueReusableCell(withIdentifier: OutletSingleTableViewCell.cellIdentifier(), for: indexPath) as! OutletSingleTableViewCell
//                cell.delegate = self
//                cell.titleLabel.text = outletHeading
//                savedOutletCellHeight = cell.configureCellsWithOutlets(outletPagination.outlets)
//                return cell
//            } else {
//                let cell = tableView.dequeueReusableCell(withIdentifier: OutletEmptyTableViewCell.cellIdentifier(), for: indexPath) as! OutletEmptyTableViewCell
//                cell.titleLabel.text = outletHeading
//                savedOutletCellHeight = 100
//                cell.messageLabel.text = "No " + outletHeading
//                return cell
//            }
        case 3:
            if customerPagination.orders.count == 0 {
                let cell = tableView.dequeueReusableCell(withIdentifier: LastOrdersEmptyTableViewCell.cellIdentifier(), for: indexPath) as! LastOrdersEmptyTableViewCell
                return cell
            } else {
                let cell = tableView.dequeueReusableCell(withIdentifier: LastOrdersTitleTableViewCell.cellIdentifier(), for: indexPath) as! LastOrdersTitleTableViewCell
                return cell
            }
        default:
            let cell = tableView.dequeueReusableCell(withIdentifier: LastOrderContentTableViewCell.cellIdentifier(), for: indexPath) as! LastOrderContentTableViewCell
            cell.delegate = self
            cell.configureCell(withOrder: orderPagination.orders[indexPath.row - 4], index: indexPath.row - 4)
            return cell
        }
    }
    
    
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.row {
        case 0:
            return 210.0
        case 1:
            return savedPromotionsBadgeHeight
        case 2:
            return savedPromotionsCellHeight
        case 3:
            return 100
//        case 4:
//            return savedOutletCellHeight//100
        default:
            if indexPath.row == 8 && customerPagination.orders.count > 5 {
                return 100.0
            } else {
                return 49.0
            }
        }
    }
    
}

extension DashboardViewController: PromotionDualTableViewCellDelegate {
    
    func viewMorePromosButtonTapped() {
    self.navigationController?.tabBarController?.selectedIndex = 3
//        let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
//        //PromotionListingViewController
//       // let promotionListingViewController = mainStoryboard.instantiateViewController(withIdentifier: ScreenID.DashboardViewMoreViewController.rawValue) as! DashboardViewMoreViewController
//        let promotionListingViewController = mainStoryboard.instantiateViewController(withIdentifier: "PromotionListingViewController") as! PromotionListingViewController
//        //promotionListingViewController.screenTitle = promotionHeading
//        self.tabBarController?.tabBar.isHidden = true
//        promotionListingViewController.isFromDashboard = true
//        self.navigationController?.pushViewController(promotionListingViewController, animated: true)
    }
    
    func promotionTwoButtonTapped() {
        navigateToPromotionDescription(promotionPagination.promotions[1])
    }
    
    func promotiontOneButtonTapped(index:Int) {
        navigateToPromotionDescription(promotionPagination.promotions[index])
    }
}

extension DashboardViewController: PromotionSingleTableViewCellDelegate {
    
    func promotionButtonTapped() {
        navigateToPromotionDescription(promotionPagination.promotions[0])
    }
    
}

extension DashboardViewController: BadgeDualTableViewCellDelegate {
    func viewMoreBadgeButtonTapped() {
                let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
               let promotionListingViewController = mainStoryboard.instantiateViewController(withIdentifier: "BadgeViewController") as! BadgeViewController
                promotionListingViewController.badges = self.badges
                self.tabBarController?.tabBar.isHidden = true
                promotionListingViewController.hidesBottomBarWhenPushed = true

                self.navigationController?.pushViewController(promotionListingViewController, animated: true)
    }
    
    func badgeOneButtonTapped() {
        
    }
    
    func badgeTwoButtonTapped() {
        
    }
    
    
//    func viewMorePromosButtonTapped() {

//    }
//
//    func promotionTwoButtonTapped() {
//        navigateToPromotionDescription(promotionPagination.promotions[1])
//    }
//
//    func promotiontOneButtonTapped() {
//        navigateToPromotionDescription(promotionPagination.promotions[0])
//    }
}

extension DashboardViewController: badgeSingleTableViewCellDelegate {
    
    func badgeButtonTapped() {
        viewMoreBadgeButtonTapped()
    }
//    func promotionButtonTapped() {
//        navigateToPromotionDescription(promotionPagination.promotions[0])
//    }
    
}

extension DashboardViewController: OutletDualTableViewCellDelegate {
    
    func viewMoreOutletsButtonTapped() {
        let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
       // PromotionListingViewController
//        let outletListingViewController = mainStoryboard.instantiateViewController(withIdentifier: ScreenID.DashboardViewMoreViewController.rawValue) as! DashboardViewMoreViewController
        
        let outletListingViewController = mainStoryboard.instantiateViewController(withIdentifier:"PromotionListingViewController") as! PromotionListingViewController
     //   outletListingViewController.screenTitle = promotionHeading
         outletListingViewController.isFromDashboard = true
        self.tabBarController?.tabBar.isHidden = true
        outletListingViewController.hidesBottomBarWhenPushed = true

        self.navigationController?.pushViewController(outletListingViewController, animated: true)
    }
    
    func outletOneButtonTapped() {
        showOutletDetailsForOutlet(outletPagination.outlets[0])

    }
    
    func outletTwoButtonTapped() {
        showOutletDetailsForOutlet(outletPagination.outlets[1])
    }
    
}

extension DashboardViewController: OutletSingleTableViewCellDelegate {
    
    func outletButtonTapped() {
        showOutletDetailsForOutlet(outletPagination.outlets[0])
    }
    
}

extension DashboardViewController: LastOrderContentTableViewCellDelegate {
    
    func showMoreOrders() {
        let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let orderHistoryViewController = mainStoryboard.instantiateViewController(withIdentifier: ScreenID.OrderHistoryViewController.rawValue) as! OrderHistoryViewController
        self.tabBarController?.tabBar.isHidden = true
        orderHistoryViewController.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(orderHistoryViewController, animated: true)
    }
    
}
