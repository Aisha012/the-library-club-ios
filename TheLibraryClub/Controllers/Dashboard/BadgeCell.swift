//
//  BadgeCell.swift
//  TheLibraryClub
//
//  Created by Namespace  on 16/07/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import UIKit

class BadgeCell: UITableViewCell, UICollectionViewDataSource, UICollectionViewDelegate{
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var btnViewMore:UIButton!
    @IBOutlet weak var titleLabel:UILabel!
    weak var badgeDelegate:BadgeDualTableViewCellDelegate?

    var badges = [Badge]()

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return badges.count > 10 ? 10 : badges.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "badgeCell", for: indexPath) as! BadgeViewCell
        let promotion1 = badges[indexPath.row]
        cell.ivBadge.sd_setShowActivityIndicatorView(true)
        cell.ivBadge.sd_setIndicatorStyle(.gray)
        cell.ivBadge.sd_setImage(with: URL(string: promotion1.picture), completed: nil)
        cell.lblBadgeTitle.text = """
        \(promotion1.name!)
        
        \(promotion1.date!)
        """
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
          badgeDelegate?.viewMoreBadgeButtonTapped()
    }
    
    @IBAction func btnViewMoreTapped(_ sender: UIButton) {
        badgeDelegate?.viewMoreBadgeButtonTapped()
    }
    
    func startTimerWithTaskFlashIndicators(){
        self.collectionView.flashScrollIndicators()
        if #available(iOS 10.0, *) {
            Timer.scheduledTimer(withTimeInterval: 5, repeats: true) {[weak self] (timer) in
                UIView.animate(withDuration: 1, animations: {
                    self?.collectionView.flashScrollIndicators()
                })
            }
        } else {
            // Fallback on earlier versions
            Timer.scheduledTimer(timeInterval: 5, target: self, selector: #selector(setTimer), userInfo: nil, repeats: true)
        }
    }
    
   @objc func setTimer(){
    UIView.animate(withDuration: 1, animations: {
        self.collectionView.flashScrollIndicators()
    })
    }
    

}



class BadgeViewCell: UICollectionViewCell {
    @IBOutlet weak var ivBadge: UIImageView!
    @IBOutlet weak var lblBadgeTitle: UILabel!
}
