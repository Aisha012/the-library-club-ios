//
//  BookingViewController.swift
//  TheLibraryClub
//
//  Created by Phaninder on 08/02/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import UIKit
import RxSwift
import SDWebImage
import JSSAlertView

class BookingViewController: BaseViewController {

    @IBOutlet weak var tableView: UITableView!
    
    var activeTextField: UITextField!
    var labelFields = ["Outlet", "Date", "Time", "Available Promotions", "Number of Persons", "Email", "Phone", "Remarks"]
    var placeHolderFields = ["Outlet", "Date", "Time", "Available Promotions", "Number of Persons", "Email", "", "", ""]

    var disposableBag = DisposeBag()
    var booking = Booking()
    var outlets = [OutletLocation]()
    let pickerView = UIPickerView()
    let datePicker = UIDatePicker()
    let timePicker = UIDatePicker()
    var outletName: String = ""
    var outletID: String = ""
    var promotionPagination = PromotionPagination.init();
    var isFirstTime = true
    var isFetchingData = false
    var promotionObserver: Observable<PromotionPagination>!
    
    var testCounter = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.tableFooterView = UIView()
        pickerView.delegate = self
        pickerView.dataSource = self
        
        datePicker.datePickerMode = .date
        datePicker.minimumDate = Date()
        datePicker.addTarget(self, action: #selector(datePickerValueChanged(sender:)), for: .valueChanged)
        
        timePicker.datePickerMode = .time
        timePicker.addTarget(self, action: #selector(timePickerValueChanged(sender:)), for: .valueChanged)
        
        booking.date = getFormattedDate(Date(), dateFormat: "dd-MM-yyyy, E")
        booking.time = getFormattedTime(Date())
        booking.outletName = outletName
        
        if outlets.count < 1 {
            fetchLocations()
        } else if outlets.count > 0 {
            filterOutlets()
        } else if !outletID.isEmpty {
            isFirstTime = true
            self.promotionPagination.paginationType = .new
            fetchPromotions()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tabBarController?.tabBar.isHidden = true
    }

    func filterOutlets() {
        outlets = outlets.filter({ (outlet) -> Bool in
            return outlet.allowBooking
        })
    }
    
    func configureTextField(_ textField: UITextField) {
        textField.delegate = self
        textField.placeholder = placeHolderFields[textField.tag]
        textField.isSecureTextEntry = false
        textField.keyboardType = .asciiCapable
        textField.inputView = nil
        switch textField.tag {
        case BookingScreenTextFieldType.date.rawValue:
            textField.inputView = datePicker
        case BookingScreenTextFieldType.time.rawValue:
            textField.inputView = timePicker
        case BookingScreenTextFieldType.outlet.rawValue:
            textField.inputView = pickerView
        case BookingScreenTextFieldType.email.rawValue:
            textField.keyboardType = .emailAddress
//        case BookingScreenTextFieldType.phone.rawValue:
//            textField.keyboardType = .phonePad
        case BookingScreenTextFieldType.persons.rawValue:
            textField.keyboardType = .numberPad
        default:
            textField.keyboardType = .asciiCapable
        }
    }

    @objc func datePickerValueChanged(sender:UIDatePicker) {
        activeTextField.text = getFormattedDate(sender.date, dateFormat: "dd-MM-yyyy, E")
        isFirstTime = true
        self.promotionPagination.paginationType = .new
        fetchPromotions()
    }

    @IBAction func doneButtonTapped(_ sender: UIButton) {
        self.view.endEditing(true)
        guard isDataValid() else {
            return
        }
        bookTheOutlet()
    }
        
    @objc func timePickerValueChanged(sender:UIDatePicker) {
        activeTextField.text = getFormattedTime(sender.date)
    }
    
    func getFormattedTime(_ date: Date) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "HH:mm"
        return dateFormatter.string(from: date)
    }

    func updateDataIntoTextField(_ textField: UITextField, fromCell: Bool) {
        switch textField.tag {
        case BookingScreenTextFieldType.outlet.rawValue:
            fromCell ? (textField.text = booking.outletName) : (booking.outletName = textField.text!)
            break
        case BookingScreenTextFieldType.date.rawValue:
            fromCell ? (textField.text = booking.date) : (booking.date = textField.text!)
            break
        case BookingScreenTextFieldType.time.rawValue:
            fromCell ? (textField.text = booking.time) : (booking.time = textField.text!)
            break
        case BookingScreenTextFieldType.persons.rawValue:
            fromCell ? (textField.text = booking.persons) : (booking.persons = textField.text!)
            break
        case BookingScreenTextFieldType.email.rawValue:
            fromCell ? (textField.text = booking.email) : (booking.email = textField.text!)
            break
        case BookingScreenTextFieldType.phone.rawValue:
            fromCell ? (textField.text = booking.phone) : (booking.phone = textField.text!)
            break
        case BookingScreenTextFieldType.allergies.rawValue:
            fromCell ? (textField.text = booking.allergiesText) : (booking.allergiesText = textField.text!)
                booking.isAllergicSelected = textField.text != ""
            break
        case BookingScreenTextFieldType.others.rawValue:
            fromCell ? (textField.text = booking.othersText) : (booking.othersText = textField.text!)
                booking.isOthersSelected = textField.text != ""
            
            break
        default:
            break
        }
    }
    
    func isDataValid() -> Bool {
        if booking.outletName.isEmpty {
            Utilities.showToast(withString: "Please select an Outlet.".localized)
            return false
        }
        if booking.persons.isEmpty {
            Utilities.showToast(withString: "Please enter number of persons.".localized)
            return false
        }
        if booking.phone.isEmpty {
            Utilities.showToast(withString: "Please enter valid phone number.".localized)
            return false
        }
        if !booking.email.isValidEmailID() {
            Utilities.showToast(withString: "Please enter valid email.".localized)
            return false
        }
//        let outletValidation = isSelectedTimeValid()
//        if !outletValidation.isValid {
//            Utilities.showToast(withString: outletValidation.text)
//            return false
//        }
        return true
    }

    func fetchLocations() {
        guard Utilities.shared.isNetworkReachable() else {
            self.showNoInternetMessage()
            return
        }
        
        Utilities.showHUD(forView: self.view, excludeViews: [])
        let outletLocationsObserver = ApiManager.shared.apiService.getOutletLocations()
        let outletLocationDisposable = outletLocationsObserver.subscribe(onNext: {(outletLocations) in
            self.outlets = outletLocations
            DispatchQueue.main.async(execute: {
                if !self.outletID.isEmpty {
                    self.isFirstTime = true
                    self.promotionPagination.paginationType = .new
                    self.fetchPromotions()
                    self.filterOutlets()
                }
                Utilities.hideHUD(forView: self.view)
                self.tableView.reloadData()
            })
        }, onError: {(error) in
            DispatchQueue.main.async(execute: {
                Utilities.hideHUD(forView: self.view)
            })
        })
        outletLocationDisposable.disposed(by: disposableBag)
    }
    
    
    func bookTheOutlet() {
        guard Utilities.shared.isNetworkReachable() else {
            self.showNoInternetMessage()
            return
        }
        
        let index = outlets.index { $0.title == booking.outletName }
        guard let _ = index else {
            return
        }
        
        let date = getFormattedDate(datePicker.date, dateFormat: "yyyy-MM-dd")
        
        
        var params = ["user_id": UserStore.shared.userId,
                      "outlet_id": String(outletID),
                      "booking_date": date,
                      "booking_time": booking.time,
                      "pax": booking.persons,
                      "email": booking.email,
                      "phone": booking.phone,
                      "name": booking.name,
                      "anniversay": booking.isAnniversarySelected ? "1" : "0",
                      "birthday": booking.isBirthdaySelected ? "1" : "0"]
        
//        if booking.isOthersSelected {
//            params["remarks"] = booking.othersText
//        }
         params["remarks"] = booking.isOthersSelected ? booking.othersText : ""
            params["allergy"] = booking.isAllergicSelected ? booking.allergiesText : ""
        
        Utilities.showHUD(forView: self.view, excludeViews: [])
        let bookingObserver = ApiManager.shared.apiService.reserveAnOutlet(params)
        let bookingDisposable = bookingObserver.subscribe(onNext: {(message) in

            DispatchQueue.main.async(execute: {
                Utilities.hideHUD(forView: self.view)
                self.navigationController?.popViewController(animated: true)
                Utilities.showToast(withString: message)
            })
        }, onError: {(error) in
            DispatchQueue.main.async(execute: {
                
                Utilities.hideHUD(forView: self.view)
                if let error_ = error as? ResponseError {
                    Utilities.showToast(withString: error_.description())
                } else {
                    JSSAlertView().danger(
                        self,
                        title: AppName,
                        text: error.localizedDescription,
                        buttonText: "Ok".localized)
                }
            })
        })
        bookingDisposable.disposed(by: disposableBag)
    }

    func fetchPromotions(isForPaging: Bool = false) {
        guard Utilities.shared.isNetworkReachable() else {
            self.showNoInternetMessage()
            return
        }
        
        if promotionPagination.paginationType != .old {
            self.promotionPagination.currentPageNumber = 1
            if isFirstTime == true {
                Utilities.showHUD(forView: self.view, excludeViews: [])
            } else {
                UIApplication.shared.isNetworkActivityIndicatorVisible = true
            }
        } else {
            UIApplication.shared.isNetworkActivityIndicatorVisible = true
        }
        
        isFetchingData = true
        let date = getFormattedDate(datePicker.date, dateFormat: "yyyy-MM-dd")

        promotionObserver = ApiManager.shared.apiService.fetchPromotions(pageNumber: String(promotionPagination.currentPageNumber), searchString: "", lat: "", long: "", favorite: false, outletId: outletID, date: date)
        let promotionDisposable = promotionObserver.subscribe(onNext: {(paginationObject) in
            self.promotionPagination.appendDataFromObject(paginationObject)
            self.isFetchingData = false
            DispatchQueue.main.async(execute: {
                Utilities.hideHUD(forView: self.view)
                if isForPaging == true {
                    let cell = self.tableView.cellForRow(at: IndexPath.init(row: BookingScreenRowType.promotion.rawValue, section: 0)) as! AvailablePromotionsTableViewCell
                    cell.collectionView.reloadData()
                } else {
                    self.tableView.reloadRows(at: [IndexPath.init(row: BookingScreenRowType.promotion.rawValue, section: 0)], with: .automatic)
                }
                if self.promotionPagination.paginationType != .old && self.isFirstTime == true {
                    self.isFirstTime = false
                } else {
                    UIApplication.shared.isNetworkActivityIndicatorVisible = false
                }
                //Do something here
            })
        }, onError: {(error) in
            self.isFetchingData = false
            DispatchQueue.main.async(execute: {
                Utilities.hideHUD(forView: self.view)
            })
        })
        promotionDisposable.disposed(by: disposableBag)
    }

    func isSelectedTimeValid() -> (isValid: Bool, text: String) {
        let index = outlets.index { $0.title == booking.outletName }
        if let _index = index, let schedule = outlets[_index].operatingHours {
            let today = datePicker.date
            let dayOfWeekInt =  NSCalendar.current.component(.weekday, from: today)
            var key = ""
            switch dayOfWeekInt {
            case 1:
                key = "sunday"
            case 2:
                key = "monday"
            case 3:
                key = "tuesday"
            case 4:
                key = "wednesday"
            case 5:
                key = "thursday"
            case 6:
                key = "friday"
            case 7:
                key = "saturday"
            default:
                break
            }
            
            let timeInformation = Utilities.getTimeInformationFromInterval(schedule[key]!)
            if timeInformation.hasError == false {
                let currentTimeDetails = Utilities.getTimeDetailsFromDate(timePicker.date)
                
                if (currentTimeDetails.hours > timeInformation.hour1) ||
                    (currentTimeDetails.hours == timeInformation.hour1 &&
                        currentTimeDetails.minutes >= timeInformation.minutes1) {
                    if currentTimeDetails.hours < timeInformation.hour2 {
                        return (isValid: true, text: "")
                    } else if currentTimeDetails.hours == timeInformation.hour2,
                        currentTimeDetails.minutes < timeInformation.minutes2 {
                        return (isValid: true, text: "")
                    } else if timeInformation.isAM2 {
                        return (isValid: true, text: "")
                    } else {
                        return (isValid: false, text: "Sorry, this outlet is open between \(schedule[key]!)")
                    }
                }
                
            } else {
                return (isValid: false, text: "Sorry, this outlet is closed on \(getFormattedDate(datePicker.date, dateFormat: "dd-MM-yyyy"))")
            }
        }
        return (isValid: false, text: "")
    }

}

extension BookingViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return labelFields.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == BookingScreenRowType.promotion.rawValue {
            let cell = tableView.dequeueReusableCell(withIdentifier: AvailablePromotionsTableViewCell.cellIdentifier(), for: indexPath) as! AvailablePromotionsTableViewCell
            cell.collectionView.delegate = self
            cell.collectionView.dataSource = self
            cell.collectionView.reloadData()
            return cell
        } else if indexPath.row == BookingScreenRowType.remarks.rawValue {
          let cell = tableView.dequeueReusableCell(withIdentifier: RemarksTableViewCell.cellIdentifier(), for: indexPath) as! RemarksTableViewCell
            cell.delegate = self
            cell.configureCell(booking: self.booking, index: indexPath.row)
            updateDataIntoTextField(cell.allergiesTextField, fromCell: true)
            configureTextField(cell.allergiesTextField)
            
            updateDataIntoTextField(cell.othersTextField, fromCell: true)
            configureTextField(cell.othersTextField)

            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: TextFieldTableViewCell.cellIdentifier(), for: indexPath) as! TextFieldTableViewCell
            cell.configureCellForRowAtIndex(indexPath.row, withText: labelFields[indexPath.row])
            updateDataIntoTextField(cell.inputTextField, fromCell: true)
            configureTextField(cell.inputTextField)
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == BookingScreenRowType.promotion.rawValue {
            if promotionPagination.promotions.count > 0 {
                return 155.0
            } else {
                return 0
            }
        } else if indexPath.row == BookingScreenRowType.remarks.rawValue {
            return 328.0
        } else {
            return 79.0
        }

    }
}

extension BookingViewController: UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        activeTextField = textField
        if activeTextField.tag == BookingScreenTextFieldType.outlet.rawValue {
            managePickerView()
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        updateDataIntoTextField(textField, fromCell: false)
        
        self.tableView.reloadData()
  
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if (textField.text?.isEmpty)! && string == " " {
            return false
        }
        let char = string.cString(using: String.Encoding.utf8)!
        let isBackSpace = strcmp(char, "\\b")
        if (isBackSpace == -92) {
            return true
        }
        return true
    }
    
}

extension BookingViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func managePickerView() {
        let index = outlets.index { $0.title == booking.outletName }
        if let outletIndex = index {
            pickerView.selectRow(outletIndex, inComponent: 0, animated: true)
        } else {
            pickerView.selectRow(0, inComponent: 0, animated: true)
            activeTextField.text = outlets[0].title
            outletID = String(outlets[0].outletID)
            isFirstTime = true
            self.promotionPagination.paginationType = .new
            fetchPromotions()
        }
        
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return outlets.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return outlets[row].title
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        outletID = String(outlets[row].outletID)
        activeTextField.text = outlets[row].title
        isFirstTime = true
        self.promotionPagination.paginationType = .new
        fetchPromotions()
    }
    
}

extension BookingViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return promotionPagination.promotions.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: PromotionBookingCollectionViewCell.cellIdentifier(), for: indexPath) as! PromotionBookingCollectionViewCell
        cell.promotionImageView.sd_setShowActivityIndicatorView(true)
        cell.promotionImageView.sd_setIndicatorStyle(.gray)
        let promotion = promotionPagination.promotions[indexPath.item]
        cell.promotionImageView.sd_setImage(with: URL(string: promotion.imageURL), completed: nil)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        var cellWidth = ScreenWidth
        if promotionPagination.promotions.count > 1 {
            cellWidth =  ScreenWidth/2
        }
        return CGSize(width: cellWidth, height: 111.0)
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if indexPath.item == promotionPagination.promotions.count - 4 {
//            && promotionPagination.hasMoreToLoad && !isFetchingData {
            fetchPromotions(isForPaging: true)
        }

    }
    
}

extension BookingViewController: RemarksTableViewCellDelegate {
    
    func anniversarySelected() {
        booking.isAnniversarySelected = !booking.isAnniversarySelected
        self.tableView.reloadData()
    }
    
    func birthdaySelected() {
        booking.isBirthdaySelected = !booking.isBirthdaySelected
        self.tableView.reloadData()
    }
    
    func othersSelected() {
        booking.isOthersSelected = !booking.isOthersSelected
        self.tableView.reloadData()
    }
    
    func allergiesSelected() {
        booking.isAllergicSelected = !booking.isAllergicSelected
        self.tableView.reloadData()
    }
}
