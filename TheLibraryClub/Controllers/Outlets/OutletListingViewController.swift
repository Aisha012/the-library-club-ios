//
//  OutletListingViewController.swift
//  TheLibraryClub
//
//  Created by Phaninder on 18/01/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import UIKit
import RxSwift
import CoreLocation
import JSSAlertView

class OutletListingViewController: BaseViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var tableViewTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var searchView: UIView!
    @IBOutlet weak var searchTextField: UITextField!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var logo: UIImageView!
    
    private let refreshControl = UIRefreshControl()

    var shouldShowTabBar = true
    var isFetchingData = false
    var disposableBag = DisposeBag()
    var outletPagination = OutletPagination.init();
    var isSearchEnabled = false
    var isFirstTime = true
    var outletObserver: Observable<OutletPagination>!
    var userLocation: CLLocation?
    let locationManager = CLLocationManager()
    var isUserLocationReceived = false
    var isFavoriteSelected = false
    var alreadyAPICalled = false
    var ignoreUserLocation = false
    var showTextField = false
    var currentSearchText = ""
    var promotionID = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        locationManager.delegate = self
        checkLocationAuthorizationStatus()
        if #available(iOS 10.0, *) {
            tableView.refreshControl = refreshControl
        } else {
            tableView.addSubview(refreshControl)
        }
        refreshControl.addTarget(self, action: #selector(refreshData(_:)), for: .valueChanged)

        NotificationCenter.default.addObserver(self,
                                               selector: #selector(deleteOutlet(_:)), name: .outletUnfavorited,
                                               object: nil)

        tableView.tableFooterView = UIView()
        tableView.reloadData()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        backButton.isHidden = shouldShowTabBar
        print("Outletlisting screen : ", shouldShowTabBar)
        self.navigationController?.tabBarController?.tabBar.isHidden = !shouldShowTabBar
        self.titleLabel.isHidden = shouldShowTabBar
        self.logo.isHidden = !shouldShowTabBar
    }

     override func viewDidAppear(_ animated: Bool) {
        self.navigationController?.tabBarController?.tabBar.isHidden = false
      }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
       // self.navigationController?.tabBarController?.tabBar.isHidden = true

    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: .outletUnfavorited, object: nil)
    }
    
    @objc func refreshData(_ sender: Any) {
        self.outletPagination.paginationType = .reload
//        self.clearFilters()
        checkLocationAuthorizationStatus()
//        fetchOutlets()
        if let refreshControl = sender as? UIRefreshControl {
            refreshControl.endRefreshing()
        }
    }

    @objc func deleteOutlet(_ notification: NSNotification) {
        if let outletID = notification.userInfo?["outletID"] as? Int,
            isFavoriteSelected == true {
            let index = self.outletPagination.outlets.index(where: { (outlet) -> Bool in
                return outlet.id == outletID
            })
            if let deleteIndex = index {
                self.outletPagination.outlets.remove(at: deleteIndex)
                DispatchQueue.main.async(execute: {
                    self.tableView.reloadData()
                })
            }
        }
    }

    func getLocationCoordinates() -> (lat: String, long: String) {
        
        guard ignoreUserLocation == false else {
            return (lat: "", long: "")
        }
        
        guard let location = userLocation else {
            return (lat: "", long: "")
        }
        return (lat: String(location.coordinate.latitude), long: String(location.coordinate.longitude))
    }
    
    func clearFilters() {
        self.isFavoriteSelected = false
        ignoreUserLocation = false
        searchTextField.text = ""
        checkLocationAuthorizationStatus()
    }

    func fetchOutlets() {
        guard Utilities.shared.isNetworkReachable() else {
            self.showNoInternetMessage()
            return
        }
        
        if outletPagination.paginationType != .old {
            self.outletPagination.currentPageNumber = 1
            if  isFirstTime == true {
                Utilities.showHUD(forView: self.view, excludeViews: [])
            } else {
                UIApplication.shared.isNetworkActivityIndicatorVisible = true
            }
        } else {
            UIApplication.shared.isNetworkActivityIndicatorVisible = true
        }
        
        isFetchingData = true
        let location = getLocationCoordinates()
        currentSearchText = searchTextField.text ?? ""
        outletObserver = ApiManager.shared.apiService.fetchOutlets(pageNumber: String(outletPagination.currentPageNumber), searchString: currentSearchText, lat: location.lat, long: location.long, favorite: isFavoriteSelected, promoId: promotionID)
        let outletDisposable = outletObserver.subscribe(onNext: {(paginationObject) in
            self.outletPagination.appendDataFromObject(paginationObject)
            self.isFetchingData = false
            DispatchQueue.main.async(execute: {
                Utilities.hideHUD(forView: self.view)
                if self.outletPagination.paginationType != .old && self.isFirstTime == true {
                    self.isFirstTime = false
                } else {
                    UIApplication.shared.isNetworkActivityIndicatorVisible = false
                }
                self.tableView.reloadData()
            })
        }, onError: {(error) in
            self.isFetchingData = false
            DispatchQueue.main.async(execute: {
                Utilities.hideHUD(forView: self.view)
            })
        })
        outletDisposable.disposed(by: disposableBag)
    }

    func checkLocationAuthorizationStatus() {
        if CLLocationManager.authorizationStatus() == .authorizedWhenInUse {
            locationManager.desiredAccuracy=kCLLocationAccuracyBest
            locationManager.distanceFilter=kCLDistanceFilterNone
            
            locationManager.startUpdatingLocation()
            if isUserLocationReceived == true {
                fetchDataAppropiately()
                alreadyAPICalled = true
            }
        } else if CLLocationManager.authorizationStatus() == .denied {
            if Utilities.hasTwoDaysPassed() {
                let alertview = JSSAlertView().show(
                    self,
                    title: "Location Permission".localized,
                    text: "Please grant location permission to use this feature.".localized,
                    buttonText: "OK".localized,
                    cancelButtonText: "Later".localized,
                    color: UIColor.white)
                UserStore.shared.locationPreviouslyShown = Int(Date().timeIntervalSince1970)
                alertview.addAction(navigateToSettingsPage)
                alertview.addCancelAction(fetchDataAppropiately)
            }
        } else {
            UserStore.shared.locationPreviouslyShown = Int(Date().timeIntervalSince1970)
            locationManager.requestWhenInUseAuthorization()
        }
    }
    
    func fetchDataAppropiately() {
        fetchOutlets()
    }

    
    @IBAction func searchIconTapped(_ sender: UIButton) {
        showTextField = !showTextField
        animateTextField()
    }
    
    func animateTextField() {
        if showTextField {
            searchTextField.becomeFirstResponder()
        } else {
            searchTextField.resignFirstResponder()
        }
        self.view.layoutIfNeeded()
        UIView.animate(withDuration: 0.7, delay: 0, usingSpringWithDamping: 0.5, initialSpringVelocity: 1, options: .curveEaseIn, animations: {
            self.tableViewTopConstraint.constant = self.showTextField ? 58 : 8
            self.searchView.isHidden = !self.showTextField
            self.view.layoutIfNeeded()
        }) { (completed) in
             
        }
    }
    
    @IBAction func valueChangedInSearchField(_ sender: Any) {
        if (outletObserver != nil && currentSearchText != searchTextField.text) {
            outletObserver = nil
        }
        outletPagination.paginationType = .new
        fetchOutlets()
    }

    @IBAction func filterButtonTapped(_ sender: UIButton) {
        self.tabBarController?.tabBar.isHidden = true
        let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let filterViewController = mainStoryboard.instantiateViewController(withIdentifier: ScreenID.FilterViewController.rawValue) as! FilterViewController
        filterViewController.isFavoriteChecked = isFavoriteSelected
        filterViewController.isLocationActive = !ignoreUserLocation
        filterViewController.delegate = self
        self.navigationController?.pushViewController(filterViewController, animated: true)
    }
    
}

extension OutletListingViewController: UITextFieldDelegate {
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if isFetchingData == false {
            outletPagination.paginationType = .new
            fetchOutlets()
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if (textField.text?.isEmpty)! && string == " " {
            return false
        }
        let char = string.cString(using: String.Encoding.utf8)!
        let isBackSpace = strcmp(char, "\\b")
        if (isBackSpace == -92) {
            return true
        }
        return true
    }
}

extension OutletListingViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let outletCount = outletPagination.outlets.count
        return outletPagination.hasMoreToLoad ? outletCount + 1 : outletCount
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard indexPath.row < outletPagination.outlets.count else {
            let cell = tableView.dequeueReusableCell(withIdentifier: LoadMoreTableViewCell.cellIdentifier(), for: indexPath) as! LoadMoreTableViewCell
            cell.activityIndicator.startAnimating()
            return cell
        }
        let cell = tableView.dequeueReusableCell(withIdentifier: OutletListingTableViewCell.cellIdentifier(), for: indexPath) as! OutletListingTableViewCell
        let outlet = outletPagination.outlets[indexPath.row]
        cell.distanceLabel.isHidden = ignoreUserLocation
        cell.configureCellWithOutlet(outlet)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        guard indexPath.row < outletPagination.outlets.count else {
            return 50.0
        }
        return 206.0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.tabBarController?.tabBar.isHidden = true
        let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let outletDetailViewController = mainStoryboard.instantiateViewController(withIdentifier: ScreenID.OutletDetailsViewController.rawValue) as! OutletDetailsViewController
        let outlet = outletPagination.outlets[indexPath.row]
        outletDetailViewController.outletID = String(outlet.id)
        outletDetailViewController.outletName = outlet.name
        self.navigationController?.pushViewController(outletDetailViewController, animated: true)
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row == self.outletPagination.outlets.count && self.outletPagination.hasMoreToLoad && !isFetchingData {
            fetchOutlets()
        }
    }
}


extension OutletListingViewController: UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let offsetY = self.tableView.contentOffset.y
        var parallexCells = [OutletListingTableViewCell]()
        for cell in self.tableView.visibleCells {
            if let parallexCell = cell as? OutletListingTableViewCell {
                parallexCells.append(parallexCell)
            }
        }
        for cell in parallexCells {
            let x = cell.outletImageView.frame.origin.x
            let w = cell.outletImageView.bounds.width
            let h = cell.outletImageView.bounds.height
            let y = ((offsetY - cell.frame.origin.y) / h) * 15
            cell.outletImageView.frame = CGRect(x: x, y: y, width: w, height: h)
            
        }
    }
}

extension OutletListingViewController: CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {

        let location = locations.last!
        if location.horizontalAccuracy > 0 {
            isUserLocationReceived = true
            userLocation = location
            locationManager.stopUpdatingLocation()
        }
        if alreadyAPICalled == false {
            checkLocationAuthorizationStatus()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .denied {
            userLocation = nil
            self.fetchDataAppropiately()
        } else if status == .authorizedWhenInUse {
            checkLocationAuthorizationStatus()
        }
    }
}

extension OutletListingViewController: FilterViewControllerDelegate {
    
    func filtersUpdated(isFavoriteChecked: Bool, isLocationChecked: Bool) {
        self.isFavoriteSelected = isFavoriteChecked
        self.ignoreUserLocation = !isLocationChecked
        self.outletPagination.paginationType = .new
        self.fetchOutlets()
        
    }
}


