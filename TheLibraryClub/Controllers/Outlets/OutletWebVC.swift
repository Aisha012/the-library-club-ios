//
//  OutletWebVC.swift
//  TheLibraryClub
//
//  Created by Namespace  on 20/07/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//
import WebKit
import UIKit

class OutletWebVC: BaseViewController, UIWebViewDelegate {
    @IBOutlet weak var webView: UIWebView!
    var url:String!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if let ur = url{
            let urlReq  = URLRequest(url: URL(string: ur)!)
            self.webView.delegate = self
            self.activityIndicator.hidesWhenStopped = true
            self.activityIndicator.startAnimating()
            self.webView.loadRequest(urlReq)
        }
       
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
    }
    
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        self.activityIndicator.stopAnimating()

    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        self.activityIndicator.stopAnimating()
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
