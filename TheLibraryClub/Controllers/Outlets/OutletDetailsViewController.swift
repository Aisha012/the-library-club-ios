//
//  OutletDetailsViewController.swift
//  TheLibraryClub
//
//  Created by Phaninder on 20/01/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import UIKit
import RxSwift
import SafariServices
import ActionSheetPicker_3_0
class OutletDetailsViewController: BaseViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var favoriteButton: UIButton!
    
    @IBOutlet weak var btnMenu: UIButton!
    @IBOutlet weak var blurView: UIVisualEffectView!
    @IBOutlet weak var weekView: UIView!
    
    @IBOutlet weak var mondayLabel: UILabel!
    @IBOutlet weak var mondayTimingLabel: UILabel!
    @IBOutlet weak var tuesdayLabel: UILabel!
    @IBOutlet weak var tuesdayTimingLabel: UILabel!
    @IBOutlet weak var wednesdayLabel: UILabel!
    @IBOutlet weak var wednesdayTimingLabel: UILabel!
    @IBOutlet weak var thursdayLabel: UILabel!
    @IBOutlet weak var thursdayTimingLabel: UILabel!
    @IBOutlet weak var fridayLabel: UILabel!
    @IBOutlet weak var fridayTimingLabel: UILabel!
    @IBOutlet weak var saturdayLabel: UILabel!
    @IBOutlet weak var saturdayTimingLabel: UILabel!
    @IBOutlet weak var sundayLabel: UILabel!
    @IBOutlet weak var sundayTimingLabel: UILabel!
    @IBOutlet weak var weekViewVeritcalConstraint: NSLayoutConstraint!
    
    var outletID: String!
    var outletName: String!
    var outletDescription: OutletDescription!
    var promotionsCellHeight:CGFloat = 0.0
    var disposableBag = DisposeBag()
    var outletStatus = "Closed Now"
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        titleLabel.text = outletName
        tableView.register(UINib.init(nibName: OutletHeaderTableViewCell.cellIdentifier(), bundle: nil), forCellReuseIdentifier: OutletHeaderTableViewCell.cellIdentifier())

        tableView.register(UINib.init(nibName: OutletContactTableViewCell.cellIdentifier(), bundle: nil), forCellReuseIdentifier: OutletContactTableViewCell.cellIdentifier())
        
        tableView.register(UINib.init(nibName: DescriptionTableViewCell.cellIdentifier(), bundle: nil), forCellReuseIdentifier: DescriptionTableViewCell.cellIdentifier())

        tableView.register(UINib.init(nibName: OutletOpeningHoursTableViewCell.cellIdentifier(), bundle: nil), forCellReuseIdentifier: OutletOpeningHoursTableViewCell.cellIdentifier())
        
        tableView.register(UINib.init(nibName: OutletMapTableViewCell.cellIdentifier(), bundle: nil), forCellReuseIdentifier: OutletMapTableViewCell.cellIdentifier())

        tableView.register(UINib.init(nibName: HorizontalPromotionTableViewCell.cellIdentifier(), bundle: nil), forCellReuseIdentifier: HorizontalPromotionTableViewCell.cellIdentifier())
        
        tableView.reloadData()
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(dismissWeekView))
        blurView.addGestureRecognizer(tap)
        fetchOutletDetails()
        self.btnMenu.layer.cornerRadius = 15
        self.btnMenu.layer.borderWidth = 1
        self.btnMenu.layer.borderColor = #colorLiteral(red: 0.3536440134, green: 0.3536530137, blue: 0.3536481857, alpha: 1)
        self.btnMenu.clipsToBounds = true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setFavoriteIcon() {
        let imageName = self.outletDescription.isFavorite ? "heart" : "heart_empty"
        self.favoriteButton.setImage(UIImage(named: imageName), for: .normal)
    }
    
    func fetchOutletDetails() {
        print(Utilities.shared.isNetworkReachable())
        guard Utilities.shared.isNetworkReachable() else {
            self.showNoInternetMessage()
            return
        }
        
        Utilities.showHUD(forView: self.view, excludeViews: [])
        let outletObserver = ApiManager.shared.apiService.getOutletDetailsWithID(outletID)
        let outletDisposable = outletObserver.subscribe(onNext: {(outlet) in
            self.outletDescription = outlet
            DispatchQueue.main.async(execute: {
                Utilities.hideHUD(forView: self.view)
                self.btnMenu.isHidden = self.outletDescription.menu! == "" ? true : false
                self.setFavoriteIcon()
                self.configureWeekView()
                self.tableView.reloadData()
            })
        }, onError: {(error) in
            DispatchQueue.main.async(execute: {
                Utilities.hideHUD(forView: self.view)
            })
        })
        outletDisposable.disposed(by: disposableBag)
    }
    
    func favoriteOutlet() {
        guard Utilities.shared.isNetworkReachable() else {
            self.showNoInternetMessage()
            return
        }
        
        Utilities.showHUD(forView: self.view, excludeViews: [])
        let favoriteObserver = ApiManager.shared.apiService.favoriteOutlet(withId: outletID, userId: UserStore.shared.userId)
        let favoriteDisposable = favoriteObserver.subscribe(onNext: {(message) in
            self.outletDescription.isFavorite = true
            DispatchQueue.main.async(execute: {
                Utilities.hideHUD(forView: self.view)
                NotificationCenter.default.post(name: .outletFavorited, object: nil, userInfo:nil)
             //   Utilities.showToast(withString: message)
                self.setFavoriteIcon()
            })
        }, onError: {(error) in
            DispatchQueue.main.async(execute: {
                Utilities.hideHUD(forView: self.view)
            })
        })
        favoriteDisposable.disposed(by: disposableBag)
    }

    
    func unFavoriteOutlet() {
        guard Utilities.shared.isNetworkReachable() else {
            self.showNoInternetMessage()
            return
        }
        
        Utilities.showHUD(forView: self.view, excludeViews: [])
        let unFavoriteObserver = ApiManager.shared.apiService.unFavoriteOutlet(withId: outletID)
        let unFavoriteDisposable = unFavoriteObserver.subscribe(onNext: {(message) in
            self.outletDescription.isFavorite = false
            DispatchQueue.main.async(execute: {
                Utilities.hideHUD(forView: self.view)
                let userInfo: [String: Int] = ["outletID": Int(self.outletID)!];
                NotificationCenter.default.post(name: .outletUnfavorited, object: nil, userInfo:userInfo)
                
              //  Utilities.showToast(withString: message)
                self.setFavoriteIcon()
            })
        }, onError: {(error) in
            DispatchQueue.main.async(execute: {
                Utilities.hideHUD(forView: self.view)
            })
        })
        unFavoriteDisposable.disposed(by: disposableBag)
    }

    func navigateToPromotionDescription(_ promotion: Promotion) {
        let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let promotionDetailViewController = mainStoryboard.instantiateViewController(withIdentifier: ScreenID.PromotionDetailViewController.rawValue) as! PromotionDetailViewController
        promotionDetailViewController.promotionID = String(promotion.id)
        promotionDetailViewController.promotionName = promotion.name
        self.navigationController?.pushViewController(promotionDetailViewController, animated: true)
    }
    
    func pushToWebViewWithURL(_ url: String) {
        if let url = URL(string: url) {
            if #available(iOS 11.0, *) {
                let config: SFSafariViewController.Configuration = SFSafariViewController.Configuration.init()
                config.entersReaderIfAvailable = true
                let vc = SFSafariViewController(url: url, configuration: config)
                present(vc, animated: true, completion: nil)
            } else {
                let safariController = SFSafariViewController(url: url)
                self.present(safariController, animated: true, completion: nil)
            }
        }
    }

    @IBAction func favoriteButtonTapped(_ sender: UIButton) {
        if outletDescription.isFavorite == true {
            unFavoriteOutlet()
        } else {
            favoriteOutlet()
        }
    }
    
    
    
    
    @IBAction func btnOnTapMenu(_ sender: UIButton) {
        if let url = self.outletDescription.menu{
            self.performSegue(withIdentifier: "SegueWebview", sender: url)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let iden = segue.identifier{
            if iden == "SegueWebview"{
                let dest = segue.destination as! OutletWebVC
                dest.url = self.outletDescription.menu!
            }
        }
    }
    
    
    @objc func dismissWeekView() {
        self.view.layoutIfNeeded()
        UIView.animate(withDuration: 0.9, delay: 0, usingSpringWithDamping: 0.5, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            self.weekViewVeritcalConstraint.constant = ScreenHeight + 127.0
            self.blurView.alpha = 0
            self.view.layoutIfNeeded()
        }) { (completed) in
            if completed {
                self.blurView.isHidden = true
                self.blurView.alpha = 1
                self.weekView.isHidden = true
            }
        }
    }
    
    @IBAction func btnOnTapReservations(btn:UIButton){
        let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let bookingViewController = mainStoryboard.instantiateViewController(withIdentifier: ScreenID.BookingViewController.rawValue) as! BookingViewController
        bookingViewController.outletName = outletDescription.name
        bookingViewController.outletID = String(outletDescription.id)
        self.navigationController?.pushViewController(bookingViewController, animated: true)
    }
    
    func configureWeekView() {
      //  mondayTimingLabel.text = "12:59 PM - 12:59 AM,12:45 AM - 12:45 PM".replacingOccurrences(of: ",", with: "\r\n")
        if let schedule = outletDescription.operatingHours {
            let today = Date()
            let dayOfWeekInt =  NSCalendar.current.component(.weekday, from: today)
            let font = UIFont(name: "Roboto-Regular", size: 15.0)!
            var key = ""
            switch dayOfWeekInt {
            case 1:
                sundayTimingLabel.font = font
                sundayLabel.font = font
                key = "sunday"
            case 2:
                mondayTimingLabel.font = font
                mondayLabel.font = font
                key = "monday"
            case 3:
                tuesdayTimingLabel.font = font
                tuesdayLabel.font = font
                key = "tuesday"
            case 4:
                wednesdayTimingLabel.font = font
                wednesdayLabel.font = font
                key = "wednesday"
            case 5:
                thursdayTimingLabel.font = font
                thursdayLabel.font = font
                key = "thursday"
            case 6:
                fridayTimingLabel.font = font
                fridayLabel.font = font
                key = "friday"
            case 7:
                saturdayTimingLabel.font = font
                saturdayLabel.font = font
                key = "saturday"
            default:
                break
            }
            
            mondayTimingLabel.text = schedule["monday"]?.replacingOccurrences(of: ",", with: "\n") ?? "Closed"
            tuesdayTimingLabel.text = schedule["tuesday"]?.replacingOccurrences(of: ",", with: "\n")  ?? "Closed"
            wednesdayTimingLabel.text = schedule["wednesday"]?.replacingOccurrences(of: ",", with: "\n")  ?? "Closed"
            thursdayTimingLabel.text = schedule["thursday"]?.replacingOccurrences(of: ",", with: "\n")  ?? "Closed"
            fridayTimingLabel.text = schedule["friday"]?.replacingOccurrences(of: ",", with: "\n")  ?? "Closed"
            saturdayTimingLabel.text = schedule["saturday"]?.replacingOccurrences(of: ",", with: "\n")  ?? "Closed"
            sundayTimingLabel.text = schedule["sunday"]?.replacingOccurrences(of: ",", with: "\n")  ?? "Closed"
            let timeArray  = schedule[key]?.components(separatedBy: " , ")
            if timeArray != nil && timeArray?.count == 2 {
                if getFirstTimeIntervalStatus(timeKey: timeArray![0])  == "Opened Now" || getSecondTimeIntervalStatus(timeKey: timeArray![1]) == "Opened Now"{
                    outletStatus = "Opened Now"
                }
            }else if schedule[key] != nil{
                if getFirstTimeIntervalStatus(timeKey:schedule[key]!)  == "Opened Now" {
                    outletStatus = "Opened Now"
                }
            }
           
        }
    }
    
    func getFirstTimeIntervalStatus(timeKey:String)-> String{
        let timeInformation = Utilities.getTimeInformationFromInterval(timeKey)
        if timeInformation.hasError == false {
            let currentTimeDetails = Utilities.getTimeDetailsFromDate(Date())
            
            if (currentTimeDetails.hours > timeInformation.hour1) ||
                (currentTimeDetails.hours == timeInformation.hour1 &&
                    currentTimeDetails.minutes >= timeInformation.minutes1) {
                if currentTimeDetails.hours < timeInformation.hour2 {
                    return "Opened Now"
                } else if currentTimeDetails.hours == timeInformation.hour2,
                    currentTimeDetails.minutes < timeInformation.minutes2 {
                    return "Opened Now"
                } else if timeInformation.isAM2 {
                    return "Opened Now"
                }
            }
        }
        return "Closed Now"

    }
    
    
    func getSecondTimeIntervalStatus(timeKey:String)->String{
        let timeInformation = Utilities.getTimeInformationFromInterval(timeKey)
        if timeInformation.hasError == false {
            let currentTimeDetails = Utilities.getTimeDetailsFromDate(Date())
            
            if (currentTimeDetails.hours > timeInformation.hour1) ||
                (currentTimeDetails.hours == timeInformation.hour1 &&
                    currentTimeDetails.minutes >= timeInformation.minutes1) {
                if currentTimeDetails.hours < timeInformation.hour2 {
                    return "Opened Now"
                } else if currentTimeDetails.hours == timeInformation.hour2,
                    currentTimeDetails.minutes < timeInformation.minutes2 {
                    return "Opened Now"
                } else if timeInformation.isAM2 {
                    return"Opened Now"
                }
            }
        }
        return "Closed Now"
    }
}

extension OutletDetailsViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let _ = outletDescription {
            return 6
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.row {
            
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: OutletHeaderTableViewCell.cellIdentifier(), for: indexPath) as! OutletHeaderTableViewCell
            cell.outletImageView.sd_setShowActivityIndicatorView(true)
            cell.outletImageView.sd_setIndicatorStyle(.gray)
            cell.outletImageView.sd_setImage(with: URL(string: outletDescription.imageURL), completed: nil)
            cell.btnReservation.isHidden = !outletDescription.allowBooking
            cell.constraintHeight.constant = outletDescription.allowBooking == true ? 41:0
            return cell
        case 5:
            let cell = tableView.dequeueReusableCell(withIdentifier: OutletContactTableViewCell.cellIdentifier(), for: indexPath) as! OutletContactTableViewCell
            cell.delegate = self
            cell.configureCellWithOutletDetails(outletDescription)
            return cell
        case 2:
            let cell = tableView.dequeueReusableCell(withIdentifier: DescriptionTableViewCell.cellIdentifier(), for: indexPath) as! DescriptionTableViewCell
            cell.descriptionLabel.text = outletDescription.info
            return cell
        case 3:
            let cell = tableView.dequeueReusableCell(withIdentifier: OutletOpeningHoursTableViewCell.cellIdentifier(), for: indexPath) as! OutletOpeningHoursTableViewCell
            
            cell.configureCellWithText(outletStatus, isClickable: outletDescription.operatingHours != nil)
            cell.delegate = self
            return cell
        case 4:
            let cell = tableView.dequeueReusableCell(withIdentifier: OutletMapTableViewCell.cellIdentifier(), for: indexPath) as! OutletMapTableViewCell
            cell.delegate = self
            cell.configureCell(outlet: outletDescription)
            return cell
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: HorizontalPromotionTableViewCell.cellIdentifier(), for: indexPath) as! HorizontalPromotionTableViewCell
            promotionsCellHeight = cell.configureCellWithPromotions(outletDescription.promotions)
            if outletDescription.promotions.count == 0 {
                cell.emptyPlaceholderLabel.isHidden = false
            } else {
                cell.emptyPlaceholderLabel.isHidden = true
            }
            cell.delegate = self
            return cell
        default:
        return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.row {
        case 0:
            return 190
        case 5:
            return 325
        case 2:
            tableView.estimatedRowHeight = 120.0
            return UITableViewAutomaticDimension
        case 3:
            return 117
        case 4:
            return 266
        case 1:
            return promotionsCellHeight
        default:
            return 0
        }
    }
}

extension OutletDetailsViewController: OutletOpeningHoursTableViewCellDelegate {
    
    func viewWeekButtonTapped() {
        if let _ = outletDescription.operatingHours{
            weekViewVeritcalConstraint.constant = -(ScreenHeight / 2) - 127.0
            self.weekView.isHidden = false
            self.view.layoutIfNeeded()
            UIView.animate(withDuration: 0.7, delay: 0, usingSpringWithDamping: 0.5, initialSpringVelocity: 1, options: .curveEaseIn, animations: {
                self.weekViewVeritcalConstraint.constant = 0
                self.blurView.isHidden = false
                self.view.layoutIfNeeded()
            }) { (completed) in
            }
        }
    }
       
}

extension OutletDetailsViewController: HorizontalPromotionTableViewCellDelegate {
    
    func viewMoreOfPromotionOneTapped() {
        navigateToPromotionDescription(outletDescription.promotions[0])
    }
    
    func viewMoreOfPromotionTwoTapped() {
        navigateToPromotionDescription(outletDescription.promotions[1])
    }
    
    func viewMorePromotionsTapped() {
        let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let promotionListingViewController = mainStoryboard.instantiateViewController(withIdentifier: ScreenID.PromotionListingViewController.rawValue) as! PromotionListingViewController
        promotionListingViewController.shouldShowTabBar = false
        promotionListingViewController.outletID = outletID
        self.navigationController?.pushViewController(promotionListingViewController, animated: true)
    }
}

extension OutletDetailsViewController: OutletMapTableViewCellDelegate {
    
    func mapPinTapped() {
        let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let mapViewController = mainStoryboard.instantiateViewController(withIdentifier: ScreenID.MapViewController.rawValue) as! MapViewController
        mapViewController.shouldShowTabBar = false
        mapViewController.outletLocations  = [outletDescription.outletLocation!]
        self.navigationController?.pushViewController(mapViewController, animated: true)
    }
    
}

extension OutletDetailsViewController: OutletContactTableViewCellDelegate {
    
    func showDialPad() {
        if let phone = outletDescription.phone, !phone.isEmpty {
            let condensedNumber = phone.replacingOccurrences(of: " ", with: "")
            if let url = URL(string: "tel://" + condensedNumber) {
                UIApplication.shared.openURL(url)
            }
        }
    }
    
    func navigateToWebView(forTag tag: Int) {
        if tag == 1 {
            pushToWebViewWithURL(outletDescription.website)
        } else if tag == 2 {
            pushToWebViewWithURL("https://www.instagram.com/" + outletDescription.instagram)
        } else {
            pushToWebViewWithURL("https://www.facebook.com/" + outletDescription.facebook)
        }
    }
    
    func showBookingScreen() {
//        let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
//        let bookingViewController = mainStoryboard.instantiateViewController(withIdentifier: ScreenID.BookingViewController.rawValue) as! BookingViewController
//        bookingViewController.outletName = outletDescription.name
//        bookingViewController.outletID = String(outletDescription.id)
//        self.navigationController?.pushViewController(bookingViewController, animated: true)
    }
}
