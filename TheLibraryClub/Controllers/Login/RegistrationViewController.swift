//
//  RegistrationViewController.swift
//  TheLibraryClub
//
//  Created by Phaninder on 22/01/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import UIKit
import RxSwift
import JSSAlertView
import Alamofire
import Unbox

class RegistrationViewController: BaseViewController {

    @IBOutlet weak var tableView: UITableView!
    var activeTextField: UITextField!
    var userRegistration = UserRegistration()
    var disposableBag = DisposeBag()
    var fbobject:[String:String?]?
    var labelFields: [String] = []
    var placeHolderFields : [String] = []
//    let labelFields = ["First Name:", "Last Name:", "Email:", "Password:", "Confirm Password:", "Date of Birth:", "Gender:", "Phone Number:"]

    //    let placeHolderFields = ["John", "Doe", "john@doe.com", "At least 6 characters", "At least 6 characters", "Tap to select Date of Birth", "Tap to select Gender", ""]
    
    let genderArray = ["Male", "Female"]
    var isImageSelected = false
    var profileImage: UIImage!
    let pickerView = UIPickerView()
    let datePicker = UIDatePicker()
    var isFBLogin = false
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.tableFooterView = UIView()
        pickerView.delegate = self
        pickerView.dataSource = self
        datePicker.datePickerMode = .date
      isFBLogin =  UserDefaults.standard.value(forKey: "IsFbLogin") as! Bool
        labelFields = ["First Name:", "Last Name:", "Email:", "Password:", "Confirm Password:", "Date of Birth:", "Gender:", "Phone Number:"]
        placeHolderFields = ["John", "Doe", "john@doe.com", "At least 6 characters", "At least 6 characters", "Tap to select Date of Birth", "Tap to select Gender", ""]

        tableView.reloadData()
//        var components = DateComponents()
//        components.year = -150
//        let minDate = Calendar.current.date(byAdding: components, to: Date())
//
//        components.year = -18
//        let maxDate = Calendar.current.date(byAdding: components, to: Date())
//        datePicker.minimumDate = minDate!
        datePicker.maximumDate = Date()
        datePicker.addTarget(self, action: #selector(datePickerValueChanged(sender:)), for: .valueChanged)
        
        if fbobject != nil{
            userRegistration.firstName = (fbobject!["firstname"] ?? "")!
            userRegistration.lastName =  (fbobject!["lastname"] ?? "")!
            userRegistration.email = (fbobject!["username"] ?? "")!
            self.tableView.reloadData()
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func configureTextField(_ textField: UITextField) {
        textField.delegate = self
        textField.placeholder = placeHolderFields[textField.tag]
        textField.isSecureTextEntry = false
        textField.keyboardType = .asciiCapable
        textField.inputView = nil

        switch textField.tag {
        case RegistrationTextFieldType.email.rawValue:
            textField.keyboardType = .emailAddress
        case RegistrationTextFieldType.password.rawValue,
             RegistrationTextFieldType.rePassword.rawValue:
            textField.keyboardType = .asciiCapable
            textField.isSecureTextEntry = true
        case RegistrationTextFieldType.dob.rawValue:
            textField.inputView = datePicker
        case RegistrationTextFieldType.gender.rawValue:
            textField.inputView = pickerView
        case RegistrationTextFieldType.phoneNumber.rawValue:
            textField.keyboardType = .phonePad
        default:
            textField.keyboardType = .asciiCapable
        }
    }
   
    @objc func datePickerValueChanged(sender:UIDatePicker) {
        activeTextField.text = getFormattedDate(sender.date)
    }

    func registerUser() {
        guard Utilities.shared.isNetworkReachable() else {
            self.showNoInternetMessage()
            return
        }
        var gender = "0"
        if userRegistration.gender == "Male" {
            gender = "1"
        } else if userRegistration.gender == "Female" {
            gender = "2"
        }

        Utilities.showHUD(forView: self.view, excludeViews: [])
        let registerObserv = ApiManager.shared.apiService.registerUserWithEmail(userRegistration.email, firstName: userRegistration.firstName, lastName: userRegistration.lastName, password: userRegistration.password, gender: gender, dateOfBirth: userRegistration.dob, phoneNumber: userRegistration.phoneNumber)
        let registerDisposable = registerObserv.subscribe(onNext: {(user) in
            self.successRegister(user)
        }, onError: {(error) in
            DispatchQueue.main.async(execute: {
                Utilities.hideHUD(forView: self.view)
                Utilities.showToast(withString: "Failed to create account.")
            })
        })
        registerDisposable.disposed(by: disposableBag)
    }
    
    func registerUserWithImage() {
        let imageData = UIImageJPEGRepresentation(profileImage, 0.5)
        
        guard Utilities.shared.isNetworkReachable() else {
            self.showNoInternetMessage()
            return
        }
        var gender = "0"
        if userRegistration.gender == "Male" {
            gender = "1"
        } else if userRegistration.gender == "Female" {
            gender = "2"
        }
        Utilities.showHUD(forView: self.view, excludeViews: [])
        let params = ["email": userRegistration.email,
                      "firstname": userRegistration.firstName,
                      "lastname": userRegistration.lastName,
                      "dob": userRegistration.dob,
                      "password": userRegistration.password,
                      "gender": gender,
                      "phonenum": userRegistration.phoneNumber]
        
        let url = "http://librarycard.club/mcustomerregistration.php" /* your API url */
        
        let headers: HTTPHeaders = [
            "Content-type": "multipart/form-data"
        ]
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            for (key, value) in params {
                multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
            }
            
            if let data = imageData {
                multipartFormData.append(data, withName: "profilepic", fileName: "image.png", mimeType: "image/png")
            }
            
        }, usingThreshold: UInt64.init(), to: url, method: .post, headers: headers) { (result) in
            switch result{
            case .success(let upload, _, _):
                upload.responseJSON { response in
                    print("Succesfully uploaded")
                    if let _ = response.error{
                        DispatchQueue.main.async(execute: {
                            Utilities.hideHUD(forView: self.view)
                            Utilities.showToast(withString: "Failed to create account.")
                        })
                        return
                    }
                    let json = try? JSONSerialization.jsonObject(with: response.data!, options: .mutableContainers)
                    if let jsonArray = json as? [[String: AnyObject]] {
                        print(jsonArray)
                        let userDict = jsonArray[0]
                        do {
                            let user: User = try unbox(dictionary: userDict)
                            self.successRegister(user)
                        } catch {
                            DispatchQueue.main.async(execute: {
                                Utilities.hideHUD(forView: self.view)
                                Utilities.showToast(withString: "Failed to create account.")
                            })
                        }
                    }
                }
            case .failure(let error):
                DispatchQueue.main.async(execute: {
                    Utilities.hideHUD(forView: self.view)
                    Utilities.showToast(withString: "Failed to create account.")
                })
            }
        }

    }

    func successRegister(_ user: User) {
        UserStore.shared.isLoggedIn = true
        UserStore.shared.userId = user.ID
        UserStore.shared.emailId = user.email
        UserStore.shared.firstName = user.firstName
        UserStore.shared.lastName = user.lastName
        UserStore.shared.profilePic = user.profilePic
        UserStore.shared.DOB = user.dob
        UserStore.shared.gender = user.gender
        UserStore.shared.phoneNumber = user.phonenum
        UserStore.shared.cardNumber = user.cardNum
        DispatchQueue.main.async(execute: {
            Utilities.hideHUD(forView: self.view)
            self.clearAllFields()
            self.showSuccessAlert()
        })
    }
    
    
    func updateDataIntoTextField(_ textField: UITextField, fromCell: Bool) {
        switch textField.tag {
        case RegistrationTextFieldType.firstName.rawValue:
            fromCell ? (textField.text = userRegistration.firstName) : (userRegistration.firstName = textField.text!)
            break
        case RegistrationTextFieldType.lastName.rawValue:
            fromCell ? (textField.text = userRegistration.lastName) : (userRegistration.lastName = textField.text!)
            break
        case RegistrationTextFieldType.email.rawValue:
            fromCell ? (textField.text = userRegistration.email) : (userRegistration.email = textField.text!)
            break
        case RegistrationTextFieldType.password.rawValue:
            fromCell ? (textField.text = userRegistration.password) : (userRegistration.password = textField.text!)
            break
        case RegistrationTextFieldType.rePassword.rawValue:
            fromCell ? (textField.text = userRegistration.confirmPassword) : (userRegistration.confirmPassword = textField.text!)
            break
        case RegistrationTextFieldType.dob.rawValue:
            fromCell ? (textField.text = userRegistration.dob) : (userRegistration.dob = textField.text!)
            break
        case RegistrationTextFieldType.gender.rawValue:
            fromCell ? (textField.text = userRegistration.gender) : (userRegistration.gender = textField.text!)
            break
        case RegistrationTextFieldType.phoneNumber.rawValue:
            fromCell ? (textField.text = userRegistration.phoneNumber) : (userRegistration.phoneNumber = textField.text!)
            break
        default:
            break
        }
    }
    
    func isDataValid() -> Bool {
        if !userRegistration.firstName.isValidName() {
            Utilities.showToast(withString: "Please enter First Name.".localized)
            return false
        }
        if !userRegistration.lastName.isValidName() {
            Utilities.showToast(withString: "Please enter Last Name.".localized)
            return false
        }
        if !userRegistration.password.isValidPassword() &&  !isFBLogin {
            Utilities.showToast(withString: "Please enter Password.".localized)
            return false
        }
        if !userRegistration.confirmPassword.isValidPassword()  &&  !isFBLogin{
            Utilities.showToast(withString: "Please Confirm your password.".localized)
            return false
        }
        if userRegistration.password != userRegistration.confirmPassword   &&  !isFBLogin{
            Utilities.showToast(withString: "Password does not match.".localized)
            return false
        }
        if !userRegistration.email.isValidEmailID() {
            Utilities.showToast(withString: "Please enter valid email.".localized)
            return false
        }
//        if userRegistration.dob.isEmpty {
//            Utilities.showToast(withString: "Please select Date of Birth.".localized)
//            return false
//        }
//        if userRegistration.gender.isEmpty {
//            Utilities.showToast(withString: "Please select Gender.".localized)
//            return false
//        }
//        if !userRegistration.phoneNumber.isValidPhoneNumber() {
//            Utilities.showToast(withString: "Please enter valid Phone Number.".localized)
//            return false
//        }
        return true
    }
    
    func clearAllFields() {
        userRegistration =  UserRegistration()
        tableView.reloadData()
    }
    
    func showSuccessAlert() {
        let alertview = JSSAlertView().show(
            self,
            title: "Successfully Registered!".localized,
            text: "Please verify your email address by clicking the link sent to your inbox.".localized,
            buttonText: "OK".localized,
            color: UIColor.white
        )
        alertview.addAction(navigateToHomeScreen)
    }
    
    func navigateToHomeScreen() {
        let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let tabBarController = mainStoryboard.instantiateViewController(withIdentifier: ScreenID.TabBarController.rawValue) as! UITabBarController
        tabBarController.selectedIndex = 0

        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        appDelegate.window = UIWindow(frame: UIScreen.main.bounds)
        appDelegate.window?.rootViewController = tabBarController
        let statusBarView = UIView.init(frame: CGRect(x: 0, y: 0, width: ScreenWidth, height: 20))
        statusBarView.backgroundColor = UIColor.black
        appDelegate.window?.rootViewController?.view.addSubview(statusBarView)

        self.present(tabBarController, animated: true, completion: nil)
    }
    
}

extension RegistrationViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return labelFields.count + 2;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard indexPath.row != 0 else {
            let cell = tableView.dequeueReusableCell(withIdentifier: ProfilePictureTableViewCell.cellIdentifier(),
                                                     for: indexPath) as! ProfilePictureTableViewCell
            if isImageSelected == false, let url = URL(string: UserStore.shared.profilePic) {
                cell.profileImageView.sd_setImage(with: url, placeholderImage: UIImage.init(named: "dummy"), options: .retryFailed, completed: { (image, error, cacheType, url) in
                    if image != nil {
                        self.profileImage = image
                    }
                })
                cell.profileImageView?.sd_setImage(with: url, placeholderImage: UIImage.init(named: "dummy"))
            } else if let profilePicture = profileImage {
                cell.profileImageView.image = profilePicture
            } else {
                cell.profileImageView.image = UIImage(named: "dummy")
            }
            cell.delegate = self
            return cell
        }

        if indexPath.row < labelFields.count + 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: TextFieldTableViewCell.cellIdentifier(), for: indexPath) as! TextFieldTableViewCell
            cell.configureCellForRowAtIndex(indexPath.row - 1, withText: labelFields[indexPath.row - 1])
            updateDataIntoTextField(cell.inputTextField, fromCell: true)
            configureTextField(cell.inputTextField)
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: RegisterButtonTableViewCell.cellIdentifier(), for: indexPath) as! RegisterButtonTableViewCell
            cell.delegate = self
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 {
            return 107.0
        } else if indexPath.row < labelFields.count + 1 {
            if (isFBLogin && (indexPath.row == 4 || indexPath.row == 5)){
                return 0
            }else{
                return 79.0
            }
            
        } else {
            return 62.0
        }
    }
}

extension RegistrationViewController: UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        activeTextField = textField
        if textField.tag == RegistrationTextFieldType.dob.rawValue {
            activeTextField.text = "00/00/0000"//getFormattedDate(Date())
        } else if textField.tag == RegistrationTextFieldType.gender.rawValue {
            managePickerView()
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        updateDataIntoTextField(textField, fromCell: false)
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if (textField.text?.isEmpty)! && string == " " {
            return false
        }
        let char = string.cString(using: String.Encoding.utf8)!
        let isBackSpace = strcmp(char, "\\b")
        if (isBackSpace == -92) {
            return true
        }
        switch textField.tag {
        case RegistrationTextFieldType.firstName.rawValue, RegistrationTextFieldType.lastName.rawValue:
            return (textField.text?.count)! < NameMaxLimit
        case RegistrationTextFieldType.email.rawValue:
            return (textField.text?.count)! < EmailMaxLimit
        case RegistrationTextFieldType.password.rawValue, RegistrationTextFieldType.rePassword.rawValue:
            return (textField.text?.count)! < PasswordMaxLimit
        default:
            return true
        }
    }
    
}

extension RegistrationViewController: RegisterButtonTableViewCellDelegate {
    
    func registerButtonTapped() {
        self.view.endEditing(true)
        if isDataValid() {
            if isImageSelected == false {
                registerUser()
            } else {
                registerUserWithImage()
            }
        }
    }
    
}


extension RegistrationViewController: ProfilePictureTableViewCellDelegate {
    
    func showImageOptionsforButton(button: UIButton) {
        let alert = UIAlertController(title: "Choose Image", message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { _ in
            self.camera()
        }))
        
        alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { _ in
            self.photoLibrary()
        }))
        
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
        
        /*If you want work actionsheet on ipad
         then you have to use popoverPresentationController to present the actionsheet,
         otherwise app will crash on iPad */
        switch UIDevice.current.userInterfaceIdiom {
        case .pad:
            alert.popoverPresentationController?.sourceView = button
            alert.popoverPresentationController?.sourceRect = button.bounds
            alert.popoverPresentationController?.permittedArrowDirections = .up
        default:
            break
        }
        
        self.present(alert, animated: true, completion: nil)
    }
    
}

extension RegistrationViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func managePickerView() {
        userRegistration.gender = userRegistration.gender.isEmpty ? "Male" : userRegistration.gender
        activeTextField.text = userRegistration.gender
        let index = genderArray.index(of: userRegistration.gender)!
        pickerView.selectRow(index, inComponent: 0, animated: true)
    }

    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return genderArray.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return genderArray[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        activeTextField.text = genderArray[row]
    }
    
}

extension RegistrationViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func camera() {
        if UIImagePickerController.isSourceTypeAvailable(.camera){
            let myPickerController = UIImagePickerController()
            myPickerController.delegate = self;
            myPickerController.sourceType = .camera
            self.present(myPickerController, animated: true, completion: nil)
        }
        
    }
    
    func photoLibrary() {
        
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary){
            let myPickerController = UIImagePickerController()
            myPickerController.delegate = self;
            myPickerController.sourceType = .photoLibrary
            self.present(myPickerController, animated: true, completion: nil)
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        let image = info[UIImagePickerControllerOriginalImage] as! UIImage
        let cell = tableView.cellForRow(at: IndexPath(row: 0, section: 0)) as! ProfilePictureTableViewCell
        self.profileImage = image
        cell.profileImageView.image = image
        isImageSelected = true
        dismiss(animated:true, completion: nil)
    }
    
}
