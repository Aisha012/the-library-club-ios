//
//  ForgotPasswordViewController.swift
//  TheLibraryClub
//
//  Created by Phaninder on 22/01/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import UIKit
import RxSwift

class ForgotPasswordViewController: BaseViewController {

    @IBOutlet weak var emailAddressTextField: UITextField!
    var disposableBag = DisposeBag()

    override func viewDidLoad() {
        super.viewDidLoad()

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func submitButtonTapped(_ sender: UIButton) {
        self.view.endEditing(true)
        guard let email = emailAddressTextField.text, !email.isEmpty else {
            Utilities.showToast(withString: "Please enter email.".localized)
            return
        }
        
        guard Utilities.shared.isNetworkReachable() else {
            self.showNoInternetMessage()
            return
        }
        
        Utilities.showHUD(forView: self.view, excludeViews: [])
        let passwordObserver = ApiManager.shared.apiService.forgotPasswordForEmail(email)
        let passwordDisposable = passwordObserver.subscribe(onNext: {(message) in

            DispatchQueue.main.async(execute: {
                Utilities.hideHUD(forView: self.view)
                Utilities.showToast(withString: message)
                self.navigateBack(sender: sender)
            })
        }, onError: {(error) in
            DispatchQueue.main.async(execute: {
                Utilities.hideHUD(forView: self.view)
                Utilities.showToast(withString: "Email does not exist")

//                if let error_ = error as? ResponseError {
//                    Utilities.showToast(withString: error_.description())
//                } else {
//                    if !error.localizedDescription.isEmpty {
//                        Utilities.showToast(withString: error.localizedDescription)
//                    }
//                }
            })
        })
        passwordDisposable.disposed(by: disposableBag)
    }
    
}
