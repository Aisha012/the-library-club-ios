//
//  LoginViewController.swift
//  TheLibraryClub
//
//  Created by Phaninder on 17/01/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//
import FacebookLogin
import FBSDKCoreKit
import FBSDKLoginKit
import UIKit
import RxSwift
import JSSAlertView

class LoginViewController: BaseViewController {

    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var showPasswordButton: UIButton!
    @IBOutlet weak var signUpButton: UIButton!
    
    var showPassword = false
    var disposableBag = DisposeBag()

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        showPassword = false
        setUpPasswordFieldType()
        let buttonTitleStr = Utilities.getUnderLineTextWithFontFamily("Roboto-Medium", ofFontSize: 15.0, andText: "Sign Up")
        signUpButton.setAttributedTitle(buttonTitleStr, for: .normal)

    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func setUpPasswordFieldType() {
        passwordTextField.isSecureTextEntry = !showPassword
        passwordTextField.keyboardType = .asciiCapable
        let tmpString = passwordTextField.text
        passwordTextField.text = " "
        passwordTextField.text = tmpString
    }
    
    @IBAction func showPasswordButtonTapped(_ sender: UIButton) {
        showPassword = !showPassword
        setUpPasswordFieldType()
        showPasswordButton.setImage(UIImage.init(named: showPassword ? "open" : "close"), for: .normal)
    }
    
    @IBAction func forgotPasswordButtonTapped(_ sender: UIButton) {
        
    }
    
    @IBAction func loginButtonTapped(_ sender: UIButton) {
        self.view.endEditing(true)
        guard let email = emailTextField.text, !email.isEmpty,
            let password = passwordTextField.text, !password.isEmpty else {
                Utilities.showToast(withString: "Please enter valid inputs.".localized)
                return
        }

        guard Utilities.shared.isNetworkReachable() else {
            self.showNoInternetMessage()
            return
        }
        
        Utilities.showHUD(forView: self.view, excludeViews: [])
        let userObserver = ApiManager.shared.apiService.loginUserWithEmail(email, andPassword: passwordTextField.text!)
        let userDisposable = userObserver.subscribe(onNext: {(user) in
            UserStore.shared.isLoggedIn = true
            UserStore.shared.userId = user.ID
            UserStore.shared.emailId = user.email
            UserStore.shared.firstName = user.firstName
            UserStore.shared.lastName = user.lastName
            UserStore.shared.profilePic = user.profilePic
            UserStore.shared.DOB = user.dob
            UserStore.shared.gender = user.gender
            UserStore.shared.phoneNumber = user.phonenum
            UserStore.shared.cardNumber = user.cardNum
            DispatchQueue.main.async(execute: {
                Utilities.hideHUD(forView: self.view)
                let mainStoryboard = UIStoryboard(name:  "Main", bundle: nil)
                let tabBarController = mainStoryboard.instantiateViewController(withIdentifier: ScreenID.TabBarController.rawValue) as! UITabBarController
                
                tabBarController.selectedIndex = 0
                let appDelegate = UIApplication.shared.delegate as! AppDelegate

                appDelegate.window = UIWindow(frame: UIScreen.main.bounds)
                appDelegate.window?.rootViewController = tabBarController
                let statusBarView = UIView.init(frame: CGRect(x: 0, y: 0, width: ScreenWidth, height: 20))
                statusBarView.backgroundColor = UIColor.black
                appDelegate.window?.rootViewController?.view.addSubview(statusBarView)

                appDelegate.window?.makeKeyAndVisible()
            })
        }, onError: {(error) in
            DispatchQueue.main.async(execute: {
                Utilities.hideHUD(forView: self.view)
                Utilities.showToast(withString: "Invalid Credentials")

//                if let error_ = error as? ResponseError {
//                    Utilities.showToast(withString: error_.description())
//                } else {
//                    if !error.localizedDescription.isEmpty {
//                        Utilities.showToast(withString: error.localizedDescription)
//                    }
//                }
            })
        })
        userDisposable.disposed(by: disposableBag)
    }

    @IBAction func signupButtonTapped(_ sender: UIButton) {
        UserDefaults.standard.set(false, forKey: "IsFbLogin")

    }
    
    
    func checkEmailWithFB(email:String, fbObj:[String:String]){
        guard Utilities.shared.isNetworkReachable() else {
            self.showNoInternetMessage()
            return
        }
        
        Utilities.showHUD(forView: self.view, excludeViews: [])
        let userObserver = ApiManager.shared.apiService.fetchCheckEmailResponse(email: email)

        let userDisposable = userObserver.subscribe(onNext: {(user) in
            UserStore.shared.isLoggedIn = true
            UserStore.shared.userId = user.id!
            UserStore.shared.emailId = user.email!
            UserStore.shared.firstName = user.first_name!
            UserStore.shared.lastName = user.last_name!
            UserStore.shared.profilePic = user.profilepic ?? ""
            UserStore.shared.DOB = user.dob!
            UserStore.shared.gender = user.gender!
            UserStore.shared.phoneNumber = user.phonenum!
            UserStore.shared.cardNumber = user.cardnum!
            DispatchQueue.main.async(execute: {
                Utilities.hideHUD(forView: self.view)
                let mainStoryboard = UIStoryboard(name:  "Main", bundle: nil)
                let tabBarController = mainStoryboard.instantiateViewController(withIdentifier: ScreenID.TabBarController.rawValue) as! UITabBarController
                
                tabBarController.selectedIndex = 0
                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                
                appDelegate.window = UIWindow(frame: UIScreen.main.bounds)
                appDelegate.window?.rootViewController = tabBarController
                let statusBarView = UIView.init(frame: CGRect(x: 0, y: 0, width: ScreenWidth, height: 20))
                statusBarView.backgroundColor = UIColor.black
                appDelegate.window?.rootViewController?.view.addSubview(statusBarView)
                
                appDelegate.window?.makeKeyAndVisible()
            })
            
            
        }, onError: {(error) in
            DispatchQueue.main.async(execute: {
                Utilities.hideHUD(forView: self.view)
                //Utilities.showToast(withString: "Invalid Credentials")
                print(error.localizedDescription)
                
                if error.localizedDescription == "The Email ID does not exist."{
                    let alert = UIAlertController(title: "", message: "This user is not  registered with ACE Card. Do you want to register your account?", preferredStyle: .alert)
                    
                    // add an action (button)
                    alert.addAction(UIAlertAction(title: "Continue", style: .default, handler: {
                      action in
                        let controller  =  self.storyboard?.instantiateViewController(withIdentifier: "RegistrationViewController") as! RegistrationViewController
                        controller.fbobject = fbObj
                        self.navigationController?.pushViewController(controller, animated: true)
                        
                    }))
                    alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))

                    // show the alert
                    self.present(alert, animated: true, completion: nil)
                
                }
            })
        })
        userDisposable.disposed(by: disposableBag)

    }
  
    @IBAction func btnOnTapLoginWithFB(_ sender: UIButton) {
        UserDefaults.standard.set(true, forKey: "IsFbLogin")

        let fbLoginManager : FBSDKLoginManager = FBSDKLoginManager()
        fbLoginManager.logOut()
        fbLoginManager.logIn(withReadPermissions: ["public_profile", "email"], from: self) { (result, error) -> Void in
            if (error == nil){
                let fbloginresult : FBSDKLoginManagerLoginResult = result!
                if (result?.isCancelled)!{
                    return
                }
                if(fbloginresult.grantedPermissions.contains("email")) {
                    self.getFBUserData()
                }
            }
        }
    }
    

    
    func getFBUserData(){
        if((FBSDKAccessToken.current()) != nil){
            FBSDKGraphRequest(graphPath: "me", parameters: ["fields": "id, name, first_name, last_name, picture.type(large), email"]).start(completionHandler: {[weak self] (connection, result, error) -> Void in
                if (error == nil){
                    print(result as! [String: Any])
                    if let fbObject = result as? [String: Any] {
                        if let email = fbObject["email"] as? String {
                            let loginObject = [
                                "username": email,
                                "firstname": fbObject["first_name"] as! String,
                                "lastname": fbObject["last_name"] as! String,
                                "facebook_id": fbObject["id"] as! String
                                ] as [String : AnyObject]
                           
                            self?.checkEmailWithFB(email: email, fbObj: loginObject as! [String : String])
                        } else {
                            self?.showInputDialog(title: "Alert", subtitle: "Please Input your email address", actionTitle: "Submit", cancelTitle: "Cancel", inputPlaceholder: "Email", inputKeyboardType: .emailAddress, cancelHandler: { (action) in
                            }, actionHandler: { (email) in
                                let loginObject = ["username": email!]
                                self?.checkEmailWithFB(email: email!, fbObj: loginObject)
                            })
                        }
                    } else {
                        JSSAlertView().show(
                            self!,
                            title: "Error!",
                            text: "Failed while trying to login with Facebook",
                            buttonText: "OK",
                            color: UIColor.white)
                    }
                }
            })
        }
    }
}




extension UIViewController{
    func showInputDialog(title:String? = nil,
                         subtitle:String? = nil,
                         actionTitle:String? = "Add",
                         cancelTitle:String? = "Cancel",
                         inputPlaceholder:String? = nil,
                         inputKeyboardType:UIKeyboardType = UIKeyboardType.default,
                         cancelHandler: ((UIAlertAction) -> Swift.Void)? = nil,
                         actionHandler: ((_ text: String?) -> Void)? = nil) {
        
        let alert = UIAlertController(title: title, message: subtitle, preferredStyle: .alert)
        let cancelAction = UIAlertAction(title: cancelTitle, style: .cancel, handler: { (action:UIAlertAction) in
        })
        
        let okAction = UIAlertAction(title: actionTitle, style: .destructive, handler: { (action:UIAlertAction) in
            guard let textField =  alert.textFields?.first else {
                actionHandler?(nil)
                return
            }
            actionHandler?(textField.text)
        })
        okAction.isEnabled = false
        alert.addTextField { (textField:UITextField) in
            textField.placeholder = inputPlaceholder
            textField.keyboardType = inputKeyboardType
            NotificationCenter.default.addObserver(forName: .UITextFieldTextDidChange, object: textField, queue: OperationQueue.main, using:
                {_ in
                    // Being in this block means that something fired the UITextFieldTextDidChange notification.
                    
                    // Access the textField object from alertController.addTextField(configurationHandler:) above and get the character count of its non whitespace characters
                    let textCount = textField.text?.trimmingCharacters(in: .whitespacesAndNewlines).characters.count ?? 0
                    let textIsNotEmpty = textCount > 0
                    
                    // If the text contains non whitespace characters, enable the OK Button
                    okAction.isEnabled = textIsNotEmpty && textField.isValidEmail()
                    
            })
            
        }
        alert.addAction(okAction)
        alert.addAction(cancelAction)
        
        self.present(alert, animated: true, completion: nil)
    }
}


extension UITextField{
    func isValidEmail() -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,6}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: self.text!)
    }
}
