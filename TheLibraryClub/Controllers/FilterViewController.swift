//
//  FilterViewController.swift
//  TheLibraryClub
//
//  Created by Phaninder on 23/01/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import UIKit

protocol FilterViewControllerDelegate {
    func filtersUpdated(isFavoriteChecked: Bool, isLocationChecked: Bool)
}

class FilterViewController: BaseViewController {

    @IBOutlet weak var locationCheckBox: UIImageView!
    @IBOutlet weak var favoriteCheckBox: UIImageView!
    
    var isLocationActive = false
    var isFavoriteChecked = false
    var delegate: FilterViewControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureFilterCheckBoxes()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func configureFilterCheckBoxes() {
        let locationCheckbox = isLocationActive ? "checkbox_green" : "checkbox_black"
        let favoriteCheckbox = isFavoriteChecked ? "checkbox_green" : "checkbox_black"
        locationCheckBox.image = UIImage(named: locationCheckbox)
        favoriteCheckBox.image = UIImage(named: favoriteCheckbox)
    }
    
    @IBAction func sortByLocationButtonTapped(_ sender: UIButton) {
        isLocationActive = !isLocationActive
        configureFilterCheckBoxes()
    }
    
    @IBAction func filterByFavoritesButtonTapped(_ sender: UIButton) {
        isFavoriteChecked = !isFavoriteChecked
        configureFilterCheckBoxes()
    }
    
    @IBAction func applyFilterButtonTapped(_ sender: UIButton) {
        delegate?.filtersUpdated(isFavoriteChecked: isFavoriteChecked, isLocationChecked: isLocationActive)
        self.navigationController?.popViewController(animated: true)
    }
}
