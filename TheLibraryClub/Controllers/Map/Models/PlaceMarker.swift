//
//  PlaceMarker.swift
//  TheLibraryClub
//
//  Created by Namespace  on 16/07/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import UIKit
import GoogleMaps

class PlaceMarker: GMSMarker {
    var outlet:OutletLocation!
    init(place: CLLocationCoordinate2D, outlets:OutletLocation) {
        super.init()
        position = place
        //icon = #imageLiteral(resourceName: "marker")
        groundAnchor = CGPoint(x: 0.5, y: 1)
        appearAnimation = .pop
        self.outlet = outlets
    }
}
