//
//  MapViewController.swift
//  TheLibraryClub
//
//  Created by Phaninder on 18/01/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import UIKit
import MapKit
import RxSwift
import CoreLocation
import JSSAlertView
import GoogleMaps


class MapViewController: BaseViewController {
    
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var logo: UIImageView!
    var infoView:MarkerInfoView?
    var shouldShowTabBar = true
    let locationManager = CLLocationManager()
    var outletLocations: [OutletLocation]!
    let regionRadius: CLLocationDistance = 10000
    var disposableBag = DisposeBag()
    var tappedOutlet:OutletLocation!
    override func viewDidLoad() {
        super.viewDidLoad()
        locationManager.delegate = self
        mapView.delegate = self
        if let locations = self.outletLocations, locations.count > 0 {
            setUpMap()
        } else {
            fetchLocations()
        }
      
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        backButton.isHidden = shouldShowTabBar
        self.tabBarController?.tabBar.isHidden = !shouldShowTabBar
        self.titleLabel.isHidden = shouldShowTabBar
        self.logo.isHidden = !shouldShowTabBar
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        checkLocationAuthorizationStatus()
        self.navigationController?.tabBarController?.tabBar.isHidden = false

    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func checkLocationAuthorizationStatus() {
        if CLLocationManager.authorizationStatus() == .authorizedWhenInUse {
        } else if CLLocationManager.authorizationStatus() == .denied {
            let alertview = JSSAlertView().show(
                self,
                title: "Location Permission".localized,
                text: "Please grant location permission to use this feature.".localized,
                buttonText: "OK".localized,
                cancelButtonText: "Later".localized,
                color: UIColor.white)
            alertview.addAction(navigateToSettingsPage)
        } else {
            locationManager.requestWhenInUseAuthorization()
        }
    }
    
    func setUpMap() {
        
        
        if let userLoc = mapView.myLocation {
           // let region = MKCoordinateRegionMakeWithDistance(
              //  userLoc.coordinate, regionRadius, regionRadius)
            setMapZoomToRadius(lat: userLoc.coordinate.latitude, lng: userLoc.coordinate.longitude, mile: regionRadius)
            //mapView.camera = GMSCameraPosition.camera(withLatitude: userLoc.coordinate.latitude, longitude:userLoc.coordinate.longitude, zoom: 6.0)
         //   mapView.setRegion(region, animated: true)
        }
        
        addMarkers()
    }
    
    
    func setMapZoomToRadius(lat:Double, lng:Double,  mile:Double)
    {
        
        let center = CLLocationCoordinate2DMake(lat, lng)
        let radius: Double = (mile ) * 621.371
        
        let region = MKCoordinateRegionMakeWithDistance(center, radius * 2.0, radius * 2.0)
        
        
        let northEast = CLLocationCoordinate2DMake(region.center.latitude - region.span.latitudeDelta, region.center.longitude - region.span.longitudeDelta)
        let  southWest = CLLocationCoordinate2DMake(region.center.latitude + region.span.latitudeDelta, region.center.longitude + region.span.longitudeDelta)
        
        
        print("\(region.center.longitude)  \(region.span.longitudeDelta)")
        let bounds = GMSCoordinateBounds(coordinate: southWest, coordinate: northEast)
        
        let camera = mapView.camera(for: bounds, insets:UIEdgeInsets.zero)
        mapView.camera = camera!;
        
    }
    
    
    func addMarkers(){
        for location in outletLocations{
            let marker = PlaceMarker(place: CLLocationCoordinate2D(latitude:location.coordinate.latitude , longitude: location.coordinate.longitude), outlets:location)
            marker.title = location.title!
            marker.snippet = location.subtitle!
            marker.map = self.mapView
        }
        let update = GMSCameraUpdate.setTarget(outletLocations.last!.coordinate, zoom: 7)
        mapView.moveCamera(update)
    }
    
    
    
    
    
    
    
    
    
    
    
    
    func fetchLocations() {
        print(Utilities.shared.isNetworkReachable())
        guard Utilities.shared.isNetworkReachable() else {
            self.showNoInternetMessage()
            return
        }
        
        Utilities.showHUD(forView: self.view, excludeViews: [])
        let outletLocationsObserver = ApiManager.shared.apiService.getOutletLocations()
        let outletLocationDisposable = outletLocationsObserver.subscribe(onNext: {(outletLocations) in
            self.outletLocations = outletLocations
            DispatchQueue.main.async(execute: {
                Utilities.hideHUD(forView: self.view)
                self.setUpMap()
            })
        }, onError: {(error) in
            DispatchQueue.main.async(execute: {
                Utilities.hideHUD(forView: self.view)
            })
        })
        outletLocationDisposable.disposed(by: disposableBag)
    }
    
}


    




extension MapViewController:GMSMapViewDelegate{
//    func mapView(_ mapView: GMSMapView, markerInfoContents marker: GMSMarker) -> UIView? {
//        guard let placeMarker = marker as? PlaceMarker else {
//            return nil
//        }
//
//        guard let infoView = UIView.viewFromNibName("MarkerInfoView") as? MarkerInfoView else {
//            return nil
//        }
//
//       // infoView.lblTitle.text = placeMarker.outlet.title
//       // infoView.lblSubtitile.text = placeMarker.outlet.subtitle
//        infoView.btnInfoView.tappedInfo = placeMarker.outlet
//        infoView.btnMapview.tappedInfo = placeMarker.outlet
//        infoView.btnInfoView.addTarget(self, action: #selector(onTapLeftAccessory(outlet:)), for: .touchUpInside)
//        infoView.btnMapview.addTarget(self, action: #selector(onTapRightAccessory(outlet:)), for: .touchUpInside)
//
//        return infoView
//    }
    
    
    
    @objc func onTapLeftAccessory(outlet:InfoButton){
        print("left tapped..")
        if outletLocations.count == 1 {
            self.navigationController?.popViewController(animated: true)
        } else {
            let location = outlet.tappedInfo
            let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let detailController = mainStoryboard.instantiateViewController(withIdentifier: ScreenID.OutletDetailsViewController.rawValue) as! OutletDetailsViewController
            detailController.outletID = String(location!.outletID)
            detailController.outletName = location!.title
            self.tabBarController?.tabBar.isHidden = true
            self.navigationController?.pushViewController(detailController, animated: true)
        }
    }
    @objc  func onTapRightAccessory(outlet:InfoButton){
        print("right tapped..")
        //let location = view.annotation as! OutletLocation
        //let launchOptions = [MKLaunchOptionsDirectionsModeKey: MKLaunchOptionsDirectionsModeDriving]
       // location.mapItem().openInMaps(launchOptions: launchOptions)
        let addr = """
        \(outlet.tappedInfo.title!)
        \(outlet.tappedInfo.subtitle!)
        """

        let urlStr = "comgooglemaps://?q=\(addr)&center=\(outlet.tappedInfo.coordinate.latitude),\(outlet.tappedInfo.coordinate.longitude)&zoom=15&views=transit"
        if UIApplication.shared.canOpenURL(URL(string: urlStr)!){
            UIApplication.shared.canOpenURL(URL(string: urlStr)!)
        }
    }
    
    
    func mapView(_ mapView: GMSMapView, didTapInfoWindowOf marker: GMSMarker) {
        print("tapped in")
        if outletLocations.count == 1 {
            self.navigationController?.popViewController(animated: true)
        } else {
            let pleaceMarker  = marker as! PlaceMarker
            let location = pleaceMarker.outlet
            let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let detailController = mainStoryboard.instantiateViewController(withIdentifier: ScreenID.OutletDetailsViewController.rawValue) as! OutletDetailsViewController
            detailController.outletID = String(location!.outletID)
            detailController.outletName = location!.title
            self.tabBarController?.tabBar.isHidden = true
            self.navigationController?.pushViewController(detailController, animated: true)
        }
    }
    
//    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
////        guard let placeMarker = marker as? PlaceMarker else {
////            return false
////        }
////
////       // infoView?.removeFromSuperview()
////        //infoView = nil
////        if infoView == nil{
////            infoView = UIView.viewFromNibName("MarkerInfoView") as? MarkerInfoView
////        }
////        infoView?.lblTitle.text = placeMarker.outlet.title
////        infoView?.lblSubtitile.text = placeMarker.outlet.subtitle
////        infoView?.btnInfoView.tappedInfo = placeMarker.outlet
////        infoView?.btnMapview.tappedInfo = placeMarker.outlet
////        infoView?.btnInfoView.addTarget(self, action: #selector(onTapLeftAccessory(outlet:)), for: .touchUpInside)
////        infoView?.btnMapview.addTarget(self, action: #selector(onTapRightAccessory(outlet:)), for: .touchUpInside)
////
////        infoView?.center = mapView.projection.point(for: marker.position)
////        infoView?.center.y = (infoView?.center.y)! - 40
////        self.view.addSubview(infoView!)
//        return true
//    }
    
    func mapView(_ mapView: GMSMapView, didChange position: GMSCameraPosition) {
        if infoView != nil{
            infoView!.center = mapView.projection.point(for:position.target)
            infoView!.center.y = (infoView?.center.y)! - 40
        }
        
    }
    
    func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D) {
//        infoView!.removeFromSuperview()
//        infoView = nil
    }
}


extension MapViewController: CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .denied {
            mapView.isMyLocationEnabled = false
            mapView.settings.myLocationButton = false
        } else if status == .authorizedWhenInUse {
            mapView.isMyLocationEnabled = true
            mapView.settings.myLocationButton = true
        }
    }
    
}




//class MapViewController: BaseViewController {
//
//    @IBOutlet weak var mapView: MKMapView!
//    @IBOutlet weak var backButton: UIButton!
//    @IBOutlet weak var titleLabel: UILabel!
//    @IBOutlet weak var logo: UIImageView!
//
//    var shouldShowTabBar = true
//    let locationManager = CLLocationManager()
//    var outletLocations: [OutletLocation]!
//    let regionRadius: CLLocationDistance = 10000
//    var disposableBag = DisposeBag()
//
//    override func viewDidLoad() {
//        super.viewDidLoad()
//        mapView.delegate = self
//        locationManager.delegate = self
//
//        if let locations = self.outletLocations, locations.count > 0 {
//            //setUpMap()
//        } else {
//            fetchLocations()
//        }
//        if #available(iOS 11.0, *) {
        //    mapView.register(OutletMapView.self,
//                             forAnnotationViewWithReuseIdentifier: MKMapViewDefaultAnnotationViewReuseIdentifier)
//        } else {
//            // Fallback on earlier versions
//        }
//
//    }
//
//    override func viewWillAppear(_ animated: Bool) {
//        super.viewWillAppear(animated)
//        backButton.isHidden = shouldShowTabBar
//        self.tabBarController?.tabBar.isHidden = !shouldShowTabBar
//        self.titleLabel.isHidden = shouldShowTabBar
//        self.logo.isHidden = !shouldShowTabBar
//    }
//
//    override func viewDidAppear(_ animated: Bool) {
//        super.viewDidAppear(animated)
//        checkLocationAuthorizationStatus()
//    }
//
//    override func didReceiveMemoryWarning() {
//        super.didReceiveMemoryWarning()
//        // Dispose of any resources that can be recreated.
//    }
//
//    func checkLocationAuthorizationStatus() {
//        if CLLocationManager.authorizationStatus() == .authorizedWhenInUse {
//            mapView.showsUserLocation = true
//        } else if CLLocationManager.authorizationStatus() == .denied {
//            let alertview = JSSAlertView().show(
//                self,
//                title: "Location Permission".localized,
//                text: "Please grant location permission to use this feature.".localized,
//                buttonText: "OK".localized,
//                cancelButtonText: "Later".localized,
//                color: UIColor.white)
//            alertview.addAction(navigateToSettingsPage)
//        } else {
//            locationManager.requestWhenInUseAuthorization()
//        }
//    }
//
//    func setUpMap() {
//
//        let userLocation = mapView.userLocation
//
//        if let userLoc = userLocation.location {
//            let region = MKCoordinateRegionMakeWithDistance(
//                userLoc.coordinate, regionRadius, regionRadius)
//            mapView.setRegion(region, animated: true)
//        }
//
//        mapView.addAnnotations(outletLocations)
//    }
//
//    func fetchLocations() {
//        print(Utilities.shared.isNetworkReachable())
//        guard Utilities.shared.isNetworkReachable() else {
//            self.showNoInternetMessage()
//            return
//        }
//
//        Utilities.showHUD(forView: self.view, excludeViews: [])
//        let outletLocationsObserver = ApiManager.shared.apiService.getOutletLocations()
//        let outletLocationDisposable = outletLocationsObserver.subscribe(onNext: {(outletLocations) in
//            self.outletLocations = outletLocations
//            DispatchQueue.main.async(execute: {
//                Utilities.hideHUD(forView: self.view)
//                self.setUpMap()
//            })
//        }, onError: {(error) in
//            DispatchQueue.main.async(execute: {
//                Utilities.hideHUD(forView: self.view)
//            })
//        })
//        outletLocationDisposable.disposed(by: disposableBag)
//    }
//
//}
//
//extension MapViewController: MKMapViewDelegate {
//
//    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
////        guard let _ = view.annotation?.title else {
////            return
////        }
////        let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
////        let detailController = mainStoryboard.instantiateViewController(withIdentifier: ScreenID.OutletDetailsViewController.rawValue) as! OutletDetailsViewController
////        var locations = [OutletLocation]()
////        if let outletLoc = outletLocations {
////            locations = outletLoc
////        }
////        if locations.count == 1 {
////            self.navigationController?.popViewController(animated: true)
////        } else {
////            let outlet = locations.first { element in
////                return element.title == view.annotation?.title!
////            }
////            if let outletLocation = outlet {
////                detailController.outletID = String(outletLocation.outletID)
////                detailController.outletName = outletLocation.title
////                self.tabBarController?.tabBar.isHidden = true
////                self.navigationController?.pushViewController(detailController, animated: true)
////            }
////        }
//    }
//
//    func mapView(_ mapView: MKMapView, annotationView view: MKAnnotationView, calloutAccessoryControlTapped control: UIControl) {
//        if (control == view.leftCalloutAccessoryView) {
//            if outletLocations.count == 1 {
//                self.navigationController?.popViewController(animated: true)
//            } else {
//                let location = view.annotation as! OutletLocation
//                let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
//                let detailController = mainStoryboard.instantiateViewController(withIdentifier: ScreenID.OutletDetailsViewController.rawValue) as! OutletDetailsViewController
//                detailController.outletID = String(location.outletID)
//                detailController.outletName = location.title
//                self.tabBarController?.tabBar.isHidden = true
//                self.navigationController?.pushViewController(detailController, animated: true)
//            }
//        } else if (control == view.rightCalloutAccessoryView) {
//            let location = view.annotation as! OutletLocation
//            let launchOptions = [MKLaunchOptionsDirectionsModeKey: MKLaunchOptionsDirectionsModeDriving]
//            location.mapItem().openInMaps(launchOptions: launchOptions)
//        }
//
//    }
//
//
////    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
////
////        if annotation.isMember(of: MKUserLocation.self) {
////            return nil
////        }
////
////        let reuseId = "MKAnnotationView"
////
////        var pinView = mapView.dequeueReusableAnnotationView(withIdentifier: reuseId)
////        if pinView == nil {
////            pinView = MKAnnotationView(annotation: annotation, reuseIdentifier: reuseId)}
////        pinView!.canShowCallout = true
////        pinView!.image = UIImage(named: "logo")
////        pinView!.frame.size = CGSize(width: 40.0, height: 40.0)
////        return pinView
////    }
//}
//
//extension MapViewController: CLLocationManagerDelegate {
//
//    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
//        if status == .denied {
//            mapView.showsUserLocation = false
//        } else if status == .authorizedWhenInUse {
//            mapView.showsUserLocation = true
//        }
//    }
//
//}
//





