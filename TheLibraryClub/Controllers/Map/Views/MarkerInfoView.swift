//
//  MarkerInfoView.swift
//  TheLibraryClub
//
//  Created by Namespace  on 16/07/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import UIKit

class MarkerInfoView: UIView {

    @IBOutlet weak var btnInfoView: InfoButton!
    @IBOutlet weak var btnMapview: InfoButton!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblSubtitile: UILabel!
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    @IBAction func btnOnTapRightAccessory(_ sender: UIButton) {
    }
    
    
    @IBAction func btnOnTapLeftAccessory(_ sender: UIButton) {
        
    }
    
}

class InfoButton: UIButton {
    var tappedInfo:OutletLocation!
}
