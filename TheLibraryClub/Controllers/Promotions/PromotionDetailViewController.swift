//
//  PromotionDetailViewController.swift
//  TheLibraryClub
//
//  Created by Phaninder on 21/01/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import UIKit
import RxSwift

class PromotionDetailViewController: BaseViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var favoriteButton: UIButton!
    
    
    var promotionID: String!
    var promotionName: String!
    var promotionDescription: PromotionDescription!
    var outletsCellHeight:CGFloat = 0.0
    var disposableBag = DisposeBag()
    var isBookingAllowed = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        titleLabel.text = promotionName
        
        tableView.register(UINib.init(nibName: PromotionDetailHeaderTableViewCell.cellIdentifier(), bundle: nil), forCellReuseIdentifier: PromotionDetailHeaderTableViewCell.cellIdentifier())

        tableView.register(UINib.init(nibName: DescriptionTableViewCell.cellIdentifier(), bundle: nil), forCellReuseIdentifier: DescriptionTableViewCell.cellIdentifier())

        tableView.register(UINib.init(nibName: VerticalOutletsTableViewCell.cellIdentifier(), bundle: nil), forCellReuseIdentifier: VerticalOutletsTableViewCell.cellIdentifier())
        
        tableView.reloadData()
        fetchPromotionDetails()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func setFavoriteIcon() {
        let imageName = self.promotionDescription.isFavorite ? "heart" : "heart_empty"
        self.favoriteButton.setImage(UIImage(named: imageName), for: .normal)
    }
    

    func fetchPromotionDetails() {
        guard Utilities.shared.isNetworkReachable() else {
            self.showNoInternetMessage()
            return
        }
        
        Utilities.showHUD(forView: self.view, excludeViews: [])
        let promotionObserver = ApiManager.shared.apiService.getPromotionDetailsWithID(promotionID)
        let promotionDisposable = promotionObserver.subscribe(onNext: {(promotion) in
            self.promotionDescription = promotion
            DispatchQueue.main.async(execute: {
                self.titleLabel.text = self.promotionDescription.name
                Utilities.hideHUD(forView: self.view)
                self.setFavoriteIcon()
                for outlet in self.promotionDescription.outlets {
                    if outlet.allowBooking == true {
                        self.isBookingAllowed = true
                    }
                }
                self.tableView.reloadData()
            })
        }, onError: {(error) in
            DispatchQueue.main.async(execute: {
                Utilities.hideHUD(forView: self.view)
            })
        })
        promotionDisposable.disposed(by: disposableBag)
    }
    
    func favoritePromotion() {
        guard Utilities.shared.isNetworkReachable() else {
            self.showNoInternetMessage()
            return
        }
        
        Utilities.showHUD(forView: self.view, excludeViews: [])
        let favoriteObserver = ApiManager.shared.apiService.favoritePromotion(withId: promotionID, userId: UserStore.shared.userId)
        let favoriteDisposable = favoriteObserver.subscribe(onNext: {(message) in
            self.promotionDescription.isFavorite = true
            DispatchQueue.main.async(execute: {
                Utilities.hideHUD(forView: self.view)
                //Utilities.showToast(withString: message)
                NotificationCenter.default.post(name: .promoFavorited, object: nil, userInfo:nil)
                self.setFavoriteIcon()
            })
        }, onError: {(error) in
            DispatchQueue.main.async(execute: {
                Utilities.hideHUD(forView: self.view)
            })
        })
        favoriteDisposable.disposed(by: disposableBag)
    }
    
    
    func unFavoritePromotion() {
        guard Utilities.shared.isNetworkReachable() else {
            self.showNoInternetMessage()
            return
        }
        
        Utilities.showHUD(forView: self.view, excludeViews: [])
        let unFavoriteObserver = ApiManager.shared.apiService.unFavoritePromotion(withId: promotionID)
        let unFavoriteDisposable = unFavoriteObserver.subscribe(onNext: {(message) in
            self.promotionDescription.isFavorite = false
            DispatchQueue.main.async(execute: {
                Utilities.hideHUD(forView: self.view)
              //  Utilities.showToast(withString: message)
                let userInfo: [String: Int] = ["promoID": Int(self.promotionID)!];
                NotificationCenter.default.post(name: .promoUnfavorited, object: nil, userInfo:userInfo)

                self.setFavoriteIcon()
            })
        }, onError: {(error) in
            DispatchQueue.main.async(execute: {
                Utilities.hideHUD(forView: self.view)
            })
        })
        unFavoriteDisposable.disposed(by: disposableBag)
    }

    func showOutletDetailsForOutlet(_ outlet: Outlet) {
        let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let outletDetailViewController = mainStoryboard.instantiateViewController(withIdentifier: ScreenID.OutletDetailsViewController.rawValue) as! OutletDetailsViewController
        outletDetailViewController.outletID = String(outlet.id)
        outletDetailViewController.outletName = outlet.name
        self.navigationController?.pushViewController(outletDetailViewController, animated: true)
    }

    @IBAction func favoriteButtonTapped(_ sender: UIButton) {
        if promotionDescription.isFavorite == true {
            unFavoritePromotion()
        } else {
            favoritePromotion()
        }
    }
}


extension PromotionDetailViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let _ = promotionDescription {
            return 3
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.row {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: PromotionDetailHeaderTableViewCell.cellIdentifier(), for: indexPath) as! PromotionDetailHeaderTableViewCell
            cell.delegate = self
            cell.bookButton.isHidden = !self.isBookingAllowed
            cell.configureCellWithPromo(promotionDescription)
            return cell
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: DescriptionTableViewCell.cellIdentifier(), for: indexPath) as! DescriptionTableViewCell
            cell.descriptionLabel.text = promotionDescription.promoInfo
            return cell
        case 2:
            let cell = tableView.dequeueReusableCell(withIdentifier: VerticalOutletsTableViewCell.cellIdentifier(), for: indexPath) as! VerticalOutletsTableViewCell
            outletsCellHeight = cell.configureCellWithOutlets(promotionDescription.outlets)
            cell.delegate = self
            return cell
        default:
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.row {
        case 0:
            return 145.0
        case 1:
            tableView.estimatedRowHeight = 120.0
            return UITableViewAutomaticDimension
        case 2:
            return outletsCellHeight
        default:
            return 0
        }
    }

}

extension PromotionDetailViewController: VerticalOutletsTableViewCellDelegate {
    
    func outletOneButtonTapped() {
        showOutletDetailsForOutlet(promotionDescription.outlets[0])
    }
    
    func outletTwoButtonTapped() {
        showOutletDetailsForOutlet(promotionDescription.outlets[1])
    }
    
    func viewMoreButtonTapped() {
        let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let outletListingViewController = mainStoryboard.instantiateViewController(withIdentifier: ScreenID.OutletListingViewController.rawValue) as! OutletListingViewController
        outletListingViewController.shouldShowTabBar = false
        outletListingViewController.promotionID = promotionID
        self.navigationController?.pushViewController(outletListingViewController, animated: true)
    }
    
}

extension PromotionDetailViewController: PromotionDetailHeaderTableViewCellDelegate {
    
    func showTheBookingScreen() {
        let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let bookingViewController = mainStoryboard.instantiateViewController(withIdentifier: ScreenID.BookingViewController.rawValue) as! BookingViewController
        let locations = getOutletLocations()
        
        bookingViewController.outlets = locations
        
        if locations.count == 1 {
            let outletLocation = locations[0]
            bookingViewController.outletName = outletLocation.title ?? ""
            bookingViewController.outletID = String(outletLocation.outletID)
        }
        self.navigationController?.pushViewController(bookingViewController, animated: true)
    }
    
    func getOutletLocations() -> [OutletLocation] {
        var outletLocations = [OutletLocation]()
        for outlet in promotionDescription.outlets {
            let location = OutletLocation(outlet: outlet)
            outletLocations.append(location)
        }
        return outletLocations
    }
    
}
