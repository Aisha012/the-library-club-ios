//
//  PromotionListingViewController.swift
//  TheLibraryClub
//
//  Created by Phaninder on 18/01/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import UIKit
import RxSwift
import CoreLocation
import JSSAlertView

class PromotionListingViewController: BaseViewController {

    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var collectionViewTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var searchView: UIView!
    @IBOutlet weak var searchTextField: UITextField!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var logo: UIImageView!
    @IBOutlet weak var emptyMessageLabel: UILabel!
    
    private let refreshControl = UIRefreshControl()
    
    
    var shouldShowTabBar = true
    var showTextField = false
    var currentSearchText = ""
    var isFetchingData = false
    var disposableBag = DisposeBag()
    var promotionPagination = PromotionPagination.init();
    var isSearchEnabled = false
    var isFirstTime = true
    var promotionObserver: Observable<PromotionPagination>!
    var userLocation: CLLocation?
    let locationManager = CLLocationManager()
    var isUserLocationReceived = false
    var isFavoriteSelected = false
    var alreadyAPICalled = false
    var ignoreUserLocation = false
    var outletID = ""
    var isFromDashboard:Bool = false
    override func viewDidLoad() {
        super.viewDidLoad()
        locationManager.delegate = self
        checkLocationAuthorizationStatus()
       
        if #available(iOS 10.0, *) {
            collectionView.refreshControl = refreshControl
        } else {
            collectionView.addSubview(refreshControl)
        }
        refreshControl.addTarget(self, action: #selector(refreshData(_:)), for: .valueChanged)
        collectionView.register(UINib.init(nibName: PromotionCollectionViewCell.cellIdentifier(), bundle: nil), forCellWithReuseIdentifier: PromotionCollectionViewCell.cellIdentifier())
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(deletePromo(_:)), name: .promoUnfavorited,
                                               object: nil)

        collectionView.reloadData()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        backButton.isHidden = shouldShowTabBar
        self.tabBarController?.tabBar.isHidden = !shouldShowTabBar
        self.titleLabel.isHidden = shouldShowTabBar
        self.logo.isHidden = !shouldShowTabBar
        
        if isFromDashboard == true{
            self.logo.isHidden = true
            self.titleLabel.isHidden = false
            self.titleLabel.text = "Promotions"
            self.backButton.isHidden = false
            self.tabBarController?.tabBar.isHidden = true

        }

    }

    override func viewDidAppear(_ animated: Bool) {
        self.navigationController?.tabBarController?.tabBar.isHidden = false
      }

    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    deinit {
        NotificationCenter.default.removeObserver(self, name: .promoUnfavorited, object: nil)
    }

    @objc func refreshData(_ sender: Any) {
        self.promotionPagination.paginationType = .reload
        checkLocationAuthorizationStatus()
        if let refreshControl = sender as? UIRefreshControl {
            refreshControl.endRefreshing()
        }
    }
    
    @objc func deletePromo(_ notification: NSNotification) {
        if let promoID = notification.userInfo?["promoID"] as? Int,
            isFavoriteSelected == true {
            let index = self.promotionPagination.promotions.index(where: { (promo) -> Bool in
                return promo.id == promoID
            })
            if let deleteIndex = index {
                self.promotionPagination.promotions.remove(at: deleteIndex)
                DispatchQueue.main.async(execute: {
                    self.collectionView.reloadData()
                })
            }
        }
    }

    func getLocationCoordinates() -> (lat: String, long: String) {
        
        guard ignoreUserLocation == false else {
            return (lat: "", long: "")
        }
        
        guard let location = userLocation else {
            return (lat: "", long: "")
        }
        return (lat: String(location.coordinate.latitude), long: String(location.coordinate.longitude))
    }
    
    func clearFilters() {
        self.isFavoriteSelected = false
        self.ignoreUserLocation = false
        checkLocationAuthorizationStatus()
    }
    
    func fetchPromotions() {
        guard Utilities.shared.isNetworkReachable() else {
            self.showNoInternetMessage()
            return
        }
        
        if promotionPagination.paginationType != .old {
            self.promotionPagination.currentPageNumber = 1
            if isFirstTime == true {
                Utilities.showHUD(forView: self.view, excludeViews: [])
            } else {
                UIApplication.shared.isNetworkActivityIndicatorVisible = true
            }
        } else {
            UIApplication.shared.isNetworkActivityIndicatorVisible = true
        }
        
        isFetchingData = true
        let location = getLocationCoordinates()
        currentSearchText = searchTextField.text ?? ""
        promotionObserver = ApiManager.shared.apiService.fetchPromotions(pageNumber: String(promotionPagination.currentPageNumber), searchString: currentSearchText, lat: location.lat, long: location.long, favorite: isFavoriteSelected, outletId: outletID, date: nil)
        let promotionDisposable = promotionObserver.subscribe(onNext: {(paginationObject) in
            self.promotionPagination.appendDataFromObject(paginationObject)
            self.isFetchingData = false
            DispatchQueue.main.async(execute: {
                Utilities.hideHUD(forView: self.view)
                
                if self.promotionPagination.paginationType != .old && self.isFirstTime == true {
                    self.isFirstTime = false
                } else {
                    UIApplication.shared.isNetworkActivityIndicatorVisible = false
                }
                if self.promotionPagination.promotions.count == 0 {
                    self.emptyMessageLabel.isHidden = false
//                    self.collectionView.isHidden = true
                } else {
                    self.emptyMessageLabel.isHidden = true
//                    self.collectionView.isHidden = false
                }
                self.collectionView.reloadData()
            })
        }, onError: {(error) in
            self.isFetchingData = false
            DispatchQueue.main.async(execute: {
                Utilities.hideHUD(forView: self.view)
            })
        })
        promotionDisposable.disposed(by: disposableBag)
    }
    
    func checkLocationAuthorizationStatus() {
        if CLLocationManager.authorizationStatus() == .authorizedWhenInUse {
            locationManager.desiredAccuracy=kCLLocationAccuracyBest
            locationManager.distanceFilter=kCLDistanceFilterNone
            
            locationManager.startUpdatingLocation()
            if isUserLocationReceived == true {
                fetchDataAppropiately()
                alreadyAPICalled = true
            }
        } else if CLLocationManager.authorizationStatus() == .denied {
            if Utilities.hasTwoDaysPassed() {
                let alertview = JSSAlertView().show(
                    self,
                    title: "Location Permission".localized,
                    text: "Please grant location permission to use this feature.".localized,
                    buttonText: "OK".localized,
                    cancelButtonText: "Later".localized,
                    color: UIColor.white)
                UserStore.shared.locationPreviouslyShown = Int(Date().timeIntervalSince1970)
                alertview.addAction(navigateToSettingsPage)
                alertview.addCancelAction(fetchDataAppropiately)
            }
        } else {
            UserStore.shared.locationPreviouslyShown = Int(Date().timeIntervalSince1970)
            locationManager.requestWhenInUseAuthorization()
        }
    }
    
    func fetchDataAppropiately() {
        fetchPromotions()
    }
    
    @IBAction func searchButtonTapped(_ sender: UIButton) {
        showTextField = !showTextField
        animateTextField()

    }
    
    func animateTextField() {
        if showTextField {
            searchTextField.becomeFirstResponder()
        } else {
            searchTextField.resignFirstResponder()
        }
        self.view.layoutIfNeeded()
        UIView.animate(withDuration: 0.7, delay: 0, usingSpringWithDamping: 0.5, initialSpringVelocity: 1, options: .curveEaseIn, animations: {
            self.collectionViewTopConstraint.constant = self.showTextField ? 50 : 0
            self.searchView.isHidden = !self.showTextField
            self.view.layoutIfNeeded()
        }) { (completed) in
            
        }
    }
    
    @IBAction func valueChangedInSearchField(_ sender: Any) {
        if (promotionObserver != nil && currentSearchText != searchTextField.text) {
            promotionObserver = nil
        }
        promotionPagination.paginationType = .new
        fetchPromotions()
    }
    
    @IBAction func filterButtonTapped(_ sender: UIButton) {
        self.tabBarController?.tabBar.isHidden = true
        let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let filterViewController = mainStoryboard.instantiateViewController(withIdentifier: ScreenID.FilterViewController.rawValue) as! FilterViewController
        filterViewController.isFavoriteChecked = isFavoriteSelected
        filterViewController.isLocationActive = !ignoreUserLocation
        filterViewController.delegate = self
        self.navigationController?.pushViewController(filterViewController, animated: true)
    }
    
}


extension PromotionListingViewController: UITextFieldDelegate {
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if isFetchingData == false {
            promotionPagination.paginationType = .new
            fetchPromotions()
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if (textField.text?.isEmpty)! && string == " " {
            return false
        }
        let char = string.cString(using: String.Encoding.utf8)!
        let isBackSpace = strcmp(char, "\\b")
        if (isBackSpace == -92) {
            return true
        }
        return true
    }
}

extension PromotionListingViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return promotionPagination.promotions.count
    }

    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: PromotionCollectionViewCell.cellIdentifier(), for: indexPath) as! PromotionCollectionViewCell
        cell.configureCellWithPromotion(promotionPagination.promotions[indexPath.item], atIndex: indexPath.item)
        cell.delegate = self
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if indexPath.item == self.promotionPagination.promotions.count - 1 && self.promotionPagination.hasMoreToLoad && !isFetchingData {
            fetchPromotions()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let promo = promotionPagination.promotions[indexPath.row]
        self.tabBarController?.tabBar.isHidden = true
        let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let promoDetailViewController = mainStoryboard.instantiateViewController(withIdentifier: ScreenID.PromotionDetailViewController.rawValue) as! PromotionDetailViewController
        
        promoDetailViewController.promotionID = String(promo.id)
        promoDetailViewController.promotionName = promo.name
        self.navigationController?.pushViewController(promoDetailViewController, animated: true)

    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize.init(width: (ScreenWidth - 48) / 2, height: 216)
    }


    
}

extension PromotionListingViewController: CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        let location = locations.last!
        if location.horizontalAccuracy > 0 {
            isUserLocationReceived = true
            userLocation = location
            locationManager.stopUpdatingLocation()
        }
        if alreadyAPICalled == false {
            checkLocationAuthorizationStatus()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .denied {
            userLocation = nil
            self.fetchDataAppropiately()
        } else if status == .authorizedWhenInUse {
            checkLocationAuthorizationStatus()
        }
    }
}

extension PromotionListingViewController: PromotionCollectionViewCellDelegate {
    
    func viewButtonTappedAtIndex(_ index: Int) {
        let promo = promotionPagination.promotions[index]
        self.tabBarController?.tabBar.isHidden = true
        let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let promoDetailViewController = mainStoryboard.instantiateViewController(withIdentifier: ScreenID.PromotionDetailViewController.rawValue) as! PromotionDetailViewController

        promoDetailViewController.promotionID = String(promo.id)
        promoDetailViewController.promotionName = promo.name
        self.navigationController?.pushViewController(promoDetailViewController, animated: true)
    }
}

extension PromotionListingViewController: FilterViewControllerDelegate {
    
    func filtersUpdated(isFavoriteChecked: Bool, isLocationChecked: Bool) {
        self.isFavoriteSelected = isFavoriteChecked
        self.ignoreUserLocation = !isLocationChecked
        self.promotionPagination.paginationType = .new
        self.fetchPromotions()

    }
}
