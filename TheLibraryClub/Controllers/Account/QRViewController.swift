//
//  QRViewController.swift
//  TheLibraryClub
//
//  Created by Kalpana_Hipster on 03/11/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import UIKit
import RxSwift

class QRViewController: BaseViewController {
    
        @IBOutlet weak var imgQRCode: UIImageView!
    
        
        var qrcodeImage: CIImage!
        
        
        override func viewDidLoad() {
            super.viewDidLoad()
            // Do any additional setup after loading the view, typically from a nib.
            createQRCode()
        }
        
        override func didReceiveMemoryWarning() {
            super.didReceiveMemoryWarning()
            // Dispose of any resources that can be recreated.
        }
        
        
        // MARK: IBAction method implementation
        
    func createQRCode(){
            if qrcodeImage == nil {
                let data = UserStore.shared.userId.data(using: String.Encoding.isoLatin1, allowLossyConversion: false)
                
                let filter = CIFilter(name: "CIQRCodeGenerator")
                
                filter?.setValue(data, forKey: "inputMessage")
                filter?.setValue("Q", forKey: "inputCorrectionLevel")
                
                qrcodeImage = filter?.outputImage
                
                displayQRCodeImage()
            }
            else {
                imgQRCode.image = nil
                qrcodeImage = nil
            }
    }
        
        
//        @IBAction func changeImageViewScale(sender: AnyObject) {
//            imgQRCode.transform = CGAffineTransform(scaleX: CGFloat(slider.value), y: CGFloat(slider.value))
//        }
    
        
        // MARK: Custom method implementation
        
        func displayQRCodeImage() {
            let scaleX = imgQRCode.frame.size.width / qrcodeImage.extent.size.width
            let scaleY = imgQRCode.frame.size.height / qrcodeImage.extent.size.height
            
            let transformedImage = qrcodeImage.transformed(by: CGAffineTransform(scaleX: scaleX, y: scaleY))
            
            imgQRCode.image = UIImage(ciImage: transformedImage)
        }

}
