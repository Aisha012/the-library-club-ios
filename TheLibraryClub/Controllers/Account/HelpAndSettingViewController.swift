//
//  HelpAndSettingViewController.swift
//  TheLibraryClub
//
//  Created by Phaninder on 22/01/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import UIKit
import SafariServices
import RxSwift

class HelpAndSettingViewController: BaseViewController {

    @IBOutlet weak var tableView: UITableView!
    
    let helpAndSettingKeys = ["Edit Profile", "Change Password", "FAQs", "Contact Us", "Terms of Service", "Privacy Policy", "Follow us on Facebook", "Language", "Version", "Sign Out"]
    var disposableBag = DisposeBag()

    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.tableFooterView = UIView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func pushToWebViewWithURL(_ url: String) {
        if let url = URL(string: url) {
            if #available(iOS 11.0, *) {
                let config: SFSafariViewController.Configuration = SFSafariViewController.Configuration.init()
                config.entersReaderIfAvailable = true
                let vc = SFSafariViewController(url: url, configuration: config)
                present(vc, animated: true, completion: nil)
            } else {
                let safariController = SFSafariViewController(url: url)
                self.present(safariController, animated: true, completion: nil)
            }
        }
    }
    
    func logoutUser() {
        guard Utilities.shared.isNetworkReachable() else {
            self.showNoInternetMessage()
            return
        }

        Utilities.showHUD(forView: self.view, excludeViews: [])

        let logoutObserver = ApiManager.shared.apiService.logoutUser()
        let logoutDisposable = logoutObserver.subscribe(onNext: {(message) in
            DispatchQueue.main.async(execute: {

                Utilities.hideHUD(forView: self.view)
                Utilities.logoutUser()
                let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let initialViewController = mainStoryboard.instantiateViewController(withIdentifier: ScreenID.LoginViewController.rawValue) as! LoginViewController
                let navController = BaseNavigationController(rootViewController: initialViewController)
                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                
                appDelegate.window = UIWindow(frame: UIScreen.main.bounds)
                appDelegate.window?.rootViewController = navController
                let statusBarView = UIView.init(frame: CGRect(x: 0, y: 0, width: ScreenWidth, height: 20))
                statusBarView.backgroundColor = UIColor.black
                appDelegate.window?.rootViewController?.view.addSubview(statusBarView)
                
                appDelegate.window?.makeKeyAndVisible()

                UIApplication.shared.isNetworkActivityIndicatorVisible = false
            })
        }, onError: {(error) in
            DispatchQueue.main.async(execute: {
                Utilities.hideHUD(forView: self.view)
            })
        })
        logoutDisposable.disposed(by: disposableBag)
    }

}

extension HelpAndSettingViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return helpAndSettingKeys.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: AccountTableViewCell.cellIdentifier(), for: indexPath) as! AccountTableViewCell
        cell.preservesSuperviewLayoutMargins = false
        cell.separatorInset = UIEdgeInsets.zero
        cell.layoutMargins = UIEdgeInsets.zero
        var value = ""
        var cellType: AccountInfoTableViewCellType = .tappable
        if indexPath.row == helpAndSettingKeys.count - 2 {
            if let version = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String {
                value = version
            }
            cellType = .nonTappable
        }
        cell.configureCellWithTitle(helpAndSettingKeys[indexPath.row], info: value, cellType: cellType)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.row {
        case HelpTableDataType.edit.rawValue:
            let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let editProfileViewController = mainStoryboard.instantiateViewController(withIdentifier: ScreenID.EditProfileViewController.rawValue) as! EditProfileViewController
            self.navigationController?.pushViewController(editProfileViewController, animated: true)
        case HelpTableDataType.changePass.rawValue:
            let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let changePasswordViewController = mainStoryboard.instantiateViewController(withIdentifier: ScreenID.ChangePasswordViewController.rawValue) as! ChangePasswordViewController
            self.navigationController?.pushViewController(changePasswordViewController, animated: true)
        case HelpTableDataType.faq.rawValue:
            pushToWebViewWithURL(faqURL)
        case HelpTableDataType.contact.rawValue:
            pushToWebViewWithURL(contactUsURL)
        case HelpTableDataType.terms.rawValue:
            pushToWebViewWithURL(termsAndConditionsURL)
        case HelpTableDataType.privacy.rawValue:
            pushToWebViewWithURL(privacyPolicyURL)
        case HelpTableDataType.facebook.rawValue:
            pushToWebViewWithURL(facebookURL)
        case HelpTableDataType.logout.rawValue:
            logoutUser()
        case HelpTableDataType.language.rawValue:
            let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let languageViewController = mainStoryboard.instantiateViewController(withIdentifier: ScreenID.LanguageSettingsViewController.rawValue) as! LanguageSettingsViewController
            self.navigationController?.pushViewController(languageViewController, animated: true)
        default:
            print("Do nothing")
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        if indexPath.row == HelpTableDataType.language.rawValue {
//            return 0.0
//        }
        return 50.0
    }
}

