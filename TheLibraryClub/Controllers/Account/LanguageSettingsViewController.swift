//
//  LanguageSettingsViewController.swift
//  TheLibraryClub
//
//  Created by Phaninder on 31/01/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import UIKit

class LanguageSettingsViewController: BaseViewController {
    
    @IBOutlet weak var englishCheckBox: UIImageView!
    @IBOutlet weak var thaiCheckBox: UIImageView!
    
    var isEnglishSelected = false
    var isThaiSelected = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        isEnglishSelected = UserStore.shared.userLanguage == "en"
        isThaiSelected = UserStore.shared.userLanguage != "en"
        
        configureLanuageCheckbox()

    }

    func configureLanuageCheckbox() {
        let englishCheckBoxString = isEnglishSelected ? "checkbox_green" : "checkbox_black"
        let thaiCheckBoxString = isThaiSelected ? "checkbox_green" : "checkbox_black"
        englishCheckBox.image = UIImage(named: englishCheckBoxString)
        thaiCheckBox.image = UIImage(named: thaiCheckBoxString)
    }

    
    @IBAction func englishButtonTapped(_ sender: UIButton) {
        isEnglishSelected = !isEnglishSelected
        isThaiSelected = !isThaiSelected
        configureLanuageCheckbox()
    }
    
    @IBAction func thaiButtonTapped(_ sender: UIButton) {
        isEnglishSelected = !isEnglishSelected
        isThaiSelected = !isThaiSelected

        configureLanuageCheckbox()
    }
    
    @IBAction func applyLanguageButtonTapped(_ sender: UIButton) {
        UserStore.shared.isLanguageSetManually = true
        UserStore.shared.userLanguage = isEnglishSelected ? "en" : "th"
        self.navigationController?.popViewController(animated: true)
    }

    
}
