//
//  BadgeViewController.swift
//  TheLibraryClub
//
//  Created by Phaninder on 08/02/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import UIKit
import RxSwift

class BadgeViewController: BaseViewController {

    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var badgeEmptyPlaceholder: UILabel!
    
    var disposableBag = DisposeBag()
    var badges = [Badge]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if badges.count == 0 {
            fetchBadges()
        }else{
            self.badgeEmptyPlaceholder.isHidden = true  
         collectionView.reloadData()
        }
    }

    func fetchBadges() {
        guard Utilities.shared.isNetworkReachable() else {
            self.showNoInternetMessage()
            return
        }
        
        Utilities.showHUD(forView: self.view, excludeViews: [])
        let badgeObserver = ApiManager.shared.apiService.fetchBadges(UserStore.shared.userId)
        let badgeDisposable = badgeObserver.subscribe(onNext: {(badgeArray) in
            self.badges = badgeArray
            DispatchQueue.main.async(execute: {
                Utilities.hideHUD(forView: self.view)
                self.badgeEmptyPlaceholder.isHidden = !self.badges.isEmpty
                self.collectionView.reloadData()
            })
        }, onError: {(error) in
            DispatchQueue.main.async(execute: {
                Utilities.hideHUD(forView: self.view)
            })
        })
        badgeDisposable.disposed(by: disposableBag)
    }
    
}

extension BadgeViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return badges.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: BadgeCollectionViewCell.cellIdentifier(), for: indexPath) as! BadgeCollectionViewCell
        cell.configureCell(badges[indexPath.row])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize.init(width: (ScreenWidth - 48) / 2, height: 190)
    }

    
}
