//
//  AccountViewController.swift
//  TheLibraryClub
//
//  Created by Phaninder on 18/01/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import UIKit
import RxSwift

class AccountViewController: BaseViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var profileImageView: UIImageView!
//    @IBOutlet weak var ivQR: UIImageView!
    
    var disposableBag = DisposeBag()

    let profileKeys = ["First Name",
                       "Last Name",
                       "Card Number",
                       "Email",
                       "DOB",
                       "Gender",
                       "Badges",
                       "Phone Number",
                       "My QR",
                       "My Bookings",
                       "Help & Settings"]

    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.tableFooterView = UIView()
        emailLabel.text = UserStore.shared.emailId
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(reloadData),
                                               name: .profileUpdated,
                                               object: nil)
        
        profileImageView.layer.masksToBounds=true
        profileImageView.layer.borderWidth=1.5
        profileImageView.layer.borderColor = UIColor.white.cgColor
        profileImageView.layer.cornerRadius = profileImageView.bounds.width/2

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tabBarController?.tabBar.isHidden = false
        getUserProfile()
        self.tableView.reloadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: .profileUpdated, object: nil)
    }
    
    @objc func reloadData() {
        self.tableView.reloadData()
    }
    
    func getUserProfile() {
        guard Utilities.shared.isNetworkReachable() else {
            self.showNoInternetMessage()
            return
        }
        Utilities.showHUD(forView: self.view, excludeViews: [])
        let profileObserver = ApiManager.shared.apiService.fetchCustomerDetails()
        let profileDisposable = profileObserver.subscribe(onNext: {(user) in
            UserStore.shared.emailId = user.email
            UserStore.shared.firstName = user.firstName
            UserStore.shared.lastName = user.lastName
            UserStore.shared.userId = user.ID
            UserStore.shared.profilePic = user.profilePic
            UserStore.shared.DOB = user.dob
            UserStore.shared.gender = user.gender
            UserStore.shared.phoneNumber = user.phonenum
            UserStore.shared.cardNumber = user.cardNum
            DispatchQueue.main.async(execute: {
                Utilities.hideHUD(forView: self.view)
                if let url = URL(string: UserStore.shared.profilePic) {
                    self.profileImageView.sd_setImage(with: url, placeholderImage: UIImage(named: "dummy"))
                }
                self.tableView.reloadData()
//                let qrString:String = user.cardNum != nil ? user.cardNum : user.ID
//                if let img  =  self.generateQRCode(from: qrString){
//                    self.ivQR.image = img
//                }
            })
        }, onError: {(error) in
            DispatchQueue.main.async(execute: {
                Utilities.hideHUD(forView: self.view)
                if let error_ = error as? ResponseError {
                    Utilities.showToast(withString: error_.description())
                } else {
                    Utilities.showToast(withString: error.localizedDescription)
                }
            })
        })
        profileDisposable.disposed(by: disposableBag)
    }

    
    
    func generateQRCode(from string: String) -> UIImage? {
        let data = string.data(using: String.Encoding.ascii)
        
        if let filter = CIFilter(name: "CIQRCodeGenerator") {
            filter.setValue(data, forKey: "inputMessage")
            let transform = CGAffineTransform(scaleX: 3, y: 3)
            
            if let output = filter.outputImage?.transformed(by: transform) {
                return UIImage(ciImage: output)
            }
        }
        
        return nil
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.navigationController?.tabBarController?.tabBar.isHidden = false
      }

    
    
}


extension AccountViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return profileKeys.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: AccountTableViewCell.cellIdentifier(), for: indexPath) as! AccountTableViewCell
        cell.preservesSuperviewLayoutMargins = false
        cell.separatorInset = UIEdgeInsets.zero
        cell.layoutMargins = UIEdgeInsets.zero
        var value = ""
        var cellType: AccountInfoTableViewCellType = .nonTappable
        switch indexPath.row {
        case ProfileInfoDataType.firstName.rawValue:
            value = UserStore.shared.firstName
        case ProfileInfoDataType.lastName.rawValue:
            value = UserStore.shared.lastName
        case ProfileInfoDataType.email.rawValue:
            value = UserStore.shared.emailId
        case ProfileInfoDataType.dob.rawValue:
            value = UserStore.shared.DOB.isEmpty ? "Not Set" : UserStore.shared.DOB
        case ProfileInfoDataType.gender.rawValue:
            value = UserStore.shared.gender
        case ProfileInfoDataType.badges.rawValue:
            cellType = .tappable
        case ProfileInfoDataType.phoneNumber.rawValue:
            value = UserStore.shared.phoneNumber.isEmpty ? "Not Set" : UserStore.shared.phoneNumber
        case ProfileInfoDataType.cardNumber.rawValue:
            value = UserStore.shared.cardNumber.isEmpty ? "Not Set" : UserStore.shared.cardNumber
        case ProfileInfoDataType.myQR.rawValue:
            cellType = .tappable
        case ProfileInfoDataType.myBookings.rawValue:
            cellType = .tappable
        case ProfileInfoDataType.help.rawValue:
            cellType = .tappable
        default:
            break
        }
        cell.configureCellWithTitle(profileKeys[indexPath.row], info: value, cellType: cellType)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
        var nextViewController: BaseViewController!
        
        if indexPath.row == ProfileInfoDataType.help.rawValue {
            nextViewController = mainStoryboard.instantiateViewController(withIdentifier: "HelpAndSettingViewController") as! HelpAndSettingViewController
        } else if indexPath.row == ProfileInfoDataType.badges.rawValue {
            nextViewController = mainStoryboard.instantiateViewController(withIdentifier: "BadgeViewController") as! BadgeViewController
        }else if indexPath.row == ProfileInfoDataType.myQR.rawValue {
            nextViewController = mainStoryboard.instantiateViewController(withIdentifier: "QRViewController") as! QRViewController
        }else if indexPath.row == ProfileInfoDataType.myBookings.rawValue {
            nextViewController = mainStoryboard.instantiateViewController(withIdentifier: "MyBookingsViewController") as! MyBookingsViewController
        }
        if let nextVC = nextViewController {
            self.tabBarController?.tabBar.isHidden = true
            self.navigationController?.pushViewController(nextVC, animated: true)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50.0
    }
}
