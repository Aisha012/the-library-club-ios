//
//  BookingDetailViewController.swift
//  TheLibraryClub
//
//  Created by Kalpana_Hipster on 03/11/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import UIKit
import RxSwift

class BookingDetailViewController: BaseViewController {
        
        @IBOutlet weak var tableView: UITableView!
    
    var myBookingsDetail: MyBookings?
    
        override func viewDidLoad() {
            super.viewDidLoad()
            tableView.tableFooterView = UIView()
        }
        
        override func viewWillAppear(_ animated: Bool) {
            super.viewWillAppear(animated)
            self.tabBarController?.tabBar.isHidden = true
        }
        
    }
    
    extension BookingDetailViewController: UITableViewDelegate, UITableViewDataSource {
        
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return 1
        }
        
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

            let cell = tableView.dequeueReusableCell(withIdentifier: BookingDetailTableViewCell.cellIdentifier(), for: indexPath) as! BookingDetailTableViewCell

            cell.textLabelOne.text = getDateFromString(dateString: (myBookingsDetail?.bookingDate)!, fromFormat: "yyyy-MM-dd", toFormat: "dd MMM, E")
        
            cell.textLabelTwo.text = GMTToLocal(getDate: (myBookingsDetail?.bookingTime)!)
           
            cell.textLabelThree.text = myBookingsDetail?.phoneNumber
            cell.textLabelFour.text = "Number of person - \(myBookingsDetail?.personsPax ?? 0)"
            cell.textLabelFive.text =  myBookingsDetail?.email
            /*\(myBookingsDetail!.outlet.name) \n*/
            var textStr = ""
            let bday = myBookingsDetail!.birthday ? "Birthday Request" : ""
            textStr.append(contentsOf: bday)
            
            let anniversary = myBookingsDetail!.anniversary ? textStr != "" ? "\nAnniversary Request" : "Anniversary Request" : ""
            textStr.append(contentsOf: anniversary)
            
            let alergy = (myBookingsDetail!.alergy != nil && myBookingsDetail!.alergy != "" ) ? textStr != "" ? "\nAlergy: \(myBookingsDetail?.alergy ?? "")" : "Alergy: \(myBookingsDetail?.alergy ?? "")" : ""
            textStr.append(contentsOf: alergy)
            
            let remarks = (myBookingsDetail!.remarks != nil && myBookingsDetail!.remarks != "") ? textStr != "" ? "\nRemarks: \(myBookingsDetail?.remarks ?? "")" : "Remarks: \(myBookingsDetail?.remarks ?? "")" : ""
            textStr.append(contentsOf: remarks)
            
            cell.textLabelSix.text = textStr
            
            return cell
        }
        
        func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
            return UITableViewAutomaticDimension
        }
    
}

class BookingDetailTableViewCell: UITableViewCell {
    
    @IBOutlet weak var outerView: UIView!
    @IBOutlet weak var textLabelOne: UILabel!
    @IBOutlet weak var textLabelTwo: UILabel!
    @IBOutlet weak var textLabelThree: UILabel!
    @IBOutlet weak var textLabelFour: UILabel!
    @IBOutlet weak var textLabelFive: UILabel!
    @IBOutlet weak var textLabelSix: UILabel!
    
    class func cellIdentifier() -> String {
        return "BookingDetailTableViewCell"
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        if let _ = outerView {
            self.outerView.layer.shadowOffset = CGSize(width: 4, height: 4)
            self.outerView.layer.shadowColor = UIColor.black.cgColor
            self.outerView.layer.shadowRadius = 1
            self.outerView.layer.cornerRadius = 8
            self.outerView.layer.borderColor = UIColor.lightGray.cgColor
            self.outerView.layer.borderWidth = 0.5
            self.outerView.layer.shadowOpacity = 0.1
            self.outerView.layer.masksToBounds = false
            self.outerView.layer.rasterizationScale = UIScreen.main.scale
            self.outerView.clipsToBounds = false
        }
        
    }
    
    
}
