//
//  ChangePasswordViewController.swift
//  TheLibraryClub
//
//  Created by Phaninder on 22/01/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import UIKit
import RxSwift

class ChangePasswordViewController: BaseViewController {

    @IBOutlet weak var newPasswordTextField: UITextField!
    @IBOutlet weak var confirmPasswordTextField: UITextField!
    
    var disposableBag = DisposeBag()

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    func isDataValid() -> Bool {
        let newPassword = newPasswordTextField.text!
        let confirmPassword = confirmPasswordTextField.text!
        
        if newPassword.isEmpty {
            Utilities.showToast(withString: "New password field cannot be empty.".localized)
            return false
        } else if confirmPassword.isEmpty {
            Utilities.showToast(withString: "Confirm password field cannot be empty.".localized)
            return false
        } else if newPassword.count < 6 {
            Utilities.showToast(withString: "New password should be atleast 6 characters long.".localized)
            return false
        } else if newPassword != confirmPassword {
            Utilities.showToast(withString: "New password and confirm password does not match.".localized)
            return false
        }
        return true
    }

    @IBAction func changePasswordButtonTapped(_ sender: UIButton) {
        self.view.endEditing(true)
        if isDataValid() {
            changePassword()
        }
    }
    
    func changePassword() {
        guard Utilities.shared.isNetworkReachable() else {
            self.showNoInternetMessage()
            return
        }
        Utilities.showHUD(forView: self.view, excludeViews: [])
        let changePasswordObserver = ApiManager.shared.apiService.changePassword(newPasswordTextField.text!)
        let changePasswordDisposable = changePasswordObserver.subscribe(onNext: {(message) in
            DispatchQueue.main.async(execute: {
                Utilities.hideHUD(forView: self.view)
                Utilities.showToast(withString: message)
                self.navigateBack(sender: UIButton())
            })
        }, onError: {(error) in
            DispatchQueue.main.async(execute: {
                Utilities.hideHUD(forView: self.view)
                if let error_ = error as? ResponseError {
                    Utilities.showToast(withString: error_.description())
                } else {
                    Utilities.showToast(withString: error.localizedDescription)
                }
            })
        })
        changePasswordDisposable.disposed(by: disposableBag)
    }

    
}
