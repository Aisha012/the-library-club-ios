//
//  MyBookingsViewController.swift
//  TheLibraryClub
//
//  Created by Kalpana_Hipster on 03/11/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import UIKit
import RxSwift

class MyBookingsViewController: BaseViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var emptyPlaceholderLabel: UILabel!
    
    private let refreshControl = UIRefreshControl()
    
    var myBookingsPagination = MyBookingsPagination.init();
    var isFirstTime = true
    var myBookingsObserver: Observable<MyBookingsPagination>!
    var disposableBag = DisposeBag()
    var isFetchingData = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if #available(iOS 10.0, *) {
            tableView.refreshControl = refreshControl
        } else {
            tableView.addSubview(refreshControl)
        }
        refreshControl.addTarget(self, action: #selector(refreshData(_:)), for: .valueChanged)
        tableView.tableFooterView = UIView()
        fetchMyBookings()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tabBarController?.tabBar.isHidden = true
    }
    
    @objc func refreshData(_ sender: Any) {
        self.myBookingsPagination.paginationType = .reload
        fetchMyBookings()
        if let refreshControl = sender as? UIRefreshControl {
            refreshControl.endRefreshing()
        }
    }
    
    func fetchMyBookings() {
        guard Utilities.shared.isNetworkReachable() else {
            self.showNoInternetMessage()
            return
        }
        
        if myBookingsPagination.paginationType != .old {
            self.myBookingsPagination.currentPageNumber = 1
            if isFirstTime == true {
                Utilities.showHUD(forView: self.view, excludeViews: [])
            } else {
                UIApplication.shared.isNetworkActivityIndicatorVisible = true
            }
        } else {
            UIApplication.shared.isNetworkActivityIndicatorVisible = true
        }
        
        isFetchingData = true
        myBookingsObserver = ApiManager.shared.apiService.fetchMyBookings(pageNumber: String(myBookingsPagination.currentPageNumber))
        let myBookingsDisposable = myBookingsObserver.subscribe(onNext: {(paginationObject) in
            self.myBookingsPagination.appendDataFromObject(paginationObject)
            self.isFetchingData = false
            DispatchQueue.main.async(execute: {
                Utilities.hideHUD(forView: self.view)
                if self.myBookingsPagination.paginationType != .old && self.isFirstTime == true {
                    self.isFirstTime = false
                } else {
                    UIApplication.shared.isNetworkActivityIndicatorVisible = false
                }
                if self.myBookingsPagination.myBookings.count == 0 {
                    self.emptyPlaceholderLabel.isHidden = false
                    self.tableView.isHidden = true
                } else {
                    self.emptyPlaceholderLabel.isHidden = true
                    self.tableView.isHidden = false
                }
                self.tableView.reloadData()
            })
        }, onError: {(error) in
            self.isFetchingData = false
            DispatchQueue.main.async(execute: {
                Utilities.hideHUD(forView: self.view)
            })
        })
        myBookingsDisposable.disposed(by: disposableBag)
    }
    
}

extension MyBookingsViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let myBookingsCount = myBookingsPagination.myBookings.count
        return myBookingsPagination.hasMoreToLoad ? myBookingsCount + 1 : myBookingsCount
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard indexPath.row < myBookingsPagination.myBookings.count else {
            let cell = tableView.dequeueReusableCell(withIdentifier: LoadMoreTableViewCell.cellIdentifier(), for: indexPath) as! LoadMoreTableViewCell
            cell.activityIndicator.startAnimating()
            return cell
        }
        let cell = tableView.dequeueReusableCell(withIdentifier: MyBookingTableViewCell.cellIdentifier(), for: indexPath) as! MyBookingTableViewCell
        let myBookingObj = myBookingsPagination.myBookings[indexPath.row]
    
        cell.dateLabel.text = getDateFromString(dateString: (myBookingObj.bookingDate)!, fromFormat: "yyyy-MM-dd", toFormat: "dd MMM, E")
        cell.timeLabel.text = GMTToLocal(getDate: (myBookingObj.bookingTime)!)
        cell.locationLabel.text = myBookingObj.outlet.name
        
        cell.statusLabel.textColor = myBookingObj.status == "booked" ? Utilities.colorFromHex(0xf4b342, alpha: 1) : myBookingObj.status == "confirmed" ? Utilities.colorFromHex(0x33a34d, alpha: 1) : myBookingObj.status == "cancelled" ? Utilities.colorFromHex(0xf22020, alpha: 1) : UIColor
                .clear
        cell.statusLabel.text = myBookingObj.status == "booked" ? "Pending" : myBookingObj.status == "confirmed" ? "Confirmed" : myBookingObj.status == "cancelled" ? "Rejected" : ""
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let bookingObj = myBookingsPagination.myBookings[indexPath.row]
        
            let storyBoard = UIStoryboard.init(name: "Main", bundle: nil)
            let viewController = storyBoard.instantiateViewController(withIdentifier: "BookingDetailViewController") as! BookingDetailViewController
            viewController.myBookingsDetail = bookingObj
            UIApplication.topViewController()?.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        guard indexPath.row <  myBookingsPagination.myBookings.count else {
            return 50.0
        }
        
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row == self.myBookingsPagination.myBookings.count && self.myBookingsPagination.hasMoreToLoad && !isFetchingData {
            fetchMyBookings()
        }
    }
    
}

class MyBookingTableViewCell: UITableViewCell {
    
    @IBOutlet weak var outerView: UIView!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var statusLabel: UILabel!
    
    class func cellIdentifier() -> String {
        return "MyBookingTableViewCell"
    }
    
    
    override func layoutSubviews() {
        super.layoutSubviews()
        if let _ = outerView {
            self.outerView.layer.shadowOffset = CGSize(width: 4, height: 4)
            self.outerView.layer.shadowColor = UIColor.black.cgColor
            self.outerView.layer.shadowRadius = 1
            self.outerView.layer.cornerRadius = 8
            self.outerView.layer.borderColor = UIColor.lightGray.cgColor
            self.outerView.layer.borderWidth = 0.5
            self.outerView.layer.shadowOpacity = 0.1
            self.outerView.layer.masksToBounds = false
            self.outerView.layer.rasterizationScale = UIScreen.main.scale
            self.outerView.clipsToBounds = false
        }
        
    }
    
    
}


