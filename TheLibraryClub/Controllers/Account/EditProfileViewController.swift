//
//  EditProfileViewController.swift
//  TheLibraryClub
//
//  Created by Phaninder on 22/01/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import UIKit
import RxSwift
import Alamofire
import Unbox

class EditProfileViewController: BaseViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    var activeTextField: UITextField!
    var userUpdate = UserUpdate()
    var disposableBag = DisposeBag()
    
    
    let labelFields = ["First Name:", "Last Name:", "Gender:", "Phone Number:"]
    let placeHolderFields = ["John", "Doe", "Tap to select Gender", ""]
    let genderArray = ["Male", "Female"]
    var isImageSelected = false
    var profileImage: UIImage!
    let pickerView = UIPickerView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.tableFooterView = UIView()
        pickerView.delegate = self
        pickerView.dataSource = self
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func configureTextField(_ textField: UITextField) {
        textField.delegate = self
        textField.placeholder = placeHolderFields[textField.tag]
        textField.isSecureTextEntry = false
        textField.keyboardType = .asciiCapable
        
        switch textField.tag {
        case UpdateProfileTextFieldType.gender.rawValue:
            textField.inputView = pickerView
        case UpdateProfileTextFieldType.phoneNumber.rawValue:
            textField.keyboardType = .phonePad
        default:
            textField.keyboardType = .asciiCapable
        }
    }
    
    @IBAction func updateProfileButtonTapped(_ sender: UIButton) {
        self.view.endEditing(true)
        if isDataValid() == false {
            return
        }
        if isImageSelected == true {
            updateUserWithImage()
        } else {
            updateUser()
        }

    }
    
    func updateUser() {
        guard Utilities.shared.isNetworkReachable() else {
            self.showNoInternetMessage()
            return
        }
        var gender = "0"
        if userUpdate.gender == "Male" {
            gender = "1"
        } else if userUpdate.gender == "Female" {
            gender = "2"
        }

        Utilities.showHUD(forView: self.view, excludeViews: [])
        let editProfileObserver = ApiManager.shared.apiService.editUserWithEmail(UserStore.shared.emailId, firstName: userUpdate.firstName, lastName: userUpdate.lastName, userId: UserStore.shared.userId, dateOfBirth: userUpdate.dob, gender: gender, phoneNumber: userUpdate.phoneNumber)
        let editProfileDisposable = editProfileObserver.subscribe(onNext: {(user) in
            UserStore.shared.emailId = user.email
            UserStore.shared.firstName = user.firstName
            UserStore.shared.lastName = user.lastName
            UserStore.shared.profilePic = user.profilePic
            UserStore.shared.DOB = user.dob
            UserStore.shared.gender = user.gender
            UserStore.shared.phoneNumber = user.phonenum
            UserStore.shared.cardNumber = user.cardNum
            DispatchQueue.main.async(execute: {
                Utilities.hideHUD(forView: self.view)
                NotificationCenter.default.post(name: .profileUpdated, object: nil, userInfo: nil)
                Utilities.showToast(withString: "Successfully updated.".localized)

            })
        }, onError: {(error) in
            DispatchQueue.main.async(execute: {
                Utilities.hideHUD(forView: self.view)
                if let error_ = error as? ResponseError {
                    Utilities.showToast(withString: error_.description())
                } else {
                    Utilities.showToast(withString: error.localizedDescription)
                }
            })
        })
        editProfileDisposable.disposed(by: disposableBag)
    }

    func updateUserWithImage() {

        let imageData = UIImageJPEGRepresentation(profileImage, 0.5)
        
        guard Utilities.shared.isNetworkReachable() else {
            self.showNoInternetMessage()
            return
        }
        var gender = "0"
        if userUpdate.gender == "Male" {
            gender = "1"
        } else if userUpdate.gender == "Female" {
            gender = "2"
        }
        Utilities.showHUD(forView: self.view, excludeViews: [])
        let params = ["email": UserStore.shared.emailId,
                      "id": UserStore.shared.userId,
                      "firstname": userUpdate.firstName,
                      "lastname": userUpdate.lastName,
                      "dob": userUpdate.dob,
                      "gender": gender,
                      "phonenum": userUpdate.phoneNumber]
        
        let url = "http://librarycard.club/mcustomerupdate.php" /* your API url */

        let headers: HTTPHeaders = [
            "Content-type": "multipart/form-data"
        ]

        Alamofire.upload(multipartFormData: { (multipartFormData) in
            for (key, value) in params {
                multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
            }

            if let data = imageData {
                multipartFormData.append(data, withName: "profilepic", fileName: "image.png", mimeType: "image/png")
            }

        }, usingThreshold: UInt64.init(), to: url, method: .post, headers: headers) { (result) in
            switch result{
            case .success(let upload, _, _):
                upload.responseJSON { response in
                    print("Succesfully uploaded")
                    if let _ = response.error{
                        return
                    }
                    let json = try? JSONSerialization.jsonObject(with: response.data!, options: .mutableContainers)
                    if let jsonArray = json as? [[String: AnyObject]] {
                        print(jsonArray)
                        let userDict = jsonArray[0]
                        do {
                            let user: User = try unbox(dictionary: userDict)
                            UserStore.shared.emailId = user.email
                            UserStore.shared.firstName = user.firstName
                            UserStore.shared.lastName = user.lastName
                            UserStore.shared.profilePic = user.profilePic
                            UserStore.shared.DOB = user.dob
                            UserStore.shared.gender = user.gender
                            UserStore.shared.phoneNumber = user.phonenum
                            UserStore.shared.cardNumber = user.cardNum
                            DispatchQueue.main.async(execute: {
                                Utilities.hideHUD(forView: self.view)
                                NotificationCenter.default.post(name: .profileUpdated, object: nil, userInfo: nil)
                                Utilities.showToast(withString: "Successfully updated.".localized)
                            })
                        } catch {}
                    }
                }
            case .failure(let error):
                print("Error in upload: \(error.localizedDescription)")
            }
        }
    }

    func updateDataIntoTextField(_ textField: UITextField, fromCell: Bool) {
        switch textField.tag {
        case UpdateProfileTextFieldType.firstName.rawValue:
            fromCell ? (textField.text = userUpdate.firstName) : (userUpdate.firstName = textField.text!)
            break
        case UpdateProfileTextFieldType.lastName.rawValue:
            fromCell ? (textField.text = userUpdate.lastName) : (userUpdate.lastName = textField.text!)
            break
        case UpdateProfileTextFieldType.gender.rawValue:
            fromCell ? (textField.text = userUpdate.gender) : (userUpdate.gender = textField.text!)
            break
        case UpdateProfileTextFieldType.phoneNumber.rawValue:
            fromCell ? (textField.text = userUpdate.phoneNumber) : (userUpdate.phoneNumber = textField.text!)
            break
        default:
            break
        }
    }
    
    func isDataValid() -> Bool {
        if !userUpdate.firstName.isValidName() {
            Utilities.showToast(withString: "Please enter First Name.".localized)
            return false
        }
        if !userUpdate.lastName.isValidName() {
            Utilities.showToast(withString: "Please enter Last Name.".localized)
            return false
        }
//        if userUpdate.gender.isEmpty {
//            Utilities.showToast(withString: "Please select Gender.".localized)
//            return false
//        }
        if !userUpdate.phoneNumber.isValidPhoneNumber() {
            Utilities.showToast(withString: "Please enter valid Phone Number.".localized)
            return false
        }
        return true
    }
    
    func clearAllFields() {
        userUpdate =  UserUpdate()
        tableView.reloadData()
    }
    
}

extension EditProfileViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return labelFields.count + 1;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard indexPath.row != 0 else {
            let cell = tableView.dequeueReusableCell(withIdentifier: ProfilePictureTableViewCell.cellIdentifier(),
                                                     for: indexPath) as! ProfilePictureTableViewCell
            if isImageSelected == false, let url = URL(string: UserStore.shared.profilePic) {
                cell.profileImageView.sd_setImage(with: url, placeholderImage: UIImage.init(named: "dummy"), options: .retryFailed, completed: { (image, error, cacheType, url) in
                    if image != nil {
                        self.profileImage = image
                    }
                })
                cell.profileImageView?.sd_setImage(with: url, placeholderImage: UIImage.init(named: "dummy"))
            } else if let profilePicture = profileImage {
                cell.profileImageView.image = profilePicture
            } else {
                cell.profileImageView.image = UIImage(named: "dummy")
            }
            cell.delegate = self
            return cell
        }
        let cell = tableView.dequeueReusableCell(withIdentifier: TextFieldTableViewCell.cellIdentifier(), for: indexPath) as! TextFieldTableViewCell
        cell.configureCellForRowAtIndex(indexPath.row - 1, withText: labelFields[indexPath.row - 1])
        updateDataIntoTextField(cell.inputTextField, fromCell: true)
        configureTextField(cell.inputTextField)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row != 0 {
            return 79.0
        } else {
            return 107.0
        }
    }
}

extension EditProfileViewController: UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        activeTextField = textField
        if textField.tag == UpdateProfileTextFieldType.gender.rawValue {
            managePickerView()
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        updateDataIntoTextField(textField, fromCell: false)
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if (textField.text?.isEmpty)! && string == " " {
            return false
        }
        let char = string.cString(using: String.Encoding.utf8)!
        let isBackSpace = strcmp(char, "\\b")
        if (isBackSpace == -92) {
            return true
        }
        switch textField.tag {
        case UpdateProfileTextFieldType.firstName.rawValue, UpdateProfileTextFieldType.lastName.rawValue:
            return (textField.text?.count)! < NameMaxLimit
        case UpdateProfileTextFieldType.phoneNumber.rawValue:
            return (textField.text?.count)! < EmailMaxLimit
        default:
            return true
        }
    }
    
}

extension EditProfileViewController: ProfilePictureTableViewCellDelegate {
    
    func showImageOptionsforButton(button: UIButton) {
        let alert = UIAlertController(title: "Choose Image", message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { _ in
            self.camera()
        }))
        
        alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { _ in
            self.photoLibrary()
        }))
        
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
        
        /*If you want work actionsheet on ipad
         then you have to use popoverPresentationController to present the actionsheet,
         otherwise app will crash on iPad */
        switch UIDevice.current.userInterfaceIdiom {
        case .pad:
            alert.popoverPresentationController?.sourceView = button
            alert.popoverPresentationController?.sourceRect = button.bounds
            alert.popoverPresentationController?.permittedArrowDirections = .up
        default:
            break
        }
        
        self.present(alert, animated: true, completion: nil)
    }

}

extension EditProfileViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func managePickerView() {
        userUpdate.gender = userUpdate.gender.isEmpty || userUpdate.gender == "Not Set" ? "Male" : userUpdate.gender
        activeTextField.text = userUpdate.gender
        let index = genderArray.index(of: userUpdate.gender)!
        pickerView.selectRow(index, inComponent: 0, animated: true)
    }

    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return genderArray.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return genderArray[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        activeTextField.text = genderArray[row]
    }

}

extension EditProfileViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func camera() {
        if UIImagePickerController.isSourceTypeAvailable(.camera){
            let myPickerController = UIImagePickerController()
            myPickerController.delegate = self;
            myPickerController.sourceType = .camera
            self.present(myPickerController, animated: true, completion: nil)
        }
        
    }
    
    func photoLibrary() {
        
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary){
            let myPickerController = UIImagePickerController()
            myPickerController.delegate = self;
            myPickerController.sourceType = .photoLibrary
            self.present(myPickerController, animated: true, completion: nil)
        }
    }

    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        let image = info[UIImagePickerControllerOriginalImage] as! UIImage
        let cell = tableView.cellForRow(at: IndexPath(row: 0, section: 0)) as! ProfilePictureTableViewCell
        self.profileImage = image
        cell.profileImageView.image = image
        isImageSelected = true
        dismiss(animated:true, completion: nil)
    }
    
}
