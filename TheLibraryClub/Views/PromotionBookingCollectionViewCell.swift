//
//  PromotionBookingCollectionViewCell.swift
//  TheLibraryClub
//
//  Created by Phaninder on 16/03/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import UIKit

class PromotionBookingCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var holderView: UIView!
    @IBOutlet weak var promotionImageView: UIImageView!
    
    class func cellIdentifier() -> String {
        return "PromotionBookingCollectionViewCell"
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        self.holderView.layer.shadowOffset = CGSize(width: 0, height: 0)
        self.holderView.layer.shadowColor = UIColor.black.cgColor
        self.holderView.layer.shadowRadius = 4
        self.holderView.layer.cornerRadius = 5
        self.holderView.layer.shadowOpacity = 0.3
        self.holderView.layer.masksToBounds = false
        self.holderView.layer.rasterizationScale = UIScreen.main.scale
        self.holderView.clipsToBounds = false
    }

    
}
