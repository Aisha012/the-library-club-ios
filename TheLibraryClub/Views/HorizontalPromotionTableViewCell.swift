//
//  HorizontalPromotionTableViewCell.swift
//  TheLibraryClub
//
//  Created by Phaninder on 20/01/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import UIKit
import SDWebImage

protocol HorizontalPromotionTableViewCellDelegate {
    
    func viewMoreOfPromotionOneTapped()
    func viewMoreOfPromotionTwoTapped()
    func viewMorePromotionsTapped()
    
}

class HorizontalPromotionTableViewCell: UITableViewCell {

    @IBOutlet weak var promotionStackView: UIStackView!
    @IBOutlet weak var headingLabel: UILabel!
    @IBOutlet weak var promotionOnePlaceholderView: UIView!
    @IBOutlet weak var promotionOneView: UIView!
    @IBOutlet weak var promotionTwoPlaceholderView: UIView!
    @IBOutlet weak var promotionTwoView: UIView!
    @IBOutlet weak var promotionOneImageView: UIImageView!
    @IBOutlet weak var promotionTwoImageView: UIImageView!
    @IBOutlet weak var promotionOneDescLabel: UILabel!
    @IBOutlet weak var promotionTwoDescLabel: UILabel!
    @IBOutlet weak var promotionOneViewButton: UIButton!
    @IBOutlet weak var promotionTwoViewButton: UIButton!
    @IBOutlet weak var viewMoreButton: UIButton!
    @IBOutlet weak var emptyPlaceholderLabel: UILabel!
    
    var delegate: HorizontalPromotionTableViewCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        let buttonTitleStr = Utilities.getUnderLineTextWithFontFamily("Roboto-Light", ofFontSize: 15.0, andText: "View More")
        viewMoreButton.setAttributedTitle(buttonTitleStr, for: .normal)
    }

    class func cellIdentifier() -> String {
        return "HorizontalPromotionTableViewCell"
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        if let _ = promotionOnePlaceholderView {
            self.promotionOneView.layer.shadowOffset = CGSize(width: 4, height: 4)
            self.promotionOneView.layer.shadowColor = UIColor.black.cgColor
            self.promotionOneView.layer.shadowRadius = 1
            self.promotionOneView.layer.cornerRadius = 8
            self.promotionOneView.layer.borderColor = UIColor.lightGray.cgColor
            self.promotionOneView.layer.borderWidth = 0.5
            self.promotionOneView.layer.shadowOpacity = 0.1
            self.promotionOneView.layer.masksToBounds = false
            self.promotionOneView.layer.rasterizationScale = UIScreen.main.scale
            self.promotionOneView.clipsToBounds = false
        }
        
        if let _ = promotionTwoPlaceholderView {
            self.promotionTwoView.layer.shadowOffset = CGSize(width: 4, height: 4)
            self.promotionTwoView.layer.shadowColor = UIColor.black.cgColor
            self.promotionTwoView.layer.shadowRadius = 1
            self.promotionTwoView.layer.cornerRadius = 8
            self.promotionTwoView.layer.borderColor = UIColor.lightGray.cgColor
            self.promotionTwoView.layer.borderWidth = 0.5
            self.promotionTwoView.layer.shadowOpacity = 0.1
            self.promotionTwoView.layer.masksToBounds = false
            self.promotionTwoView.layer.rasterizationScale = UIScreen.main.scale
            self.promotionTwoView.clipsToBounds = false
        }

    }

    func configureCellWithPromotions(_ promotions: [Promotion]) -> CGFloat {
        var height: CGFloat = 320.0
        if promotions.count <= 2 {
            viewMoreButton.isHidden = true
        }
        
        if promotions.count > 1 {
            let promotion = promotions[1]
            promotionTwoImageView.sd_setShowActivityIndicatorView(true)
            promotionTwoImageView.sd_setIndicatorStyle(.gray)
            promotionTwoImageView.sd_setImage(with: URL(string: promotion.imageURL), completed: nil)
            promotionTwoDescLabel.text = promotion.promoDesc
            
            if promotions.count == 2 {
                height -= 40.0
            }
        } else {
            removePromotionView(view: promotionTwoPlaceholderView)
            if let placeHolderView = promotionOnePlaceholderView {
                let widthConstraint = NSLayoutConstraint (item: placeHolderView,
                                                          attribute: NSLayoutAttribute.width,
                                                          relatedBy: NSLayoutRelation.equal,
                                                          toItem: nil,
                                                          attribute: NSLayoutAttribute.notAnAttribute,
                                                          multiplier: 1,
                                                          constant: (ScreenWidth/2 - 24))
                self.addConstraint(widthConstraint)
                
                let xConstraint = NSLayoutConstraint(item: placeHolderView,
                                                     attribute: .centerX,
                                                     relatedBy: .equal,
                                                     toItem: self,
                                                     attribute: .centerX,
                                                     multiplier: 1,
                                                     constant: 0)
                self.addConstraint(xConstraint)
            }
        }
        
        if promotions.count > 0 {
            let promotion = promotions[0]
            promotionOneImageView.sd_setShowActivityIndicatorView(true)
            promotionOneImageView.sd_setIndicatorStyle(.gray)
            promotionOneImageView.sd_setImage(with: URL(string: promotion.imageURL), completed: nil)
            promotionOneDescLabel.text = promotion.promoDesc

            if promotions.count == 1 {
                height -= 40.0
            }
        } else {
            removePromotionView(view: promotionTwoPlaceholderView)
            removePromotionView(view: promotionOnePlaceholderView)
            height = 86.0
        }
        
        return height

    }
    
    func removePromotionView(view: UIView?) {
        if let promotionView = view {
            if let _ = promotionStackView.arrangedSubviews.index(of: promotionView) {
                promotionStackView.removeArrangedSubview(promotionView)
                promotionView.removeFromSuperview()
                if promotionStackView.arrangedSubviews.count == 0 {
                }
            }
        }
    }
    
    @IBAction func viewMorePromotionOneButtonTapped(_ sender: UIButton) {
        delegate?.viewMoreOfPromotionOneTapped()
    }
    
    @IBAction func viewMorePromotionTwoButtonTapped(_ sender: UIButton) {
        delegate?.viewMoreOfPromotionTwoTapped()
    }
    
    @IBAction func viewMorePromotionsButtonTapped(_ sender: UIButton) {
        delegate?.viewMorePromotionsTapped()
    }
    
    
    
}
