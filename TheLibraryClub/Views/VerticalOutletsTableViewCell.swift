//
//  VerticalOutletsTableViewCell.swift
//  TheLibraryClub
//
//  Created by Phaninder on 21/01/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import UIKit
import SDWebImage

protocol VerticalOutletsTableViewCellDelegate {
    
    func outletOneButtonTapped()
    func outletTwoButtonTapped()
    func viewMoreButtonTapped()
    
}

class VerticalOutletsTableViewCell: UITableViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var outletStackView: UIStackView!
    @IBOutlet weak var outletOnePlaceholderView: UIView!
    @IBOutlet weak var outletTwoPlaceholderView: UIView!
    @IBOutlet weak var outletOneImageView: UIImageView!
    @IBOutlet weak var outletTwoImageView: UIImageView!
    @IBOutlet weak var outletOneDistanceLabel: UILabel!
    @IBOutlet weak var outletTwoDistanceLabel: UILabel!
    @IBOutlet weak var outletOneNameLabel: UILabel!
    @IBOutlet weak var outletTwoNameLabel: UILabel!
    @IBOutlet weak var outletOneAddressLabel: UILabel!
    @IBOutlet weak var outletTwoAddressLabel: UILabel!
    @IBOutlet weak var viewMoreButton: UIButton!
    @IBOutlet weak var noOutletsMessageLabel: UILabel!

    var delegate: VerticalOutletsTableViewCellDelegate?
    
    class func cellIdentifier() -> String {
        return "VerticalOutletsTableViewCell"
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        let buttonTitleStr = Utilities.getUnderLineTextWithFontFamily("Roboto-Light", ofFontSize: 15.0, andText: "View More")
        viewMoreButton.setAttributedTitle(buttonTitleStr, for: .normal)
    }

    func configureCellWithOutlets(_ outlets: [Outlet]) ->CGFloat {
        var height: CGFloat = 476
        if outlets.count <= 2 {
            viewMoreButton.isHidden = true
            height = 446
        }
        
        if outlets.count > 1 {
            let outlet = outlets[1]
            outletTwoImageView.sd_setShowActivityIndicatorView(true)
            outletTwoImageView.sd_setIndicatorStyle(.gray)
            outletTwoImageView.sd_setImage(with: URL(string: outlet.imageURL), completed: nil)
            outletTwoDistanceLabel.text = outlet.distance
            outletTwoNameLabel.text = outlet.name
            outletTwoAddressLabel.text = outlet.address
        } else {
            removeOutletView(view: outletTwoPlaceholderView)
            if let placeHolderView = outletOnePlaceholderView {
                let widthConstraint = NSLayoutConstraint (item: placeHolderView,
                                                          attribute: NSLayoutAttribute.width,
                                                          relatedBy: NSLayoutRelation.equal,
                                                          toItem: nil,
                                                          attribute: NSLayoutAttribute.notAnAttribute,
                                                          multiplier: 1,
                                                          constant: (ScreenWidth - 32))
                self.addConstraint(widthConstraint)
                
                let xConstraint = NSLayoutConstraint(item: placeHolderView,
                                                     attribute: .centerX,
                                                     relatedBy: .equal,
                                                     toItem: self,
                                                     attribute: .centerX,
                                                     multiplier: 1,
                                                     constant: 0)
                self.addConstraint(xConstraint)
            }

            height = 245.0
        }

        if outlets.count > 0 {
            let outlet = outlets[0]
            outletOneImageView.sd_setShowActivityIndicatorView(true)
            outletOneImageView.sd_setIndicatorStyle(.gray)
            outletOneImageView.sd_setImage(with: URL(string: outlet.imageURL), completed: nil)
            outletOneDistanceLabel.text = outlet.distance
            outletOneNameLabel.text = outlet.name
            outletOneAddressLabel.text = outlet.address
        } else {
            height = 71.0
            noOutletsMessageLabel.isHidden = false
            removeOutletView(view: outletOnePlaceholderView)
        }
        
        return height
    }
    
    func removeOutletView(view: UIView?) {
        if let outletView = view {
            if let _ = outletStackView.arrangedSubviews.index(of: outletView) {
                outletStackView.removeArrangedSubview(outletView)
                outletView.removeFromSuperview()
            }
        }
    }

    @IBAction func outletOneButtonTapped(_ sender: UIButton) {
        delegate?.outletOneButtonTapped()
    }
    
    @IBAction func outletTwoButtonTapped(_ sender: UIButton) {
        delegate?.outletTwoButtonTapped()
    }
    
    @IBAction func viewMoreButtonTapped(_ sender: UIButton) {
        delegate?.viewMoreButtonTapped()
    }
    
    
    
}
