//
//  DescriptionTableViewCell.swift
//  TheLibraryClub
//
//  Created by Phaninder on 20/01/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import UIKit

class DescriptionTableViewCell: UITableViewCell {
    
    @IBOutlet weak var descriptionLabel: UILabel!

    class func cellIdentifier() -> String {
        return "DescriptionTableViewCell"
    }

}
