//
//  LoadMoreTableViewCell.swift
//  TheLibraryClub
//
//  Created by Phaninder on 19/01/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import UIKit

class LoadMoreTableViewCell: UITableViewCell {

    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    class func cellIdentifier() -> String {
        return "LoadMoreTableViewCell"
    }
    
}
