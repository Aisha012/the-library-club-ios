//
//  OutletMapView.swift
//  TheLibraryClub
//
//  Created by Phaninder on 09/05/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import Foundation
import MapKit

class OutletMapView: MKAnnotationView {
    
    override var annotation: MKAnnotation? {
        willSet {
            guard let artwork = newValue as? OutletLocation else {return}
            canShowCallout = true
            calloutOffset = CGPoint(x: -5, y: 5)
            let mapsButton = UIButton(frame: CGRect(origin: CGPoint.zero,
                                                    size: CGSize(width: 30, height: 30)))
            mapsButton.setBackgroundImage(UIImage(named: "Maps-icon"), for: UIControlState())
            rightCalloutAccessoryView = mapsButton
            
            let detailLabel = UILabel()
            detailLabel.numberOfLines = 0
            detailLabel.font = detailLabel.font.withSize(12)
            detailLabel.text = artwork.subtitle
            detailCalloutAccessoryView = detailLabel
            image = UIImage(named: "pin")
            
            leftCalloutAccessoryView = UIButton.init(type: .infoLight)
        }
    }
}
