//
//  OutletListingTableViewCell.swift
//  TheLibraryClub
//
//  Created by Phaninder on 18/01/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import UIKit
import SDWebImage

class OutletListingTableViewCell: UITableViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var distanceLabel: UILabel!
    @IBOutlet weak var outletImageView: UIImageView!
    
    class func cellIdentifier() -> String {
        return "OutletListingTableViewCell"
    }
    
    func configureCellWithOutlet(_ outlet: Outlet) {
        nameLabel.text = outlet.name
        addressLabel.text = outlet.address
        distanceLabel.text = outlet.distance
        outletImageView.sd_setShowActivityIndicatorView(true)
        outletImageView.sd_setIndicatorStyle(.gray)
        outletImageView.sd_setImage(with: URL(string: outlet.imageURL), completed: nil)
    }
    
}
