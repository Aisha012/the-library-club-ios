//
//  OutletMapTableViewCell.swift
//  TheLibraryClub
//
//  Created by Phaninder on 20/01/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import UIKit
import GoogleMaps
import CoreLocation
import MapKit
import OpenInGoogleMaps

protocol OutletMapTableViewCellDelegate {
    func mapPinTapped()
}

class OutletMapTableViewCell: UITableViewCell, GMSMapViewDelegate {
    
    @IBOutlet var outletMapView: GMSMapView!
    @IBOutlet weak var trainLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    
    var delegate: OutletMapTableViewCellDelegate?
    
    class func cellIdentifier() -> String {
        return "OutletMapTableViewCell"
    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
       // outletMapView!.showsPointsOfInterest = true
        if let mapView = self.outletMapView {
            mapView.delegate = self
//            if #available(iOS 11.0, *) {
//                mapView.register(OutletMapView.self,
//                                 forAnnotationViewWithReuseIdentifier: MKMapViewDefaultAnnotationViewReuseIdentifier)
//
//            } else {
//                // Fallback on earlier versions
//            }

        }
    }
    
    var outletLoc:OutletLocation?
    func configureCell(outlet: OutletDescription) {
        addressLabel.text = outlet.address
        if let location = outlet.outletLocation {
            //outletMapView?.addAnnotation(location)
            outletLoc = location
            let marker = GMSMarker()
            marker.position = CLLocationCoordinate2D(latitude:location.coordinate.latitude, longitude: location.coordinate.longitude)
            marker.title = location.title!
            marker.snippet = location.subtitle!
            marker.map = outletMapView
            outletMapView.selectedMarker = marker

           // self.outletMapView?.setRegion(MKCoordinateRegionMakeWithDistance(location.coordinate, 500, 500), animated: true)
           // self.outletMapView.delegate = self
            setMapZoomToRadius(lat: location.coordinate.latitude, lng: location.coordinate.longitude, mile: 600)
            
        }
        
    }
    
    func setMapZoomToRadius(lat:Double, lng:Double,  mile:Double)
    {
        
        let center = CLLocationCoordinate2DMake(lat, lng)
        let radius: Double = (mile ) * 621.371
        
        let region = MKCoordinateRegionMakeWithDistance(center, radius * 2.0, radius * 2.0)
        
        
        let northEast = CLLocationCoordinate2DMake(region.center.latitude - region.span.latitudeDelta, region.center.longitude - region.span.longitudeDelta)
        let  southWest = CLLocationCoordinate2DMake(region.center.latitude + region.span.latitudeDelta, region.center.longitude + region.span.longitudeDelta)
        
        
        print("\(region.center.longitude)  \(region.span.longitudeDelta)")
        let bounds = GMSCoordinateBounds(coordinate: southWest, coordinate: northEast)
        
        let camera = outletMapView.camera(for: bounds, insets:UIEdgeInsets.init(top: 100, left: 0, bottom: 0, right: 0))
        outletMapView.camera = camera!;
        
    }
    
    
    
    func mapView(_ mapView: GMSMapView, didTapInfoWindowOf marker: GMSMarker) {
       // let addr = "\(outletLoc!.title!) \(outletLoc!.subtitle!)"
        
       // let urlStr = "comgooglemaps://?q=\(addr)&center=\(outletLoc!.coordinate.latitude),\(outletLoc!.coordinate.longitude)&zoom=15&views=transit"
        //let urlString = "comgooglemaps://?ll=\(outletLoc!.coordinate.latitude),\(outletLoc!.coordinate.longitude)"

        if UIApplication.shared.canOpenURL(URL(string: "comgooglemaps://")!){
          //  UIApplication.shared.canOpenURL(URL(string: urlString)!)
            let definition  = GoogleMapDefinition()
            definition.center = (outletLoc?.coordinate)!
            definition.queryString = """
             \(outletLoc!.title!)
             \(outletLoc!.subtitle!)
            """
            definition.viewOptions = GoogleMapsViewOptions(rawValue: GoogleMapsViewOptions.RawValue(UInt8(GoogleMapsViewOptions.satellite.rawValue) | UInt8(GoogleMapsViewOptions.traffic.rawValue)))
            OpenInGoogleMapsController.sharedInstance().openMap(definition)

        }else{
            let launchOptions = [MKLaunchOptionsDirectionsModeKey: MKLaunchOptionsDirectionsModeDriving]
            outletLoc!.mapItem().openInMaps(launchOptions: launchOptions)
        }
    }
    
    func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D) {
        if UIApplication.shared.canOpenURL(URL(string: "comgooglemaps://")!){
            //  UIApplication.shared.canOpenURL(URL(string: urlString)!)
            let definition  = GoogleMapDefinition()
            definition.center = (outletLoc?.coordinate)!
            definition.queryString = """
            \(outletLoc!.title!)
            \(outletLoc!.subtitle!)
            """
            definition.viewOptions = GoogleMapsViewOptions(rawValue: GoogleMapsViewOptions.RawValue(UInt8(GoogleMapsViewOptions.satellite.rawValue) | UInt8(GoogleMapsViewOptions.traffic.rawValue)))
            OpenInGoogleMapsController.sharedInstance().openMap(definition)
            
        }else{
            let launchOptions = [MKLaunchOptionsDirectionsModeKey: MKLaunchOptionsDirectionsModeDriving]
            outletLoc!.mapItem().openInMaps(launchOptions: launchOptions)
        }
    }
    
    
    
    
//    
//    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
//        
//        if annotation.isMember(of: MKUserLocation.self) {
//            return nil
//        }
//        
//        let reuseId = "MKAnnotationView"
//        
//        var pinView = mapView.dequeueReusableAnnotationView(withIdentifier: reuseId)
//        if pinView == nil {
//            pinView = MKAnnotationView(annotation: annotation, reuseIdentifier: reuseId)}
//        pinView!.canShowCallout = true
//        //        pinView!.image = UIImage(named: "logo")
//        pinView!.frame.size = CGSize(width: 40.0, height: 40.0)
//        return pinView
//    }
    
    
//    func mapView(_ mapView: MKMapView, annotationView view: MKAnnotationView, calloutAccessoryControlTapped control: UIControl) {
//        if (control == view.leftCalloutAccessoryView) {
//            delegate?.mapPinTapped()
//        } else if (control == view.rightCalloutAccessoryView) {
//            let location = view.annotation as! OutletLocation
//            let launchOptions = [MKLaunchOptionsDirectionsModeKey: MKLaunchOptionsDirectionsModeDriving]
////            location.mapItem().openInMaps(launchOptions: launchOptions)
//        }
//    }

}
