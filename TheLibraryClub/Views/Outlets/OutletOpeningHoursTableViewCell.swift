//
//  OutletOpeningHoursTableViewCell.swift
//  TheLibraryClub
//
//  Created by Phaninder on 20/01/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import UIKit

protocol OutletOpeningHoursTableViewCellDelegate {
    func viewWeekButtonTapped()
}

class OutletOpeningHoursTableViewCell: UITableViewCell {

    @IBOutlet weak var viewWeekButton: UIButton!
    @IBOutlet weak var timeStatusLabel: UILabel!
    
    var delegate: OutletOpeningHoursTableViewCellDelegate?
    
    class func cellIdentifier() -> String {
        return "OutletOpeningHoursTableViewCell"
    }
    
    func configureCellWithText(_ displayText: String, isClickable:Bool) {
        viewWeekButton.layer.cornerRadius = 4.0
        viewWeekButton.layer.borderColor =   isClickable == true ? UIColor.black.cgColor : UIColor.lightGray.cgColor
        viewWeekButton.setTitleColor(isClickable == true ? UIColor.black : UIColor.lightGray, for: .normal)
        viewWeekButton.layer.borderWidth = 1.0
        timeStatusLabel.text = displayText
    }
    
    @IBAction func viewButtonTapped(_ sender: UIButton) {
        delegate?.viewWeekButtonTapped()
    }
    
}
