//
//  OutletContactTableViewCell.swift
//  TheLibraryClub
//
//  Created by Phaninder on 20/01/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import UIKit
import SDWebImage

protocol OutletContactTableViewCellDelegate {
    
    func showDialPad()
    
    func navigateToWebView(forTag tag: Int)
    
    func showBookingScreen()
}

class OutletContactTableViewCell: UITableViewCell {

    @IBOutlet weak var contactUsView: UIView!
    @IBOutlet weak var phoneNumberLabel: UILabel!
    @IBOutlet weak var websiteLabel: UILabel!
    @IBOutlet weak var instaLabel: UILabel!
    @IBOutlet weak var facebookLabel: UILabel!
    @IBOutlet weak var bookButton: UIButton!
    
    var delegate: OutletContactTableViewCellDelegate?
    
    class func cellIdentifier() -> String {
        return "OutletContactTableViewCell"
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.contactUsView.layer.shadowOffset = CGSize(width: 0, height: 1)
        self.contactUsView.layer.shadowColor = UIColor.black.cgColor
        self.contactUsView.layer.shadowRadius = 1
        self.contactUsView.layer.shadowOpacity = 0.2
        self.contactUsView.layer.masksToBounds = false
        self.contactUsView.layer.rasterizationScale = UIScreen.main.scale
        self.contactUsView.clipsToBounds = false
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
//        bookButton.layer.cornerRadius = 5.0
//        bookButton.layer.borderColor = UIColor.black.cgColor
//        bookButton.layer.borderWidth = 1.0
    }
    
    func configureCellWithOutletDetails(_ outlet: OutletDescription) {
      //  bookButton.isHidden = !outlet.allowBooking
        phoneNumberLabel.text = outlet.phone
        websiteLabel.text = outlet.websiteDisplay
        instaLabel.text = outlet.instagram
        facebookLabel.text = outlet.facebook
    }
    
    @IBAction func showDialPad(_ sender: UIButton) {
        delegate?.showDialPad()
    }
    
    @IBAction func navigateToWebView(_ sender: UIButton) {
        delegate?.navigateToWebView(forTag: sender.tag)
    }
    
    @IBAction func bookButtonTapped(_ sender: UIButton) {
        delegate?.showBookingScreen()
    }
    
}
