//
//  OutletHeaderTableViewCell.swift
//  TheLibraryClub
//
//  Created by Phaninder Kumar on 19/04/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import UIKit

class OutletHeaderTableViewCell: UITableViewCell {

    @IBOutlet weak var btnReservation: UIButton!
    @IBOutlet weak var outletImageView: UIImageView!
    @IBOutlet weak var constraintHeight: NSLayoutConstraint!
    
    class func cellIdentifier() -> String {
        return "OutletHeaderTableViewCell"
    }

    override func awakeFromNib() {
        if btnReservation != nil{
            btnReservation.layer.cornerRadius = 5
            btnReservation.clipsToBounds = true
        }
    }
    
}
