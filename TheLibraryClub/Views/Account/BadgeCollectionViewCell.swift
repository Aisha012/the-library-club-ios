//
//  BadgeCollectionViewCell.swift
//  TheLibraryClub
//
//  Created by Phaninder on 08/02/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import UIKit

class BadgeCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var badgeImageView: UIImageView!
    @IBOutlet weak var badgeTitle: UILabel!
    @IBOutlet weak var badgeDate: UILabel!
    
    class func cellIdentifier() -> String {
        return "BadgeCollectionViewCell"
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.layer.shadowOffset = CGSize(width: 4, height: 4)
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowRadius = 1
        self.layer.cornerRadius = 8
        self.layer.borderColor = UIColor.lightGray.cgColor
        self.layer.borderWidth = 0.5
        self.layer.shadowOpacity = 0.1
        self.layer.masksToBounds = false
        self.layer.rasterizationScale = UIScreen.main.scale
        self.clipsToBounds = false
    }

    func configureCell(_ badge: Badge) {
        badgeImageView.sd_setShowActivityIndicatorView(true)
        badgeImageView.sd_setIndicatorStyle(.gray)
        badgeImageView.sd_setImage(with: URL(string: badge.picture), completed: nil)
        
        badgeTitle.text = badge.name
        badgeDate.text = badge.date
    }
    
}
