//
//  AccountTableViewCell.swift
//  TheLibraryClub
//
//  Created by Phaninder on 22/01/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import UIKit

class AccountTableViewCell: UITableViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    var cellType: AccountInfoTableViewCellType!
    
    class func cellIdentifier() -> String {
        return "AccountTableViewCell"
    }
    
    func configureCellWithTitle(_ name: String, info: String, cellType: AccountInfoTableViewCellType) {
        self.cellType = cellType
        nameLabel.text = name
        descriptionLabel.text = info
        self.accessoryType = .none
        self.isUserInteractionEnabled = false
        if (cellType == .tappable) {
            self.tintColor = UIColor.red
            self.accessoryType = UITableViewCellAccessoryType.disclosureIndicator
            self.isUserInteractionEnabled = true
        }
    }

}
