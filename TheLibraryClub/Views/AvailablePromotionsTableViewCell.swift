//
//  AvailablePromotionsTableViewCell.swift
//  TheLibraryClub
//
//  Created by Phaninder on 16/03/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import UIKit

class AvailablePromotionsTableViewCell: UITableViewCell {

    @IBOutlet weak var collectionView: UICollectionView!
    
    class func cellIdentifier() -> String {
        return "AvailablePromotionsTableViewCell"
    }
    
}
