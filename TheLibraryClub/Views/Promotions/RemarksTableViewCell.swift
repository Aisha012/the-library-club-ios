//
//  RemarksTableViewCell.swift
//  TheLibraryClub
//
//  Created by Phaninder Kumar on 17/04/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import UIKit

protocol RemarksTableViewCellDelegate {
    func anniversarySelected()
    func birthdaySelected()
    func allergiesSelected()
    func othersSelected()
}

class RemarksTableViewCell: UITableViewCell {

    @IBOutlet weak var anniversaryCheckBox: UIImageView!
    @IBOutlet weak var birthdayCheckBox: UIImageView!
    @IBOutlet weak var othersCheckBox: UIImageView!
    @IBOutlet weak var allergicCheckBox: UIImageView!
    @IBOutlet weak var othersTextField: UITextField!
    @IBOutlet weak var allergiesTextField: UITextField!
    
    var delegate: RemarksTableViewCellDelegate?
    
    class func cellIdentifier() -> String {
        return "RemarksTableViewCell"
    }
    
    func configureCell(booking: Booking, index: Int) {
        allergiesTextField.tag = index
        othersTextField.tag = index + 1
        anniversaryCheckBox.image = UIImage(named: booking.isAnniversarySelected ? "checkbox_green" : "checkbox_black")
        birthdayCheckBox.image = UIImage(named: booking.isBirthdaySelected ? "checkbox_green" : "checkbox_black")
        allergicCheckBox.image = UIImage(named: booking.isAllergicSelected ? "checkbox_green" : "checkbox_black")
        othersCheckBox.image = UIImage(named: booking.isOthersSelected ? "checkbox_green" : "checkbox_black")
    }
    
    @IBAction func anniversaryButtonTapped(_ sender: UIButton) {
        delegate?.anniversarySelected()
    }
    
    @IBAction func birthdayButtonTapped(_ sender: UIButton) {
        delegate?.birthdaySelected()
    }
    
    @IBAction func othersButtonTapped(_ sender: UIButton) {
        delegate?.othersSelected()
    }
    
    @IBAction func allergicButtonTapped(_ sender: UIButton) {
        delegate?.allergiesSelected()
    }
    
}
