//
//  PromotionDetailHeaderTableViewCell.swift
//  TheLibraryClub
//
//  Created by Phaninder on 21/01/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import UIKit
import SDWebImage

protocol PromotionDetailHeaderTableViewCellDelegate {
    
    func showTheBookingScreen()
    
}
class PromotionDetailHeaderTableViewCell: UITableViewCell {

    
    @IBOutlet weak var promotionImageView: UIImageView!
    @IBOutlet weak var detailsLabel: UILabel!
    @IBOutlet weak var endDateLabel: UILabel!
    @IBOutlet weak var bookButton: UIButton!
    
    var delegate: PromotionDetailHeaderTableViewCellDelegate?
    
    class func cellIdentifier() -> String {
        return "PromotionDetailHeaderTableViewCell"
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        bookButton.layer.cornerRadius = 5.0
//        bookButton.layer.borderColor = UIColor.black.cgColor
//        bookButton.layer.borderWidth = 1.0
    }

    func configureCellWithPromo(_ promo: PromotionDescription) {
        promotionImageView.sd_setShowActivityIndicatorView(true)
        promotionImageView.sd_setIndicatorStyle(.gray)
        promotionImageView.sd_setImage(with: URL(string: promo.imageURL), completed: nil)
        detailsLabel.text = promo.promoDesc
        endDateLabel.text = "Valid Till: ".localized + promo.endDate
    }
    
    @IBAction func bookButtonTapped(_ sender: UIButton) {
        delegate?.showTheBookingScreen()
    }
}
