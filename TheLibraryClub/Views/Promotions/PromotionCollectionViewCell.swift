//
//  PromotionCollectionViewCell.swift
//  TheLibraryClub
//
//  Created by Phaninder on 19/01/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import UIKit

protocol PromotionCollectionViewCellDelegate {
    
    func viewButtonTappedAtIndex(_ index: Int)
}

class PromotionCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var promotionImageView: UIImageView!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var viewMoreButton: UIButton!
    
    var delegate: PromotionCollectionViewCellDelegate?
    
    class func cellIdentifier() -> String {
        return "PromotionCollectionViewCell"
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.layer.shadowOffset = CGSize(width: 4, height: 4)
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowRadius = 1
        self.layer.cornerRadius = 8
        self.layer.borderColor = UIColor.lightGray.cgColor
        self.layer.borderWidth = 0.5
        self.layer.shadowOpacity = 0.1
        self.layer.masksToBounds = false
        self.layer.rasterizationScale = UIScreen.main.scale
        self.clipsToBounds = false
    }

    func configureCellWithPromotion(_ promotion: Promotion, atIndex index: Int) {        
        promotionImageView.sd_setShowActivityIndicatorView(true)
        promotionImageView.sd_setIndicatorStyle(.gray)
        promotionImageView.sd_setImage(with: URL(string: promotion.imageURL), completed: nil)
        descriptionLabel.text = promotion.promoDesc
        viewMoreButton.tag = index
    }

    @IBAction func viewMoreButtonTapped(_ sender: UIButton) {
        delegate?.viewButtonTappedAtIndex(sender.tag)
    }
    
}
