//
//  LastOrderContentTableViewCell.swift
//  TheLibraryClub
//
//  Created by Phaninder on 05/02/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import UIKit

protocol LastOrderContentTableViewCellDelegate {
    func showMoreOrders()
}

class LastOrderContentTableViewCell: UITableViewCell {

    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var pointsLabel: UILabel!
    @IBOutlet weak var amountLabel: UILabel!
    @IBOutlet weak var pointsAmountLabel: UILabel!
    @IBOutlet weak var balanceLabel: UILabel!
    @IBOutlet weak var receiptLabel: UILabel!
    @IBOutlet weak var stackView: UIStackView!
    
    @IBOutlet weak var viewMoreButton: UIButton!
    
    var delegate: LastOrderContentTableViewCellDelegate?
    
    class func cellIdentifier() -> String {
        return "LastOrderContentTableViewCell"
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        let buttonTitleStr = Utilities.getUnderLineTextWithFontFamily("Roboto-Light", ofFontSize: 15.0, andText: "View More")
        viewMoreButton.setAttributedTitle(buttonTitleStr, for: .normal)
    }

    func configureCell(withOrder order: Order, index: Int) {
        let isEven = index % 2 == 0
        dateLabel.text = order.transactionTime
        var sign = "+"
        if order.type != "Add" {
            sign = "-"
        }
        pointsLabel.text = sign + order.point
        balanceLabel.text = order.balance
        amountLabel.text = "฿" + order.receiptAmount
        receiptLabel.text = order.receiptID
        pointsAmountLabel.text = order.point_amount
        
        dateLabel.backgroundColor = isEven ? UIColor.white : UIColor.groupTableViewBackground
        pointsLabel.backgroundColor = isEven ? UIColor.white : UIColor.groupTableViewBackground
        amountLabel.backgroundColor = isEven ? UIColor.white : UIColor.groupTableViewBackground
        balanceLabel.backgroundColor = isEven ? UIColor.white : UIColor.groupTableViewBackground
        receiptLabel.backgroundColor = isEven ? UIColor.white : UIColor.groupTableViewBackground
        pointsAmountLabel.backgroundColor = isEven ? UIColor.white : UIColor.groupTableViewBackground
    }
    
    @IBAction func viewMoreButtonTapped(_ sender: UIButton) {
        delegate?.showMoreOrders()
    }
    
}
