//
//  OutletDualTableViewCell.swift
//  TheLibraryClub
//
//  Created by Phaninder on 29/01/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import UIKit


protocol OutletDualTableViewCellDelegate {
    func viewMoreOutletsButtonTapped()
    func outletOneButtonTapped()
    func outletTwoButtonTapped()
}

class OutletDualTableViewCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var outletOnePlaceholderView: UIView!
    @IBOutlet weak var outletTwoPlaceholderView: UIView!
    @IBOutlet weak var outletOneImageView: UIImageView!
    @IBOutlet weak var outletTwoImageView: UIImageView!
    @IBOutlet weak var OutletOneNameLabel: UILabel!
    @IBOutlet weak var OutletTwoNameLabel: UILabel!
    @IBOutlet weak var OutletOneAddressLabel: UILabel!
    @IBOutlet weak var OutletTwoAddressLabel: UILabel!
    @IBOutlet weak var viewMoreButton: UIButton!
    @IBOutlet weak var outletOneButton: UIButton!
    @IBOutlet weak var outletTwoButton: UIButton!
    
    var delegate: OutletDualTableViewCellDelegate?
    
    class func cellIdentifier() -> String {
        return "OutletDualTableViewCell"
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        let buttonTitleStr = Utilities.getUnderLineTextWithFontFamily("Roboto-Light", ofFontSize: 15.0, andText: "View More")
        viewMoreButton.setAttributedTitle(buttonTitleStr, for: .normal)
    }
    
    func configureCellsWithOutlets(_ outlets: [Outlet]) -> CGFloat {
        var height: CGFloat = 238.0
        if outlets.count <= 2 {
            viewMoreButton.isHidden = true
        }
        
            let outlet1 = outlets[0]
            outletOneImageView.sd_setShowActivityIndicatorView(true)
            outletOneImageView.sd_setIndicatorStyle(.gray)
            outletOneImageView.sd_setImage(with: URL(string: outlet1.imageURL), completed: nil)
            
            OutletOneNameLabel.text = outlet1.name
            OutletOneAddressLabel.text = outlet1.address
            
            if outlets.count == 1 {
                height -= 52.0
            }

            let outlet2 = outlets[1]
            outletTwoImageView.sd_setShowActivityIndicatorView(true)
            outletTwoImageView.sd_setIndicatorStyle(.gray)
            outletTwoImageView.sd_setImage(with: URL(string: outlet2.imageURL), completed: nil)
            
            OutletTwoNameLabel.text = outlet2.name
            OutletTwoAddressLabel.text = outlet2.address
            if outlets.count == 2 {
                height -= 52.0
            }
        
        return height
    }
    
    
    @IBAction func viewMoreButtonTapped(_ sender: UIButton) {
        delegate?.viewMoreOutletsButtonTapped()
    }
    
    @IBAction func outletOneTapped(_ sender: UIButton) {
        delegate?.outletOneButtonTapped()
    }
    
    @IBAction func outletTwoTapped(_ sender: UIButton) {
        delegate?.outletTwoButtonTapped()
    }
    
}
