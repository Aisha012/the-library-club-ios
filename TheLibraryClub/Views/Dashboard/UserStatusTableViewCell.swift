//
//  UserStatusTableViewCell.swift
//  TheLibraryClub
//
//  Created by Phaninder on 22/01/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import UIKit

class UserStatusTableViewCell: UITableViewCell {

    @IBOutlet weak var memberLabel: UILabel!
    @IBOutlet weak var pointsLabel: UILabel!
    @IBOutlet weak var statusImageView: UIImageView!
    weak var controller:DashboardViewController?
    class func cellIdentifier() -> String {
        return "UserStatusTableViewCell"
    }
    
    func configureCell(_ customerInfo: CustomerInfoPagination) {
        var membership = ""
        var imageName = ""
        
        switch customerInfo.group {
        case "7":
            membership = "Gold"
            imageName = "gold"
            let color1 = #colorLiteral(red: 0.9058823529, green: 0.6941176471, blue: 0.262745098, alpha: 1)
            let color2 = #colorLiteral(red: 0.9568627451, green: 0.9058823529, blue: 0.4784313725, alpha: 1)
            let color3 = #colorLiteral(red: 0.8705882353, green: 0.6941176471, blue: 0.3960784314, alpha: 1)
            self.setGradient(gradientcolor: [color1.cgColor, color2.cgColor,color3.cgColor])

        case "5":
            membership = "Silver"
            imageName = "silver"
            let color1 = #colorLiteral(red: 0.7843137255, green: 0.7882352941, blue: 0.7960784314, alpha: 1)
            let color2 = #colorLiteral(red: 0.9215686275, green: 0.9254901961, blue: 0.9333333333, alpha: 1)
            let color3 = #colorLiteral(red: 0.8039215686, green: 0.8039215686, blue: 0.8117647059, alpha: 1)
            self.setGradient(gradientcolor: [color1.cgColor, color2.cgColor,color3.cgColor])
        case "1":
            membership = "Black"
            imageName = "black"
           
            
        default:
            membership = "Platinum"
            imageName = "platinum"
            controller?.vwHeader.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
            controller?.view.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)


        }
        memberLabel.text = membership
        pointsLabel.text = customerInfo.rewardPoints
        let img = UIImage(named: imageName)
        statusImageView.image = img
    }
    

    func setGradient(gradientcolor:[Any]){
        if !controller!.hasGradientSet{
            let gradientLayer = CAGradientLayer()
            let backgradientLayer = CAGradientLayer()
            gradientLayer.colors = gradientcolor
            gradientLayer.locations = [0.0, 0.45, 0.85,  1.0]
            gradientLayer.startPoint = CGPoint.init(x: 0, y: 0)
            gradientLayer.endPoint = CGPoint.init(x: 1, y: 0)
            gradientLayer.frame = controller!.vwHeader.bounds
            
            backgradientLayer.colors = gradientcolor
            backgradientLayer.locations = [0.0, 0.45, 0.85,  1.0]
            backgradientLayer.startPoint = CGPoint.init(x: 0, y: 0)
            backgradientLayer.endPoint = CGPoint.init(x: 1, y: 0)
            controller?.vwHeader.layer.insertSublayer(gradientLayer, at: 0)
            let window: UIWindow? = UIApplication.shared.keyWindow
            controller!.gradientView = UIView()
             controller!.gradientView!.backgroundColor = UIColor.clear
             controller!.gradientView!.translatesAutoresizingMaskIntoConstraints = false
             controller!.gradientView!.frame = CGRect(x: 0, y: 0, width: (window?.frame.width)!, height: controller!.view.frame.height * 0.07)
            var yVal:CGFloat = 15.0
            if UIScreen.main.bounds.size.height == 568{
                yVal = 10.0
            }
            backgradientLayer.frame = CGRect(x:0, y:-yVal, width:(window?.frame.size.width)!, height:controller!.view.frame.height * 0.06)

             controller!.gradientView!.layer.addSublayer(backgradientLayer)
            window?.addSubview( controller!.gradientView!)
            controller!.hasGradientSet = true
            controller?.vwHeader.backgroundColor = UIColor.clear
            controller?.view.backgroundColor = UIColor.white
        }
    }
    
    
}
