//
//  NotificationTableViewCell.swift
//  TheLibraryClub
//
//  Created by Phaninder on 30/01/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import UIKit

class NotificationTableViewCell: UITableViewCell {
    
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    
    class func cellIdentifier() -> String {
        return "NotificationTableViewCell"
    }
    

}
