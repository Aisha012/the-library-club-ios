//
//  ViewMoreOutletCollectionViewCell.swift
//  TheLibraryClub
//
//  Created by Phaninder on 04/04/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import UIKit

class ViewMoreOutletCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var outletPlaceholderView: UIView!
    @IBOutlet weak var outletImageView: UIImageView!
    @IBOutlet weak var outletNameLabel: UILabel!
    @IBOutlet weak var outletAddressLabel: UILabel!
    
    class func cellIdentifier() -> String {
        return "ViewMoreOutletCollectionViewCell"
    }
    
    func configureCellWithOutlet(_ outlet: Outlet) {
        outletImageView.sd_setShowActivityIndicatorView(true)
        outletImageView.sd_setIndicatorStyle(.gray)
        outletImageView.sd_setImage(with: URL(string: outlet.imageURL), completed: nil)
        
        outletNameLabel.text = outlet.name
        outletAddressLabel.text = outlet.address
    }
}
