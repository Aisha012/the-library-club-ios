//
//  LastOrdersEmptyTableViewCell.swift
//  TheLibraryClub
//
//  Created by Phaninder on 05/02/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import UIKit

class LastOrdersEmptyTableViewCell: UITableViewCell {

    class func cellIdentifier() -> String {
        return "LastOrdersEmptyTableViewCell"
    }

}
