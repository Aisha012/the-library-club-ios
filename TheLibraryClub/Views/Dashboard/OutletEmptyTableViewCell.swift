//
//  OutletEmptyTableViewCell.swift
//  TheLibraryClub
//
//  Created by Phaninder on 29/01/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import UIKit

class OutletEmptyTableViewCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var messageLabel: UILabel!
    
    class func cellIdentifier() -> String {
        return "OutletEmptyTableViewCell"
    }
    
}
