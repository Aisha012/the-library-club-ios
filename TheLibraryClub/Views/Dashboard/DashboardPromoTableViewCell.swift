//
//  DashboardPromoTableViewCell.swift
//  TheLibraryClub
//
//  Created by Phaninder on 22/01/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import UIKit

//protocol DashboardPromoTableViewCellDelegate {
//    func viewMorePromosButtonTapped()
//}

class DashboardPromoTableViewCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var viewMoreButton: UIButton!
    @IBOutlet weak var emptyPlaceholderLabel: UILabel!
    
    @IBOutlet weak var promoStackView: UIStackView!
    @IBOutlet weak var stackViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var promotionOnePlaceholderView: UIView!
    
    @IBOutlet weak var promotionTwoPlaceholderView: UIView!
    
    @IBOutlet weak var promotionOneView: UIView!
    @IBOutlet weak var promotionTwoView: UIView!
    @IBOutlet weak var promotionOneImageView: UIImageView!
    @IBOutlet weak var promotionTwoImageView: UIImageView!
    @IBOutlet weak var promotionOneDescLabel: UILabel!
    @IBOutlet weak var promotionTwoDescLabel: UILabel!
    
//    var delegate: DashboardPromoTableViewCellDelegate?
    
    class func cellIdentifier() -> String {
        return "DashboardPromoTableViewCell"
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        if let _ = promotionOnePlaceholderView {
            self.promotionOneView.layer.shadowOffset = CGSize(width: 4, height: 4)
            self.promotionOneView.layer.shadowColor = UIColor.black.cgColor
            self.promotionOneView.layer.shadowRadius = 1
            self.promotionOneView.layer.cornerRadius = 8
            self.promotionOneView.layer.borderColor = UIColor.lightGray.cgColor
            self.promotionOneView.layer.borderWidth = 0.5
            self.promotionOneView.layer.shadowOpacity = 0.1
            self.promotionOneView.layer.masksToBounds = false
            self.promotionOneView.layer.rasterizationScale = UIScreen.main.scale
            self.promotionOneView.clipsToBounds = false
        }
        
        if let _ = promotionTwoPlaceholderView {
            self.promotionTwoView.layer.shadowOffset = CGSize(width: 4, height: 4)
            self.promotionTwoView.layer.shadowColor = UIColor.black.cgColor
            self.promotionTwoView.layer.shadowRadius = 1
            self.promotionTwoView.layer.cornerRadius = 8
            self.promotionTwoView.layer.borderColor = UIColor.lightGray.cgColor
            self.promotionTwoView.layer.borderWidth = 0.5
            self.promotionTwoView.layer.shadowOpacity = 0.1
            self.promotionTwoView.layer.masksToBounds = false
            self.promotionTwoView.layer.rasterizationScale = UIScreen.main.scale
            self.promotionTwoView.clipsToBounds = false
        }
        
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        let buttonTitleStr = Utilities.getUnderLineTextWithFontFamily("Roboto-Light", ofFontSize: 15.0, andText: "View More")
        viewMoreButton.setAttributedTitle(buttonTitleStr, for: .normal)
    }
    
    func configureCellsWithPromos(_ promotions: [Promotion]) -> CGFloat {
        var height: CGFloat = 328.0
        if promotions.count <= 2 {
            viewMoreButton.isHidden = true
        }
        
        if promotions.count > 1 {
            let promotion = promotions[1]
            promotionTwoImageView.sd_setShowActivityIndicatorView(true)
            promotionTwoImageView.sd_setIndicatorStyle(.gray)
            promotionTwoImageView.sd_setImage(with: URL(string: promotion.imageURL), completed: nil)
            
            promotionTwoDescLabel.text = promotion.promoDesc
            let promoOneLabelHeight = calculateHeightForText(promotions[0].promoDesc, inLabel: promotionOneDescLabel)
            let promoTwoLabelHeight = calculateHeightForText(promotion.promoDesc, inLabel: promotionTwoDescLabel)
            let maxHeightOfLabel = promoOneLabelHeight >= promoTwoLabelHeight ? promoOneLabelHeight : promoTwoLabelHeight
            stackViewHeightConstraint.constant = 138 + maxHeightOfLabel
            let heightToBeAdded: CGFloat = promotions.count == 2 ? 72.0 : 102.0
            height = stackViewHeightConstraint.constant + heightToBeAdded
        } else {
            removePromotionView(view: promotionTwoPlaceholderView)
            if let placeHolderView = promotionOnePlaceholderView {
                let widthConstraint = NSLayoutConstraint (item: placeHolderView,
                                                          attribute: NSLayoutAttribute.width,
                                                          relatedBy: NSLayoutRelation.equal,
                                                          toItem: nil,
                                                          attribute: NSLayoutAttribute.notAnAttribute,
                                                          multiplier: 1,
                                                          constant: (ScreenWidth/2 - 24))
                self.addConstraint(widthConstraint)
                
                let xConstraint = NSLayoutConstraint(item: placeHolderView,
                                                     attribute: .centerX,
                                                     relatedBy: .equal,
                                                     toItem: self,
                                                     attribute: .centerX,
                                                     multiplier: 1,
                                                     constant: 0)
                self.addConstraint(xConstraint)
            }
        }

        if promotions.count > 0 {
            let promotion = promotions[0]
            promotionOneImageView.sd_setShowActivityIndicatorView(true)
            promotionOneImageView.sd_setIndicatorStyle(.gray)
            promotionOneImageView.sd_setImage(with: URL(string: promotion.imageURL), completed: nil)
            
            promotionOneDescLabel.text = promotion.promoDesc
            if promotions.count == 1 {
                let labelHeight = calculateHeightForText(promotion.promoDesc, inLabel: promotionOneDescLabel)
                stackViewHeightConstraint.constant = 138 + labelHeight
                height = stackViewHeightConstraint.constant + 72.0
            }
        } else {
            removePromotionView(view: promotionOnePlaceholderView)
            removePromotionView(view: promotionTwoPlaceholderView)
            height = 86.0
        }
        
        return height
    }
    
    func removePromotionView(view: UIView?) {
        if let promotionView = view {
            if let _ = promoStackView.arrangedSubviews.index(of: promotionView) {
                promoStackView.removeArrangedSubview(promotionView)
                promotionView.removeFromSuperview()
            }
        }
    }

    func calculateHeightForText(_ text: String, inLabel label: UILabel) -> CGFloat {
        let titleParagraphStyle = NSMutableParagraphStyle()
        titleParagraphStyle.alignment = .center
        
        
        let attrText = [
            NSAttributedStringKey.font: UIFont.init(name: "Roboto-Light", size: 15.0)!,
            NSAttributedStringKey.paragraphStyle: titleParagraphStyle
        ]
        let attrRect : CGRect = text.boundingRect(with: CGSize(width: (ScreenWidth/2 - 40), height: CGFloat.greatestFiniteMagnitude), options: .usesLineFragmentOrigin, attributes: attrText, context: nil)
        return attrRect.height
    }
    
    @IBAction func viewMoreButtonTapped(_ sender: UIButton) {
//        delegate?.viewMorePromosButtonTapped()
    }
    
}
