//
//  OutletSingleTableViewCell.swift
//  TheLibraryClub
//
//  Created by Phaninder on 29/01/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import UIKit

protocol OutletSingleTableViewCellDelegate {
    func outletButtonTapped()
}

class OutletSingleTableViewCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var outletPlaceholderView: UIView!
    @IBOutlet weak var outletImageView: UIImageView!
    @IBOutlet weak var outletNameLabel: UILabel!
    @IBOutlet weak var outletAddressLabel: UILabel!
    
    var delegate: OutletSingleTableViewCellDelegate?
    
    class func cellIdentifier() -> String {
        return "OutletSingleTableViewCell"
    }
    
    func configureCellsWithOutlets(_ outlets: [Outlet]) -> CGFloat {
        let widthConstraint = NSLayoutConstraint (item: outletPlaceholderView,
                                                  attribute: NSLayoutAttribute.width,
                                                  relatedBy: NSLayoutRelation.equal,
                                                  toItem: nil,
                                                  attribute: NSLayoutAttribute.notAnAttribute,
                                                  multiplier: 1,
                                                  constant: (ScreenWidth/2 - 16))
        self.addConstraint(widthConstraint)
        
        let xConstraint = NSLayoutConstraint(item: outletPlaceholderView,
                                             attribute: .centerX,
                                             relatedBy: .equal,
                                             toItem: self,
                                             attribute: .centerX,
                                             multiplier: 1,
                                             constant: 0)
        self.addConstraint(xConstraint)

        let outlet = outlets[0]
        outletImageView.sd_setShowActivityIndicatorView(true)
        outletImageView.sd_setIndicatorStyle(.gray)
        outletImageView.sd_setImage(with: URL(string: outlet.imageURL), completed: nil)
        
        outletNameLabel.text = outlet.name
        outletAddressLabel.text = outlet.address
        return 186.0
    }

    @IBAction func outletButtonTapped(_ sender: UIButton) {
        delegate?.outletButtonTapped()
    }
}
