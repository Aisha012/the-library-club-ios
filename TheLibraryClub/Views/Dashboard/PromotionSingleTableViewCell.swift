//
//  PromotionSingleTableViewCell.swift
//  TheLibraryClub
//
//  Created by Phaninder on 29/01/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import UIKit

protocol PromotionSingleTableViewCellDelegate {
    func promotionButtonTapped()
}
protocol badgeSingleTableViewCellDelegate {
    func badgeButtonTapped()
}

class PromotionSingleTableViewCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var stackViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var promotionPlaceholderView: UIView!
    @IBOutlet weak var promotionView: UIView!
    @IBOutlet weak var promotionImageView: UIImageView!
    @IBOutlet weak var promotionDescLabel: UILabel!
    @IBOutlet weak var promotionButton: UIButton!
    
    var delegate: PromotionSingleTableViewCellDelegate?
    var badgedelegate: badgeSingleTableViewCellDelegate?

    class func cellIdentifier() -> String {
        return "PromotionSingleTableViewCell"
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.promotionView.layer.shadowOffset = CGSize(width: 4, height: 4)
        self.promotionView.layer.shadowColor = UIColor.black.cgColor
        self.promotionView.layer.shadowRadius = 1
        self.promotionView.layer.cornerRadius = 8
        self.promotionView.layer.borderColor = UIColor.lightGray.cgColor
        self.promotionView.layer.borderWidth = 0.5
        self.promotionView.layer.shadowOpacity = 0.1
        self.promotionView.layer.masksToBounds = false
        self.promotionView.layer.rasterizationScale = UIScreen.main.scale
        self.promotionView.clipsToBounds = false
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func configureCellsWithPromos(_ promotions: [Promotion]) -> CGFloat {
        
        let widthConstraint = NSLayoutConstraint (item: promotionPlaceholderView,
                                                  attribute: NSLayoutAttribute.width,
                                                  relatedBy: NSLayoutRelation.equal,
                                                  toItem: nil,
                                                  attribute: NSLayoutAttribute.notAnAttribute,
                                                  multiplier: 1,
                                                  constant: (ScreenWidth/2 - 16))
        self.addConstraint(widthConstraint)
        
        let xConstraint = NSLayoutConstraint(item: promotionPlaceholderView,
                                             attribute: .centerX,
                                             relatedBy: .equal,
                                             toItem: self,
                                             attribute: .centerX,
                                             multiplier: 1,
                                             constant: 0)
        self.addConstraint(xConstraint)
        let promotion = promotions[0]
        promotionImageView.sd_setShowActivityIndicatorView(true)
        promotionImageView.sd_setIndicatorStyle(.gray)
        promotionImageView.sd_setImage(with: URL(string: promotion.imageURL), completed: nil)
        
        promotionDescLabel.text = promotion.promoDesc
        let labelHeight = calculateHeightForText(promotion.promoDesc, inLabel: promotionDescLabel)
        stackViewHeightConstraint.constant = 138 + labelHeight
        let height = stackViewHeightConstraint.constant + 72.0

        return height
    }
    
    func configureCellsWithBadge(_ promotions: [Badge]) -> CGFloat {
        
        let widthConstraint = NSLayoutConstraint (item: promotionPlaceholderView,
                                                  attribute: NSLayoutAttribute.width,
                                                  relatedBy: NSLayoutRelation.equal,
                                                  toItem: nil,
                                                  attribute: NSLayoutAttribute.notAnAttribute,
                                                  multiplier: 1,
                                                  constant: (ScreenWidth/2 - 16))
        self.addConstraint(widthConstraint)
        
        let xConstraint = NSLayoutConstraint(item: promotionPlaceholderView,
                                             attribute: .centerX,
                                             relatedBy: .equal,
                                             toItem: self,
                                             attribute: .centerX,
                                             multiplier: 1,
                                             constant: 0)
        self.addConstraint(xConstraint)
        let promotion = promotions[0]
        promotionImageView.sd_setShowActivityIndicatorView(true)
        promotionImageView.sd_setIndicatorStyle(.gray)
        promotionImageView.sd_setImage(with: URL(string: promotion.picture), completed: nil)
        
        promotionDescLabel.text = promotion.name
        let labelHeight = calculateHeightForText(promotion.name, inLabel: promotionDescLabel)
        stackViewHeightConstraint.constant = 138 + labelHeight
        let height = stackViewHeightConstraint.constant + 72.0
        
        return height
    }
    
    
    func calculateHeightForText(_ text: String, inLabel label: UILabel) -> CGFloat {
        let titleParagraphStyle = NSMutableParagraphStyle()
        titleParagraphStyle.alignment = .center
        
        
        let attrText = [
            NSAttributedStringKey.font: UIFont.init(name: "Roboto-Light", size: 15.0)!,
            NSAttributedStringKey.paragraphStyle: titleParagraphStyle
        ]
        let attrRect : CGRect = text.boundingRect(with: CGSize(width: (ScreenWidth/2 - 40), height: CGFloat.greatestFiniteMagnitude), options: .usesLineFragmentOrigin, attributes: attrText, context: nil)
        return attrRect.height
    }
    
    @IBAction func promotionButtonTapped(_ sender: UIButton) {
        delegate?.promotionButtonTapped()
    }
    @IBAction func badgeButtonTapped(_ sender: UIButton) {
        badgedelegate?.badgeButtonTapped()
    }
    
}
