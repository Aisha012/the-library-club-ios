//
//  BadgesTableViewCell.swift
//  TheLibraryClub
//
//  Created by Namespace  on 12/07/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import UIKit


    protocol BadgesTableViewCellDelegate {
        func viewMorePromosButtonTapped()
        func promotiontOneButtonTapped()
        func promotionTwoButtonTapped()
    }

class BadgesTableViewCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var viewMoreButton: UIButton!
    
    @IBOutlet weak var stackViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var promotionOnePlaceholderView: UIView!
    
    @IBOutlet weak var promotionTwoPlaceholderView: UIView!
    
    @IBOutlet weak var promotionOneView: UIView!
    @IBOutlet weak var promotionTwoView: UIView!
    @IBOutlet weak var promotionOneImageView: UIImageView!
    @IBOutlet weak var promotionTwoImageView: UIImageView!
    @IBOutlet weak var promotionOneDescLabel: UILabel!
    @IBOutlet weak var promotionTwoDescLabel: UILabel!
    @IBOutlet weak var promotionOneButton: UIButton!
    @IBOutlet weak var promotionTwoButton: UIButton!
    
    
    var delegate: BadgesTableViewCellDelegate?
    
    class func cellIdentifier() -> String {
        return "badgesDualTableViewCell"
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.promotionOneView.layer.shadowOffset = CGSize(width: 4, height: 4)
        self.promotionOneView.layer.shadowColor = UIColor.black.cgColor
        self.promotionOneView.layer.shadowRadius = 1
        self.promotionOneView.layer.cornerRadius = 8
        self.promotionOneView.layer.borderColor = UIColor.lightGray.cgColor
        self.promotionOneView.layer.borderWidth = 0.5
        self.promotionOneView.layer.shadowOpacity = 0.1
        self.promotionOneView.layer.masksToBounds = false
        self.promotionOneView.layer.rasterizationScale = UIScreen.main.scale
        self.promotionOneView.clipsToBounds = false
        
        self.promotionTwoView.layer.shadowOffset = CGSize(width: 4, height: 4)
        self.promotionTwoView.layer.shadowColor = UIColor.black.cgColor
        self.promotionTwoView.layer.shadowRadius = 1
        self.promotionTwoView.layer.cornerRadius = 8
        self.promotionTwoView.layer.borderColor = UIColor.lightGray.cgColor
        self.promotionTwoView.layer.borderWidth = 0.5
        self.promotionTwoView.layer.shadowOpacity = 0.1
        self.promotionTwoView.layer.masksToBounds = false
        self.promotionTwoView.layer.rasterizationScale = UIScreen.main.scale
        self.promotionTwoView.clipsToBounds = false
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        let buttonTitleStr = Utilities.getUnderLineTextWithFontFamily("Roboto-Light", ofFontSize: 15.0, andText: "View More")
        viewMoreButton.setAttributedTitle(buttonTitleStr, for: .normal)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func configureCellsWithPromos(_ promotions: [Promotion]) -> CGFloat {
        var height: CGFloat = 328.0
        if promotions.count == 2 {
            viewMoreButton.isHidden = true
        }
        
        
        let promotion1 = promotions[0]
        promotionOneImageView.sd_setShowActivityIndicatorView(true)
        promotionOneImageView.sd_setIndicatorStyle(.gray)
        promotionOneImageView.sd_setImage(with: URL(string: promotion1.imageURL), completed: nil)
        
        promotionOneDescLabel.text = promotion1.promoDesc
        
        let promotion2 = promotions[1]
        promotionTwoImageView.sd_setShowActivityIndicatorView(true)
        promotionTwoImageView.sd_setIndicatorStyle(.gray)
        promotionTwoImageView.sd_setImage(with: URL(string: promotion2.imageURL), completed: nil)
        
        promotionTwoDescLabel.text = promotion2.promoDesc
        let promoOneLabelHeight = calculateHeightForText(promotion1.promoDesc, inLabel: promotionOneDescLabel)
        let promoTwoLabelHeight = calculateHeightForText(promotion2.promoDesc, inLabel: promotionTwoDescLabel)
        let maxHeightOfLabel = promoOneLabelHeight >= promoTwoLabelHeight ? promoOneLabelHeight : promoTwoLabelHeight
        stackViewHeightConstraint.constant = 138 + maxHeightOfLabel
        let heightToBeAdded: CGFloat = promotions.count == 2 ? 72.0 : 102.0
        height = stackViewHeightConstraint.constant + heightToBeAdded
        
        
        return height
    }
    
    func calculateHeightForText(_ text: String, inLabel label: UILabel) -> CGFloat {
        let titleParagraphStyle = NSMutableParagraphStyle()
        titleParagraphStyle.alignment = .center
        
        
        let attrText = [
            NSAttributedStringKey.font: UIFont.init(name: "Roboto-Light", size: 15.0)!,
            NSAttributedStringKey.paragraphStyle: titleParagraphStyle
        ]
        let attrRect : CGRect = text.boundingRect(with: CGSize(width: (ScreenWidth/2 - 40), height: CGFloat.greatestFiniteMagnitude), options: .usesLineFragmentOrigin, attributes: attrText, context: nil)
        return attrRect.height
    }
    
    @IBAction func viewMoreButtonTapped(_ sender: UIButton) {
        delegate?.viewMorePromosButtonTapped()
    }
    
    @IBAction func promotionOneTapped(_ sender: UIButton) {
        delegate?.promotiontOneButtonTapped()
    }
    
    @IBAction func promotionTwoTapped(_ sender: UIButton) {
        delegate?.promotionTwoButtonTapped()
    }
}
