//
//  ViewMorePromotionCollectionViewCell.swift
//  TheLibraryClub
//
//  Created by Phaninder on 04/04/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import UIKit

class ViewMorePromotionCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var promotionView: UIView!
    @IBOutlet weak var promotionImageView: UIImageView!
    @IBOutlet weak var promotionDescLabel: UILabel!
    
    //    var delegate: DashboardPromoTableViewCellDelegate?
    
    class func cellIdentifier() -> String {
        return "ViewMorePromotionCollectionViewCell"
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.promotionView.layer.shadowOffset = CGSize(width: 4, height: 4)
        self.promotionView.layer.shadowColor = UIColor.black.cgColor
        self.promotionView.layer.shadowRadius = 1
        self.promotionView.layer.cornerRadius = 8
        self.promotionView.layer.borderColor = UIColor.lightGray.cgColor
        self.promotionView.layer.borderWidth = 0.5
        self.promotionView.layer.shadowOpacity = 0.1
        self.promotionView.layer.masksToBounds = false
        self.promotionView.layer.rasterizationScale = UIScreen.main.scale
        self.promotionView.clipsToBounds = false
    }

    func configureCell(promotion: Promotion) {
        promotionImageView.sd_setShowActivityIndicatorView(true)
        promotionImageView.sd_setIndicatorStyle(.gray)
        promotionImageView.sd_setImage(with: URL(string: promotion.imageURL), completed: nil)
        
        promotionDescLabel.text = promotion.promoDesc
    }
    
}
