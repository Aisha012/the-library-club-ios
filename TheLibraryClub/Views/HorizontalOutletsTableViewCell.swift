//
//  HorizontalOutletsTableViewCell.swift
//  TheLibraryClub
//
//  Created by Phaninder on 22/01/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import UIKit
import SDWebImage

protocol HorizontalOutletsTableViewCellDelegate {
    func viewMoreOultetsButtonTapped()
}

class HorizontalOutletsTableViewCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var outletsStackView: UIStackView!
    @IBOutlet weak var noOutletsPlaceholderLabel: UILabel!
    
    @IBOutlet weak var outletOnePlaceholderView: UIView!
    @IBOutlet weak var outletTwoPlaceholderView: UIView!
    @IBOutlet weak var outletOneImageView: UIImageView!
    @IBOutlet weak var outletTwoImageView: UIImageView!
    @IBOutlet weak var OutletOneNameLabel: UILabel!
    @IBOutlet weak var OutletTwoNameLabel: UILabel!
    @IBOutlet weak var OutletOneAddressLabel: UILabel!
    @IBOutlet weak var OutletTwoAddressLabel: UILabel!
    @IBOutlet weak var viewMoreButton: UIButton!
    
    var delegate: HorizontalOutletsTableViewCellDelegate?
    
    class func cellIdentifier() -> String {
        return "HorizontalOutletsTableViewCell"
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        let buttonTitleStr = Utilities.getUnderLineTextWithFontFamily("Roboto-Light", ofFontSize: 15.0, andText: "View More")
        viewMoreButton.setAttributedTitle(buttonTitleStr, for: .normal)
    }

    func configureCellsWithOutlets(_ outlets: [Outlet]) -> CGFloat {
        var height: CGFloat = 238.0
        if outlets.count <= 2 {
            viewMoreButton.isHidden = true
        }
        
        if outlets.count > 1 {
            let outlet = outlets[1]
            outletTwoImageView.sd_setShowActivityIndicatorView(true)
            outletTwoImageView.sd_setIndicatorStyle(.gray)
            outletTwoImageView.sd_setImage(with: URL(string: outlet.imageURL), completed: nil)
            
            OutletTwoNameLabel.text = outlet.name
            OutletTwoAddressLabel.text = outlet.address
            if outlets.count == 2 {
                height -= 52.0
            }
        } else {
            removeOutletView(view: outletTwoPlaceholderView)
            if let placeHolderView = outletOnePlaceholderView {
                let widthConstraint = NSLayoutConstraint (item: placeHolderView,
                                                          attribute: NSLayoutAttribute.width,
                                                          relatedBy: NSLayoutRelation.equal,
                                                          toItem: nil,
                                                          attribute: NSLayoutAttribute.notAnAttribute,
                                                          multiplier: 1,
                                                          constant: (ScreenWidth/2 - 24))
                self.addConstraint(widthConstraint)
                
                let xConstraint = NSLayoutConstraint(item: placeHolderView,
                                                     attribute: .centerX,
                                                     relatedBy: .equal,
                                                     toItem: self,
                                                     attribute: .centerX,
                                                     multiplier: 1,
                                                     constant: 0)
                self.addConstraint(xConstraint)
            }
        }
        
        if outlets.count > 0 {
            let outlet = outlets[0]
            outletOneImageView.sd_setShowActivityIndicatorView(true)
            outletOneImageView.sd_setIndicatorStyle(.gray)
            outletOneImageView.sd_setImage(with: URL(string: outlet.imageURL), completed: nil)
            
            OutletOneNameLabel.text = outlet.name
            OutletOneAddressLabel.text = outlet.address

            if outlets.count == 1 {
                height -= 52.0
            }
        } else {
            removeOutletView(view: outletOnePlaceholderView)
            removeOutletView(view: outletTwoPlaceholderView)
            height = 86.0
        }
        
        return height
    }
    
    func removeOutletView(view: UIView?) {
        if let outletView = view {
            if let _ = outletsStackView.arrangedSubviews.index(of: outletView) {
                outletsStackView.removeArrangedSubview(outletView)
                outletView.removeFromSuperview()
            }
        }
    }
    
    @IBAction func viewMoreButtonTapped(_ sender: UIButton) {
        delegate?.viewMoreOultetsButtonTapped()
    }
}
