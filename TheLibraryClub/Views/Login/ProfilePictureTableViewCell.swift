//
//  ProfilePictureTableViewCell.swift
//  TheLibraryClub
//
//  Created by Phaninder on 08/02/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import UIKit

protocol ProfilePictureTableViewCellDelegate {
    func showImageOptionsforButton(button: UIButton)
}

class ProfilePictureTableViewCell: UITableViewCell {

    @IBOutlet weak var profileImageView: UIImageView!

    var delegate: ProfilePictureTableViewCellDelegate?

    override func awakeFromNib() {
        profileImageView.layer.masksToBounds = true
        profileImageView.layer.borderWidth = 1.5
        profileImageView.layer.borderColor = UIColor.white.cgColor
        profileImageView.layer.cornerRadius = profileImageView.bounds.width/2
    }
    
    class func cellIdentifier() -> String {
        return "ProfilePictureTableViewCell"
    }
    
    @IBAction func imageButtonTapped(_ sender: UIButton) {
        delegate?.showImageOptionsforButton(button: sender)
    }
}
