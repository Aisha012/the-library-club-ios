//
//  TextFieldTableViewCell.swift
//  TheLibraryClub
//
//  Created by Phaninder on 22/01/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import UIKit

class TextFieldTableViewCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var inputTextField: UITextField!
    
    class func cellIdentifier() -> String {
        return "TextFieldTableViewCell"
    }
    
    func configureCellForRowAtIndex(_ index: Int, withText text: String) {
        inputTextField.tag = index
        titleLabel.text = text
    }

}
