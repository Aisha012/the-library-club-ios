//
//  RegisterButtonTableViewCell.swift
//  TheLibraryClub
//
//  Created by Phaninder on 22/01/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import UIKit

protocol RegisterButtonTableViewCellDelegate {
    func registerButtonTapped()
}

class RegisterButtonTableViewCell: UITableViewCell {
    
    @IBOutlet weak var registerButton: UIButton!
    var delegate: RegisterButtonTableViewCellDelegate?

    class func cellIdentifier() -> String {
        return "RegisterButtonTableViewCell"
    }

    @IBAction func registerButtonTapped(_ sender: Any) {
        delegate?.registerButtonTapped()
    }

}
