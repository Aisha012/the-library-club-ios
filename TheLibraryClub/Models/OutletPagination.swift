//
//  OutletPagination.swift
//  TheLibraryClub
//
//  Created by Phaninder on 18/01/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import Foundation
import Unbox

class OutletPagination: Unboxable {
    
    var outlets: [Outlet] = []
    private let limit = 10
    var currentPageNumber = 1
    var paginationType: PaginationType = .new
    var hasMoreToLoad: Bool
    
    init() {
        outlets = []
        currentPageNumber = 1
        paginationType = .new
        hasMoreToLoad = false
    }
    
    required init(unboxer: Unboxer) throws {
        if let outletsArray = unboxer.dictionary["data"] as? [[String: Any]] {
            for outletDict in outletsArray {
                let outlet: Outlet = try unbox(dictionary: outletDict)
                self.outlets.append(outlet)
            }
        }
        hasMoreToLoad = self.outlets.count == limit
    }
    
    func appendDataFromObject(_ newPaginationObject: OutletPagination) {
        switch self.paginationType {
        case .new, .reload:
            self.outlets = [];
            self.outlets = newPaginationObject.outlets
            self.paginationType = .old
        case .old:
            self.outlets += newPaginationObject.outlets
            self.paginationType = .old
        }
        self.hasMoreToLoad = newPaginationObject.hasMoreToLoad
        self.currentPageNumber += 1
    }
    
}
