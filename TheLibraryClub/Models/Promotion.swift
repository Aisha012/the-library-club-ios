//
//  Promotion.swift
//  TheLibraryClub
//
//  Created by Phaninder on 19/01/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import Foundation
import Unbox

class Promotion: Unboxable {
    
    let id: Int
    let name: String!
    let promoDesc: String!
    let imageURL: String!
    
    
    required init(unboxer: Unboxer) throws {
        id = unboxer.unbox(key: "id") ?? 0
        let promoName = unboxer.unbox(key: "name") ?? ""
        name = promoName
        imageURL = unboxer.unbox(key: "thumb")
        promoDesc = unboxer.unbox(key: "subtitle") ?? ""
            //("$10 off " + promoName)
    }
    
    init(id: Int, name: String, desc: String, imageURL: String) {
        self.id = id
        self.name = name
        self.promoDesc = desc
        self.imageURL = imageURL
    }
    

}
