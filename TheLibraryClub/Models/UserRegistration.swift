//
//  UserRegistration.swift
//  TheLibraryClub
//
//  Created by Phaninder on 22/01/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import Foundation

struct UserRegistration {
    
    var firstName = ""
    var lastName = ""
    var email = ""
    var password = ""
    var confirmPassword = ""
    var dob = ""
    var gender = ""
    var phoneNumber = ""
    
    init() {
        self.firstName = ""
        self.lastName = ""
        self.email = ""
        self.password = ""
        self.confirmPassword = ""
        self.dob = ""
        self.gender = ""
        self.phoneNumber = ""
    }
    
}

