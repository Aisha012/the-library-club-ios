//
//  PromotionDescription.swift
//  TheLibraryClub
//
//  Created by Phaninder on 21/01/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import Foundation
import Unbox

class PromotionDescription: Unboxable  {
    
    let id: Int
    let name: String!
    let promoDesc: String!
    let imageURL: String!
    let promoInfo: String!
    var endDate: String!
    var outlets = [Outlet]()
    var isFavorite = false
    
    required init(unboxer: Unboxer) throws {
        
        id = unboxer.unbox(key: "id") ?? 0
        let promoName = unboxer.unbox(key: "name") ?? ""
        name = promoName
        imageURL = unboxer.unbox(key: "thumb") ?? ""
        isFavorite = unboxer.unbox(key: "favorite") ?? false
        promoDesc = unboxer.unbox(key: "subtitle") ?? ""
            //?? ("$10 off " + promoName)
        promoInfo = unboxer.unbox(key: "description") ?? "GRUB is proud to be part of The Food Explorer Group, a home-grown brand conceptualised and run by 4 young local entrepreneurs. Having built Cookyn Inc, Singapore’s premier cooking events company, the quartet have recently added redpan, a modern Singaporean bistro at Marina Square to their repertoire of ‘food exploring’ adventures.\r\n\r\nWe are blessed to call Singapore our home and look to source locally wherever possible. Our signature burger buns are customised and freshly baked by a local bakery. We work to list local brewers extensively in our craft beer list and infuse local flavours creatively throughout our menu. You’ll find us in one of the largest urban parks in Singapore that features a beautiful meandering river and flourishing wildlife biodiversity."
        endDate = unboxer.unbox(key: "end_date") ?? ""
        if let date = self.endDate, !date.isEmpty {
            let index = date.index(date.startIndex, offsetBy: 10)
            let myDateString = String(date[..<index])
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd"
            let myDate = dateFormatter.date(from: myDateString)!
            
            dateFormatter.dateFormat = "dd MMM, yyyy"
            self.endDate = dateFormatter.string(from: myDate)
            
        }

        if let outletsArray: [[String: AnyObject]] = unboxer.unbox(key: "outlets") {
            for outletDict in outletsArray {
                do {
                    let outlet: Outlet = try unbox(dictionary: outletDict)
                    outlets.append(outlet)
                } catch {
                    //error during single parsing
                }
            }
        }

    }

}

