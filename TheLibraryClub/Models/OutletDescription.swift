//
//  OutletDescription.swift
//  TheLibraryClub
//
//  Created by Phaninder on 20/01/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import Foundation
import Unbox
import MapKit

class OutletDescription: Unboxable {
    
    let id: Int
    let name: String!
    let info: String!
    let address: String!
    let distance: String!
    let imageURL: String!
    let latitude: String?
    let longitude: String?
    let phone: String!
    let email: String!
    let website: String!
    let websiteDisplay: String!
    let facebook: String!
    let instagram: String!
    var promotions = [Promotion]()
    let operatingHours: [String: String]?
    let outletLocation: OutletLocation?
    let allowBooking: Bool!
    var isFavorite = false
    let menu:String?
    required init(unboxer: Unboxer) throws {
        id = unboxer.unbox(key: "id") ?? 0
        name = unboxer.unbox(key: "name") ?? ""
        info = unboxer.unbox(key: "description") ?? ""
        menu = unboxer.unbox(key: "menu") ?? ""
        address = unboxer.unbox(key: "address") ?? ""
        allowBooking = unboxer.unbox(key: "allow_booking") ?? false

        if let dist: Int = unboxer.unbox(key: "distance") {
            distance = "\(dist) KM"
        } else {
            distance = ""
        }
        imageURL = unboxer.unbox(keyPath: "picture.thumb") ?? ""
        latitude = unboxer.unbox(key: "latitude")
        longitude = unboxer.unbox(key: "longitude")
        phone = unboxer.unbox(key: "phone") ?? ""
        email = unboxer.unbox(key: "email") ?? ""
        website = unboxer.unbox(key: "website_link") ?? ""
        websiteDisplay = unboxer.unbox(key: "website_host") ?? ""
        facebook = unboxer.unbox(key: "facebook") ?? ""
        instagram = unboxer.unbox(key: "instagram") ?? ""
        operatingHours = unboxer.unbox(key: "operating_hours")
        isFavorite = unboxer.unbox(key: "favorite") ?? false
        if let lat = latitude, let long = longitude {
            outletLocation = OutletLocation.init(title: name,
                                                 outletID: id,
                                                 locationName: address,
                                                 allowBooking: allowBooking,
                                                 coordinate: CLLocationCoordinate2D(latitude: Double(lat)!,longitude: Double(long)!), operatingHours: operatingHours)
        } else {
            outletLocation = nil
        }
        if let promotionsArray: [[String: AnyObject]] = unboxer.unbox(key: "promotions") {
            for promotionDict in promotionsArray {
                do {
                    let promotion: Promotion = try unbox(dictionary: promotionDict)
                    promotions.append(promotion)
                } catch {
                    //error during single parsing
                }
            }
        }
    }

}
