//
//  Badge.swift
//  TheLibraryClub
//
//  Created by Phaninder on 08/02/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import Foundation
import Unbox

class Badge: Unboxable {
    
    let name: String!
    let date: String!
    let picture: String!
    
    required init(unboxer: Unboxer) throws {
        name = unboxer.unbox(key: "badge") ?? ""
        date = unboxer.unbox(key: "date2") ?? ""
        picture = unboxer.unbox(key: "pic") ?? ""
    }
    
}
