//
//  Notify.swift
//  TheLibraryClub
//
//  Created by Phaninder on 30/01/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import Foundation
import Unbox

class Notify: Unboxable {

    let id: Int!
    let message: String!
    let title: String!
    let type: String!
    let redirectId: Int!
    let userId: Int!
    let createdAt: String!
    let createdDate: Date!
    let elapsedTime: String!
    
    required init(unboxer: Unboxer) throws {
        id = unboxer.unbox(key: "id") ?? 0
        message = unboxer.unbox(key: "message") ?? ""
        createdAt = unboxer.unbox(key: "created_at") ?? "2018-01-30T20:19:16.000Z"
        if let date = self.createdAt, !date.isEmpty {
            let index = date.index(date.startIndex, offsetBy: 16)
            let myDateString = String(date[..<index])
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm"
            createdDate = dateFormatter.date(from: myDateString)!
            dateFormatter.dateFormat = "dd MMM, yyyy"
            elapsedTime = dateFormatter.string(from: createdDate)
        } else {
            createdDate = Date()
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "dd MMM, yyyy"
            elapsedTime = dateFormatter.string(from: createdDate)
        }
        title = unboxer.unbox(key: "title") ?? "Library Club"
        type = unboxer.unbox(key: "notification_type") ?? ""
        redirectId = unboxer.unbox(key: "redirect_id") ?? 0
        userId = unboxer.unbox(key: "user_id") ?? 0
    }
    
}
