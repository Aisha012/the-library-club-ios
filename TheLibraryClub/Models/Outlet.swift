//
//  Outlet.swift
//  TheLibraryClub
//
//  Created by Phaninder on 18/01/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import Foundation
import Unbox

class Outlet: Unboxable {
    
    let id: Int
    let name: String!
    let address: String!
    let distance: String!
    let imageURL: String!
    let latitude: String?
    let longitude: String?
    let allowBooking: Bool!
    let operatingHours: [String: String]?

    required init(unboxer: Unboxer) throws {
        id = unboxer.unbox(key: "id") ?? 0
        name = unboxer.unbox(key: "name") ?? ""
        address = unboxer.unbox(key: "address") ?? ""
        if let dist: Int = unboxer.unbox(key: "distance") {
            distance = "\(dist) KM"
        } else {
            distance = ""
        }
        imageURL = unboxer.unbox(keyPath: "picture.original") ?? ""
        latitude = unboxer.unbox(key: "latitude")
        longitude = unboxer.unbox(key: "longitude")
        allowBooking = unboxer.unbox(key: "allow_booking") ?? false
        operatingHours = unboxer.unbox(key: "operating_hours")
    }
    
    init(id: Int, name: String, address: String, distance: String, imageURL: String, allowBooking: Bool, operatingHours: [String: String]) {
        self.id = id
        self.name = name
        self.address = address
        self.distance = distance
        self.imageURL = imageURL
        self.allowBooking = allowBooking
        self.operatingHours = operatingHours
        self.latitude = nil
        self.longitude = nil
    }
    
}
