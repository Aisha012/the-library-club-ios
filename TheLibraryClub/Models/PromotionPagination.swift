//
//  PromotionPagination.swift
//  TheLibraryClub
//
//  Created by Phaninder on 19/01/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import Foundation
import Unbox

class PromotionPagination: Unboxable {
    
    var promotions: [Promotion] = []
    private let limit = 10
    var currentPageNumber = 1
    var paginationType: PaginationType = .new
    var hasMoreToLoad: Bool
    
    init() {
        promotions = []
        currentPageNumber = 1
        paginationType = .new
        hasMoreToLoad = false
    }
    
    required init(unboxer: Unboxer) throws {
        if let outletsArray = unboxer.dictionary["data"] as? [[String: Any]] {
            for outletDict in outletsArray {
                let outlet: Promotion = try unbox(dictionary: outletDict)
                self.promotions.append(outlet)
            }
        }
        hasMoreToLoad = self.promotions.count == limit
    }
    
    func appendDataFromObject(_ newPaginationObject: PromotionPagination) {
        switch self.paginationType {
        case .new, .reload:
            self.promotions = [];
            self.promotions = newPaginationObject.promotions
        case .old:
            self.promotions += newPaginationObject.promotions
        }
        self.paginationType = .old
        self.hasMoreToLoad = newPaginationObject.hasMoreToLoad
        self.currentPageNumber += 1
    }
    
}
