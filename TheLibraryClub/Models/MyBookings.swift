//
//  MyBookings.swift
//  TheLibraryClub
//
//  Created by Kalpana_Hipster on 03/11/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import Foundation
import Unbox

class MyBookings: Unboxable {
    
    let id: Int!
    let status: String!
    let outlet: Outlet!
    let bookingDate: String!
    let bookingTime: String!
    let phoneNumber: String!
    let email: String!
    let personsPax: Int!
    let alergy: String!
    let remarks: String!
    let anniversary: Bool!
    let birthday: Bool!
    
    required init(unboxer: Unboxer) throws {
        id = unboxer.unbox(key: "id") ?? 0
        bookingDate = unboxer.unbox(key: "booking_date") ?? ""
        bookingTime = unboxer.unbox(key: "booking_time") ?? "2018-01-30T20:19:16.000Z"
        status = unboxer.unbox(key: "status") ?? ""
        phoneNumber = unboxer.unbox(key: "phone_no") ?? ""
        email = unboxer.unbox(key: "email") ?? ""
        personsPax = unboxer.unbox(key: "pax") ?? 0
        outlet = unboxer.unbox(key: "outlet")
        alergy = unboxer.unbox(key: "alergy") ?? ""
        remarks = unboxer.unbox(key: "remarks") ?? ""
        anniversary = unboxer.unbox(key: "anniversary") ?? false
        birthday = unboxer.unbox(key: "birthday") ?? false
    }
    
}
