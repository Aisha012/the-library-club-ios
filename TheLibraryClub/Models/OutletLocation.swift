//
//  OutletLocation.swift
//  TheLibraryClub
//
//  Created by Phaninder on 19/01/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import Foundation
import MapKit
import Contacts

class OutletLocation: NSObject, MKAnnotation {
    
    let title: String?
    let outletID: Int
    let locationName: String
    let coordinate: CLLocationCoordinate2D
    let allowBooking: Bool!
    let operatingHours: [String: String]?

    init(title: String, outletID: Int, locationName: String, allowBooking: Bool, coordinate: CLLocationCoordinate2D, operatingHours: [String: String]?) {
        self.title = title
        self.outletID = outletID
        self.locationName = locationName
        self.coordinate = coordinate
        self.allowBooking = allowBooking
        self.operatingHours = operatingHours
        super.init()
    }
    
    var subtitle: String? {
        return locationName
    }
    
    init(outlet: Outlet) {
        self.title = outlet.name
        self.outletID = outlet.id
        self.locationName = outlet.address
        self.allowBooking = outlet.allowBooking
        
        let lat = outlet.latitude ?? "0"
        let long = outlet.longitude ?? "0"
        
        self.coordinate = CLLocationCoordinate2D(latitude: Double(lat)!,
                                                 longitude: Double(long)!)
        self.operatingHours = outlet.operatingHours
        super.init()
    }
    
    func mapItem() -> MKMapItem {
        let addressDict = [CNPostalAddressStreetKey: subtitle!]
        let placemark = MKPlacemark(coordinate: coordinate, addressDictionary: addressDict)
        let mapItem = MKMapItem(placemark: placemark)
        mapItem.name = title
        return mapItem
    }

}
