


import Foundation
import Unbox

class CheckEmailResponse : Unboxable {
	var id : String?
	var email : String?
	var username : String?
	var first_name : String?
	var last_name : String?
	var is_active : String?
	var profilepic : String?
	var dob : String?
	var gender : String?
	var phonenum : String?
	var joinfrom : String?
	var joindate : String?
	var cardnum : String?
	var group : String?
	var balance : String?

	

    required init(unboxer: Unboxer) throws {

		id = unboxer.unbox(key:"id") ?? ""
		email = unboxer.unbox(key:"email") ?? ""
		username = unboxer.unbox(key:"username") ?? ""
		first_name = unboxer.unbox(key:"first_name") ?? ""
		last_name = unboxer.unbox(key:"last_name") ?? ""
		is_active = unboxer.unbox(key:"is_active") ?? ""
		profilepic = unboxer.unbox(key:"profilepic") ?? ""
		dob = unboxer.unbox(key:"dob") ?? ""
		gender = unboxer.unbox(key:"gender") ?? ""
		phonenum = unboxer.unbox(key:"phonenum") ?? ""
		joinfrom = unboxer.unbox(key:"joinfrom") ?? ""
		joindate = unboxer.unbox(key:"joindate") ?? ""
		cardnum = unboxer.unbox(key:"cardnum") ?? ""
		group = unboxer.unbox(key:"group") ?? ""
		balance = unboxer.unbox(key:"balance") ?? ""
	}

}
