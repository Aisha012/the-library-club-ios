//
//  CustomerInfoPagination.swift
//  TheLibraryClub
//
//  Created by Phaninder on 31/01/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import Foundation
import Unbox

class CustomerInfoPagination: Unboxable {
    
    var orders: [Order] = []
    var rewardPoints: String!
    var rewardPointChange: String!
    var group: String!
    var totalHistory: Int!
    private let limit = 10
    var currentPageNumber = 1
    var paginationType: PaginationType = .new
    var hasMoreToLoad: Bool
    
    init() {
        orders = []
        rewardPoints = "0"
        rewardPointChange = "0"
        totalHistory = 0
        group = "1"
        currentPageNumber = 1
        paginationType = .new
        hasMoreToLoad = false
    }
    
    required init(unboxer: Unboxer) throws {
        if let ordersArray = unboxer.dictionary["histories"] as? [[String: Any]] {
//            for i in 0..<4 {
//                let order: Order = try unbox(dictionary: ordersArray[i])
//                self.orders.append(order)
//            }
            for orderDict in ordersArray {
                let order: Order = try unbox(dictionary: orderDict)
                self.orders.append(order)
            }
        }
        hasMoreToLoad = self.orders.count == limit
        rewardPoints = unboxer.unbox(key: "rewardpoint") ?? "0"
        rewardPointChange = unboxer.unbox(key: "rewardpointchange") ?? "0"
        totalHistory = unboxer.unbox(key: "totalhistory") ?? 0
        group = unboxer.unbox(key: "group") ?? "1"
    }
    
    func appendDataFromObject(_ newPaginationObject: CustomerInfoPagination) {
        switch self.paginationType {
        case .new, .reload:
            self.orders = [];
            self.orders = newPaginationObject.orders
        case .old:
            self.orders += newPaginationObject.orders
        }
        self.rewardPoints = newPaginationObject.rewardPoints
        self.rewardPointChange = newPaginationObject.rewardPointChange
        self.totalHistory = newPaginationObject.totalHistory
        self.group = newPaginationObject.group
        self.paginationType = .old
        self.hasMoreToLoad = newPaginationObject.hasMoreToLoad
        self.currentPageNumber += 1
    }
    
}
