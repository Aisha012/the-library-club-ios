//
//  Booking.swift
//  TheLibraryClub
//
//  Created by Phaninder on 08/02/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import Foundation

struct Booking {
    
    var outletName = ""
    var date = ""
    var time = ""
    var persons = ""
    var email = ""
    var phone = ""
    var remarks = ""
    var name = ""
    var isAnniversarySelected = false
    var isBirthdaySelected = false
    var isOthersSelected = false
    var isAllergicSelected = false
    var othersText = ""
    var allergiesText = ""
    
    init() {
        self.outletName = ""
        self.date = ""
        self.time = ""
        self.persons = ""
        self.email = UserStore.shared.emailId
        self.phone = UserStore.shared.phoneNumber
        self.name = UserStore.shared.firstName + " " + UserStore.shared.lastName
        self.remarks = ""
        self.isAnniversarySelected = false
        self.isBirthdaySelected = false
        self.isOthersSelected = false
        self.isAllergicSelected = false
        self.othersText = ""
        self.allergiesText = ""
    }
    
}
