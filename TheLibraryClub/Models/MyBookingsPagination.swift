//
//  MyBookingsPagination.swift
//  TheLibraryClub
//
//  Created by Kalpana_Hipster on 03/11/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import Foundation
import Unbox

class MyBookingsPagination: Unboxable {
    
    var myBookings: [MyBookings] = []
    private let limit = 10
    var currentPageNumber = 1
    var paginationType: PaginationType = .new
    var hasMoreToLoad: Bool
    
    init() {
        myBookings = []
        currentPageNumber = 1
        paginationType = .new
        hasMoreToLoad = false
    }
    
    required init(unboxer: Unboxer) throws {
        if let myBookingArray = unboxer.dictionary["data"] as? [[String: Any]] {
            for myBookingDict in myBookingArray {
                let myBooking: MyBookings = try unbox(dictionary: myBookingDict)
                self.myBookings.append(myBooking)
            }
        }
        hasMoreToLoad = self.myBookings.count == limit
    }
    
    func appendDataFromObject(_ newPaginationObject: MyBookingsPagination) {
        switch self.paginationType {
        case .new, .reload:
            self.myBookings = [];
            self.myBookings = newPaginationObject.myBookings
            self.paginationType = .old
        case .old:
            self.myBookings += newPaginationObject.myBookings
            self.paginationType = .old
        }
        self.hasMoreToLoad = newPaginationObject.hasMoreToLoad
        self.currentPageNumber += 1
    }
    
}
