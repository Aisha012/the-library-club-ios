//
//  User.swift
//  TheLibraryClub
//
//  Created by Phaninder on 22/01/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import Foundation
import Unbox

class User: Unboxable {
    
    let ID: String!
    let email: String!
    let firstName: String!
    let lastName: String!
    let isActive: Bool
    let profilePic: String!
    var dob: String!
    let gender: String!
    let phonenum: String!
    let cardNum: String!
    
    required init(unboxer: Unboxer) throws {
        ID = unboxer.unbox(key: "id") ?? ""
        email = unboxer.unbox(key: "email") ?? ""
        firstName = unboxer.unbox(key: "first_name") ?? ""
        lastName = unboxer.unbox(key: "last_name") ?? ""
        let activeUser: String = unboxer.unbox(key: "is_active") ?? "0"
        isActive = activeUser == "1" ? true : false
        profilePic = unboxer.unbox(key: "profilepic") ?? ""
        dob = unboxer.unbox(key: "dob") ?? ""
//        if let date = self.dob, !date.isEmpty {
//            let index = date.index(date.startIndex, offsetBy: 10)
//            let myDateString = String(date[..<index])
//            
//            let dateFormatter = DateFormatter()
//            dateFormatter.dateFormat = "yyyy-MM-dd"
//            let myDate = dateFormatter.date(from: myDateString)!
//            
//            dateFormatter.dateFormat = "dd/MM/yyyy"
//            self.dob = dateFormatter.string(from: myDate)
//        }
        
        let userGender = unboxer.unbox(key: "gender") ?? "Not Set"
        if userGender == "1" {
            gender = "Male"
        } else if userGender == "2" {
            gender = "Female"
        } else {
            gender = "Not Set"
        }
        phonenum = unboxer.unbox(key: "phonenum") ?? ""
        cardNum = unboxer.unbox(key: "cardnum") ?? ""
    }
    
}
