//
//  UserUpdate.swift
//  TheLibraryClub
//
//  Created by Phaninder on 08/02/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import Foundation

struct UserUpdate {
    
    var firstName = ""
    var lastName = ""
    var gender = ""
    var dob = ""
    var phoneNumber = ""
    
    init() {
        self.firstName = UserStore.shared.firstName
        self.lastName = UserStore.shared.lastName
        self.gender = UserStore.shared.gender
        self.dob = UserStore.shared.DOB
        self.phoneNumber = UserStore.shared.phoneNumber
    }
    
}
