//
//  OrderPagination.swift
//  TheLibraryClub
//
//  Created by Phaninder on 06/04/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import Foundation
import Unbox

class OrderPagination: Unboxable {
    
    var orders: [Order] = []
    private let limit = 10
    var currentPageNumber = 0
    var paginationType: PaginationType = .new
    var hasMoreToLoad: Bool
    
    init() {
        orders = []
        currentPageNumber = 1
        paginationType = .new
        hasMoreToLoad = false
    }
    
    required init(unboxer: Unboxer) throws {
        if let ordersArray = unboxer.dictionary["data"] as? [[String: Any]] {
            for orderDict in ordersArray {
                let order: Order = try unbox(dictionary: orderDict)
                self.orders.append(order)
            }
        }
        hasMoreToLoad = self.orders.count == limit
    }
    
    func appendDataFromObject(_ newPaginationObject: OrderPagination) {
        switch self.paginationType {
        case .new, .reload:
            self.orders = [];
            self.orders = newPaginationObject.orders
            self.paginationType = .old
        case .old:
            self.orders += newPaginationObject.orders
            self.paginationType = .old
        }
        self.hasMoreToLoad = newPaginationObject.hasMoreToLoad
        self.currentPageNumber += 1
    }
    
}
