//
//  NotificationPagination.swift
//  TheLibraryClub
//
//  Created by Phaninder on 27/03/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import Foundation
import Unbox

class NotificationPagination: Unboxable {
    
    var notifications: [Notify] = []
    private let limit = 10
    var currentPageNumber = 1
    var paginationType: PaginationType = .new
    var hasMoreToLoad: Bool
    
    init() {
        notifications = []
        currentPageNumber = 1
        paginationType = .new
        hasMoreToLoad = false
    }
    
    required init(unboxer: Unboxer) throws {
        if let notificationsArray = unboxer.dictionary["data"] as? [[String: Any]] {
            for notificationDict in notificationsArray {
                let notification: Notify = try unbox(dictionary: notificationDict)
                self.notifications.append(notification)
            }
        }
        hasMoreToLoad = self.notifications.count == limit
    }
    
    func appendDataFromObject(_ newPaginationObject: NotificationPagination) {
        switch self.paginationType {
        case .new, .reload:
            self.notifications = [];
            self.notifications = newPaginationObject.notifications
            self.paginationType = .old
        case .old:
            self.notifications += newPaginationObject.notifications
            self.paginationType = .old
        }
        self.hasMoreToLoad = newPaginationObject.hasMoreToLoad
        self.currentPageNumber += 1
    }
    
}
