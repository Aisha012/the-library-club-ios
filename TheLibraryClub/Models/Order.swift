//
//  Order.swift
//  TheLibraryClub
//
//  Created by Phaninder on 31/01/18.
//  Copyright © 2018 Phaninder Kumar. All rights reserved.
//

import Foundation
import Unbox

class Order: Unboxable {
    
    let id: String!
    let currency: String!
    let transactionDetails: String!
    let outlet: String!
    
    
    //new
    var transactionTime: String!
    let receiptID: String!
    let receiptAmount: String!
    let point: String!
    let point_amount : String!
    let balance: String!
    let type: String!

    required init(unboxer: Unboxer) throws {
        id = unboxer.unbox(key: "history_id") ?? ""
        transactionTime = unboxer.unbox(key: "transaction_time") ?? ""

        if let date = self.transactionTime, !date.isEmpty {
            let index = date.index(date.startIndex, offsetBy: 10)
            let myDateString = String(date[..<index])
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd"
            let myDate = dateFormatter.date(from: myDateString)!
            
            dateFormatter.dateFormat = "dd/MM/yy"
            self.transactionTime = dateFormatter.string(from: myDate)
        }
        receiptID = unboxer.unbox(key: "receipt") ?? ""
        receiptAmount = unboxer.unbox(key: "receipt_amt") ?? ""
        point = unboxer.unbox(key: "point") ?? ""
        balance = unboxer.unbox(key: "balance") ?? ""
        type = unboxer.unbox(key: "type") ?? "Add"

        currency = unboxer.unbox(key: "currency") ?? ""
        transactionDetails = unboxer.unbox(key: "transaction_detail") ?? ""
        point_amount = unboxer.unbox(key: "point_amount") ?? ""
        outlet = unboxer.unbox(key: "outlet") ?? ""
    }
    
}
